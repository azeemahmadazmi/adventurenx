<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Adventure | NX</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon" />
    <link rel="stylesheet" href="{{asset('css/uikit.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/uikit.components.min.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/css/jquery-ui.min.css')}}" />
    <link rel="stylesheet" href="{{asset('custom/css/fonts.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/css/flexslider.css')}}" />
    <link rel="stylesheet" href="{{asset('custom/css/style.css')}}" />
    @yield('css')
    <style>
        .help-block{
            color:red !important;
        }
    </style>
</head>
<body>
@include('frontuser.layouts.header')
@yield('content')
@include('frontuser.layouts.footer')
<script data-cfasync="false" src="{{asset('assets/email-decode.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit.components.min.js')}}"></script>
<script src="{{asset('vendor/js/waypoints.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery.counterup.js')}}"></script>
<script src="{{asset('vendor/js/jquery.flexslider.js')}}"></script>
<script src="{{asset('custom/js/custom.js')}}"></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyCmZhoiuy8cT2ORR78A_-gQJ690U14ePlo'></script>
@yield('scripts')

</body>
</html>