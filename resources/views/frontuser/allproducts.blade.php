@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative shop-breadcrumbs-wellness">
            <img class="uk-invisible" src="images/images_wellness/shop_breadcrumbs.jpg" alt="Classes Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">shop</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">shop</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <div class="shop section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <aside class="uk-width-medium-2-10 uk-width-small-3-10">
                    <p class="wall">Refine by:</p>
                    <h4 class="h-4">product Categories</h4>
                    <ul class="aside-navigation">
                        <li><a href="#">Sports Wear</a><span class="products-available">(70)</span></li>
                        <li><a href="#">Accessories and Shoe</a><span class="products-available">(50)</span></li>
                        <li><a href="#">Equipment</a><span class="products-available">(35)</span></li>
                        <li><a href="#">All Cycling</a><span class="products-available">(123)</span></li>
                        <li><a href="#">All Rafting</a><span class="products-available">(30)</span></li>
                        <li><a href="#">Snowboards</a><span class="products-available">(10)</span></li>
                        <li class="active"><a href="#">All Products</a><span class="products-available">(318)</span></li>
                    </ul>
                    <div class="price-range">
                        <h4 class="h-4">price range</h4>
                        <div class="price-range-slider"></div>
                        <button class="uk-button btn-custom">apply filter</button>
                    </div>
                </aside>
                <div class="uk-width-medium-8-10 uk-width-small-7-10">
                    <div class="uk-clearfix shop-display-results">
                        <ul class="blog-info-list uk-float-left">
                            <li>Showing 1-8 of 278 results</li>
                            <li>
                                Show products per page:
                                <ul>
                                    <li class="active"><a href="#">8</a></li>
                                    <li><a href="#">16</a></li>
                                    <li><a href="#">all</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="uk-form-select uk-float-right" data-uk-form-select>
                            <span></span>
                            <i class="uk-icon-angle-down uk-float-right"></i>
                            <select>
                                <option value="">Default Sorting</option>
                                <option value="">By Price</option>
                                <option value="">By Product</option>
                            </select>
                        </div>
                    </div>
                    <ul class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-3 uk-grid-width-small-1-2 products-thumbnail" data-uk-grid-margin>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_01.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">trident composite Helmet</h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$136</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_02.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">cataract carbon razor oar blade</h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$73</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <button class="uk-button btn-custom-sale" type="button">sale</button>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_03.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">lounger chair 2 colors</h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center"><span class="shop-item-price-old">$128</span><span class="shop-item-price-new">$118</span></p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_04.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">fitbit blaze smart fitness watch </h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$215</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_06.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">Big Wall Harness 2 colors</h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$136</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_07.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">Women's Thrasher Helmet</h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$73</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="product.html">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_08.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">bear spray with hip holster</h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$128</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                        <li class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden uk-animation-hover">
                                    <a href="{{url('/products')}}">
                                        <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/shop_item_09.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                                <figcaption>
                                    <h5 class="h-5">Outdoor Research Cascadia Gaiters Belt </h5>
                                    <div class="title-line-sm"></div>
                                    <p class="shop-item-price uk-text-center">$215</p>
                                    <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                                </figcaption>
                            </figure>
                        </li>
                    </ul>
                    <div class="shop-pagination">
                        <ul class="uk-pagination uk-pagination-right pagination">
                            <li class="btn-active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#" class="uk-text-bold">Next  <i class="uk-icon-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection