<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ClientProfile extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */

       protected $table = 'client_profiles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id','image', 'name', 'email', 'mobile', 'pan_card_number', 'pan_card_image', 'adhaar_card_number', 'adhaar_card_image', 'bank_account_number', 'bank_account_type', 'bank_holder_account_name', 'bank_ifsc_code', 'address', 'office_address','previous_expeience', 'status'];

    public function client(){
        return $this->belongsTo('App\Client');
    }

    
}
