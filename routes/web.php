<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*---------------------------------- Start User Routes--------------------------------------------------------*/


Route::group(['namespace' => 'FrontUser'], function () {
    Route::get('/', 'IndexController@index');
    Route::get('/about', 'IndexController@about');
    Route::get('/all_classes', 'IndexController@all_classes');
    Route::get('/classdescriptions', 'IndexController@classdescriptions');
    Route::get('/allevents', 'IndexController@events');
    Route::get('/fullblogpage', 'IndexController@fullblogpage');
    Route::get('/blogpage', 'IndexController@blogpage');
    Route::get('/contactus', 'IndexController@contactus');
    Route::get('/allproducts', 'IndexController@allproducts');
    Route::get('/products', 'IndexController@products');
    Route::get('/checkout', 'IndexController@checkout');
  //  Route::get('/savecurrenteventurl', 'IndexController@savecurrenteventurl');
    Route::get('/gallery', 'IndexController@gallery');
    Route::get('/about-team-member', 'IndexController@team_member');
    Route::post('/contactusform','IndexController@contactusform');
    Route::post('/blogcomments','IndexController@blogcomments');
    Route::post('/subscriberform','IndexController@subscriberform');
    Route::get('/showImageGallery','IndexController@showImageGallery');
    Route::get('/eventdescriptions','EventController@event');

});

Route::group(['namespace' => 'Auth','middleware' => 'guest'], function () {
    Route::get('/login/{social}', 'LoginController@socialLogin')
        ->where('social', 'twitter|facebook|linkedin|google|github');
    Route::get('login/{social}/callback', 'LoginController@handleProviderCallback')
        ->where('social', 'twitter|facebook|linkedin|google|github');
});
Auth::routes();

Route::group(['middleware' => 'auth','namespace' => 'FrontUser'], function () {
    Route::get('/cart', 'HomeController@cart');
    Route::get('/home', 'IndexController@index');
    Route::get('/myaccount', 'HomeController@myaccount');
    Route::get('/orderhistories', 'HomeController@orderhistories');
    Route::get('/show_orderhisories','HomeController@showHistories');
    Route::get('/editprofile', 'HomeController@editprofile');
    Route::post('/saveuserprofile', 'HomeController@saveuserprofile');
    Route::post('cartpayment','CartController@cartpayment');
    Route::post('/paysuccess', 'RazorpayController@paysuccess');
    Route::post('/createorder', 'RazorpayController@createorder');
    Route::get('/razor-thank-you', 'RazorpayController@thankYou');
    Route::get('/reciept', 'RazorpayController@reciept');
    Route::get('/users_tickets_description','CartController@users_tickets_description');


    Route::post('/userdetails_attendee','CartController@userdetails_attendee');
    Route::post('/next_users_detaisl','CartController@next_users_detaisl');

    Route::get('/final_cart_payment','CartController@final_cart_payment');
});


/*---------------------------------- End User Routes--------------------------------------------------------*/




/*---------------------------------- Start Client Routes--------------------------------------------------------*/

Route::group(['prefix' => 'client','namespace' => 'Client\Auth','middleware' => 'client_guest'], function () {
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('client.login');
    Route::get('register', 'RegisterController@showRegistrationForm');
    Route::post('register', 'RegisterController@register')->name('client.register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('client.password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('client.password.update');
});
Route::group(['prefix' => 'client','namespace' => 'Client\Auth','middleware' => 'client_auth'], function () {
    Route::get('/logout', 'LoginController@logout')->name('client.logout');
});

Route::group(['prefix' => 'client','namespace' => 'Client\Auth','middleware' => ['client_auth','emailNot_verified_client']], function () {
    Route::get('email/verify', 'VerificationController@show')->name('client.verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('client.verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('client.verification.resend');
});

Route::group(['prefix' => 'client','namespace' => 'Client','middleware' => ['client_auth','email_verified_client']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
    Route::resource('events','EventsController');
    Route::get('events/createdby/{id}','EventsController@createdBy');

    Route::get('/edit-client-profile','HomeController@edit');
    Route::get('/show-client-profile','HomeController@show');
    Route::PATCH('/update-client-profile','HomeController@update');
   // Route::resource('client-profile', 'ClientProfileController');
    Route::resource('/users', 'UserController');
    Route::get('/client-events/orders','OrderController@clientOrder');
    Route::get('/client-events/orders/{id}','OrderController@viewclientOrder');
    Route::get('/reciept', 'OrderController@reciept');
    Route::get('events/addTicket/{id}','EventsController@addTicket');
    Route::post('event/createNewTicket','EventsController@createNewTicket');
    Route::get('events/editTicket/{id}','EventsController@editTicket');
    Route::PATCH('event/saveeditTicket/{id}','EventsController@saveeditTicket');
});
/*----------------------------------End Client Routes-------------------------------------------------------------*/





/*----------------------------------Admin Routes Start-------------------------------------------------------------*/

Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'admin_guest'], function () {
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('admin.login');
    Route::get('register', 'RegisterController@showRegistrationForm');
    Route::post('register', 'RegisterController@register')->name('admin.register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('admin.password.update');
});
Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'admin_auth'], function () {
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => ['admin_auth','emailNot_verified']], function () {
    Route::get('email/verify', 'VerificationController@show')->name('admin.verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('admin.verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('admin.verification.resend');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['admin_auth','email_verified']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
    Route::resource('/users', 'UserController');
    Route::resource('home-page-banners', 'HomePageBannersController');
    Route::resource('gallery', 'GalleryController');
    Route::resource('banners', 'BannersController');
    Route::resource('teammembers', 'TeammembersController');
    Route::resource('about', 'AboutController');
    Route::resource('aboutus-camp', 'AboutusCampController');
    Route::resource('aboutus-activities', 'AboutusActivitiesController');
    Route::resource('aboutus-client-say', 'AboutusClientSayController');
    Route::resource('blog-category', 'BlogCategoryController');
    Route::resource('blogs', 'BlogsController');
    Route::resource('blog-comments', 'BlogCommentsController');
    Route::resource('contactus', 'ContactusController');
    Route::resource('subscriber', 'SubscriberController');
    Route::resource('event-categories', 'EventCategoriesController');
    Route::resource('company-partner', 'CompanyPartnerController');
    Route::resource('events','EventsController');
    Route::get('events/createdby/{id}','EventsController@createdBy');
    Route::get('/chnageeventstatus','EventsController@chnageeventstatus');
    Route::resource('client-profile', 'ClientProfileController');
    Route::resource('user-profile', 'UserProfileController');
    Route::resource('folders', 'FoldersController');
    Route::get('/admin-events/orders','OrderController@adminOrder');
    Route::get('/admin-events/orders/{id}','OrderController@viewadminOrder');
    Route::get('/client-events/orders','OrderController@clientOrder');
    Route::get('/client-events/orders/{id}','OrderController@viewclientOrder');
    Route::get('/reciept', 'OrderController@reciept');
    Route::get('events/addTicket/{id}','EventsController@addTicket');
    Route::post('event/createNewTicket','EventsController@createNewTicket');

    Route::get('events/editTicket/{id}','EventsController@editTicket');
    Route::PATCH('event/saveeditTicket/{id}','EventsController@saveeditTicket');

    Route::get('/add_dynamic_form','EventsController@add_dynamic_form');
    Route::get('/delete_dynamic_form','EventsController@delete_dynamic_form');


    Route::resource('coupons', 'CouponsController');






});

/*----------------------------------End Admin Routes-------------------------------------------------------------*/













