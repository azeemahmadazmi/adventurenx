@extends('frontuser.layouts.master')
<style>

    .events-calendar{
      margin-left: 300px;
    }
    @media (max-width: 768px){
        .events-calendar{
            margin-left: 20px;
        }
    }

</style>
@section('banner')
    <section class="home-slideshow-wellness">
        <div class="uk-slidenav-position" data-uk-slideshow="{autoplay:true, autoplayInterval: 4000}">
            <ul class="uk-slideshow">
                @if(isset($homebanner) && $homebanner->isNotEmpty())
                    @foreach($homebanner as $hk => $hv)
                        <li>
                            <img class="slideshow-image" src="{{url('images/HomepageBanner/'.$hv->image)}}" alt="Slide Image">
                            <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-left">
                                <div class="slider-header-group">
                                    <h1 class="uk-text-uppercase">{{$hv->title1}}</h1>
                                    <h3 class="uk-text-uppercase uk-text-right">{{$hv->title2}}</h3>
                                </div>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
            <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
                @if(isset($homebanner) && $homebanner->isNotEmpty())
                    @foreach($homebanner as $hk => $hv)
                        <li data-uk-slideshow-item="{{$hk}}"><a href="#"></a></li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>
 @endsection
@section('content')
    <section class="next-events-wellness section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">next events</h2>
                    <div class="title-line-lg"></div>
                    <div class="events-thumbnail border-b2">
                        <div class="uk-grid uk-grid-width-medium-1-3" data-uk-grid-margin>
                            @if(isset($events) && $events->isNotEmpty())
                                @foreach($events as $ek => $ev)
                                    <div class="thumbnails-wrapper">
                                        <figure>
                                            <div class="overflow-hidden uk-animation-hover thumb-img-wrapper">
                                                <a href="{{url('/eventdescriptions?eventid='.base64_encode($ev->id))}}">
                                                    <img class="uk-animation-scale uk-animation-reverse" src="{{url('Events/'.$ev->image)}}" alt="Event Image" width="364px" height="250px">
                                                </a>
                                            </div>
                                            <figcaption>
                                                <div class="figcaption-wrapper uk-grid uk-grid-small" data-uk-grid-margin>
                                                    <div class="uk-width-large-1-3 uk-text-left">
                                                        <button class="uk-button btn-custom" type="button">{{date('d/m/Y',strtotime($ev->event_start_date))}}</button>
                                                    </div>
                                                    <div class="uk-width-large-2-3 events-detail uk-text-left">
                                                        <h5 class="h-5">{{$ev->event_name}}</h5>
                                                        <div class="title-line-lg"></div>
                                                        <p>{{isset($ev->eventAddress->event_venue)?ucwords($ev->eventAddress->event_venue):''}} | at {{ date('h:i:A',strtotime($ev->event_start_time))}}</p>
                                                        <p>Event Fees : ₹{{isset($ev->eventTicket->price)?$ev->eventTicket->price:0}}</p>
                                                    </div>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    @endforeach
                                @endif
                        </div>
                    </div>
                    <!-- / uk-grid -->
                    <a href="{{url('/allevents')}}" class="text-overview">View all Events<img class="icon-right" src="images/images_wellness/icon-arrow-right-black.png" alt="Read More Icon"></a>
                </div>
            </div>
        </div>
    </section>
    <section>


        <div class="events-calendar">
            <h4 class="h-4">events calendar</h4>
            <div class="date-picker-custom-custom-callender">
            <iframe src="https://calendar.google.com/calendar/embed?height=300&amp;wkst=1&amp;bgcolor=%234285F4&amp;ctz=Asia%2FKolkata&amp;src=YWR2ZW50dXJlbnhAZ21haWwuY29t&amp;src=MGRiNzRkam1wZXExcjl0ZTk3cGlnNWluYzBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%23039BE5&amp;color=%23F4511E&amp;showPrint=0&amp;showNav=1&amp;showTitle=1&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;showDate=1" style="border:solid 1px #777" width="500" height="300" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>


    </section>
<section class="home-about section-padding-top section-padding-bottom">
    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <h2 class="h-2">be well with adventure-nx</h2>
                <div class="title-line-lg"></div>
                <!-- ABOUT SLIDESET START -->
                <div class="about-slideset">
                    <div data-uk-slideset="{small: 2, medium: 3, large: 3}">
                        <div class="uk-slidenav-position">
                            <!-- slideset item list -->
                            <ul class="uk-grid uk-slideset">

                                @if(isset($eventCategory) && $eventCategory->isNotEmpty())
                                    @foreach($eventCategory as $evk => $evv)
                                <li class="thumbnails-wrapper">
                                    <figure class="uk-overlay uk-animation-hover">
                                        <div class="overflow-hidden thumb-img-wrapper">
                                            <img class="uk-animation-scale uk-animation-reverse" src="{{asset('Events/EventsCategories/'.$evv->image)}}" alt="About Slideset Images" width="364px" height="605px">
                                        </div>
                                        <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                            <div class="uk-text-left">
                                                <h3 class="h-3">{{$evv->name}}</h3>
                                                <div class="title-line-lg"></div>
                                                <p class="text-overview">{!! substr($evv->about, 0, 100) !!}</p>
                                                <a class="text-overview" href="#">Read more<img class="icon-right" src="{{asset('images/images_wellness/icon-arrow-right.png')}}" alt="Read More Icon"></a>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </li>
                                    @endforeach
                                    @endif

                            </ul>
                            <!-- dotnav -->
                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / ABOUT SLIDESET END -->
    </div>
</section>
<!-- / HOME ABOUT END -->
<!-- ===========================================================================
    WE ARE ATMA
    ================================================================================-->
{{--<section class="we-are-wellness">--}}
    {{--<div class="uk-container uk-container-center">--}}
        {{--<div class="uk-grid">--}}
            {{--<div class="uk-width-1-1">--}}
                {{--<div class="home-about-classes section-padding-bottom">--}}
                    {{--<h2 class="h-2">we are adventure-nx training camp</h2>--}}
                    {{--<div class="title-line-lg"></div>--}}
                    {{--<div class="uk-grid simple-classes" data-uk-grid-margin>--}}
                        {{--<div class="uk-width-large-1-4 uk-width-small-1-2">--}}
                            {{--<figure>--}}
                                {{--<img src="images/images_wellness/about_classes_01.png" alt="Travel and Sports Icon">--}}
                            {{--</figure>--}}
                            {{--<h5 class="h-5">travel and sports</h5>--}}
                            {{--<p>Messenger bag single-origin coffee roof party before they sold out, hoodie crucifix meditation poutine tilde fanny pack locavore microdosing brunch.</p>--}}
                        {{--</div>--}}
                        {{--<div class="uk-width-large-1-4 uk-width-small-1-2">--}}
                            {{--<figure>--}}
                                {{--<img src="images/images_wellness/about_classes_02.png" alt="Relaxation and Harmony Icon">--}}
                            {{--</figure>--}}
                            {{--<h5 class="h-5">relaxation and harmony</h5>--}}
                            {{--<p>Messenger bag single-origin coffee roof party before they sold out, hoodie crucifix meditation poutine tilde fanny pack locavore microdosing brunch.</p>--}}
                        {{--</div>--}}
                        {{--<div class="uk-width-large-1-4 uk-width-small-1-2">--}}
                            {{--<figure>--}}
                                {{--<img src="images/images_wellness/about_classes_03.png" alt="Beauty and Health Icon">--}}
                            {{--</figure>--}}
                            {{--<h5 class="h-5">beauty and health</h5>--}}
                            {{--<p>Messenger bag single-origin coffee roof party before they sold out, hoodie crucifix meditation poutine tilde fanny pack locavore microdosing brunch.</p>--}}
                        {{--</div>--}}
                        {{--<div class="uk-width-large-1-4 uk-width-small-1-2">--}}
                            {{--<figure>--}}
                                {{--<img src="images/images_wellness/about_classes_04.png" alt="Balanced Diet Icon">--}}
                            {{--</figure>--}}
                            {{--<h5 class="h-5">balanced diet</h5>--}}
                            {{--<p>Messenger bag single-origin coffee roof party before they sold out, hoodie crucifix meditation poutine tilde fanny pack locavore microdosing brunch.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}
<!-- ===========================================================================
    HOME VIDEO
    ================================================================================-->
<div class="home-video">
    <div class="container-fluid uk-padding-remove">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-cover uk-position-relative video-cover-wellness">
                    <img class="uk-invisible" src="images/images_wellness/video.jpg" alt="Home Video">
                    <video id="video-element" class="uk-position-cover video-object uk-invisible">
                        <source src="{{asset('samplevideo/SampleVideo.mp4')}}" type="video/webm">
                    </video>

                    <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                        <button class="play-btn">
                            <img src="images/play_btn_wellness.png" alt="Play Button">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- / HOME VIDEO END -->
<!-- ===========================================================================
    HOME EVENTS
    ================================================================================-->

<!-- / HOME EVENTS END -->
<!-- ===========================================================================
    HOME BLOG | WELLNESS
    ================================================================================-->
<section class="home-blog-wellness">
    <div class="container-fluid uk-padding-remove">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-slidenav-position section-padding-top"  data-uk-slideshow>
                    <ul class="uk-slideshow">
                        @if(isset($blog) && $blog->isNotEmpty())
                            @foreach($blog as $bk => $bv)
                        <li class="uk-cover-background">
                            <figure class="uk-container-center uk-container">
                                <h2 class="h-2">our blog</h2>
                                <div class="title-line-lg"></div>
                                <figcaption class="uk-position-relative uk-overlay">
                                    <img src="{{asset('Blog/'.$bv->image)}}" alt="Slide Image" width="1130px" height="675px">
                                    <div class="uk-overlay-panel uk-overlay-bottom blog-info">
                                        <h3 class="h-3">{{$bv->name}}</h3>
                                        <div class="title-line-lg"></div>
                                        <ul class="blog-info-list">
                                            <li><a class="post-author" href="#">{{$bv->created_by}}</a></li>
                                            <li><a class="post-date" href="#">{{$bv->date}}</a></li>
                                            <li>In: <a class="post-category">{{isset($bv->blogcategory->name)?$bv->blogcategory->name:''}}</a></li>
                                        </ul>
                                        <p class="text-overview">{!!  substr($bv->about, 0, 300)!!}...</p>
                                        <a href="{{url('/blogpage?blog_id='.base64_encode($bv->id))}}" class="text-overview">Read more<img class="icon-right" src="images/images_wellness/icon-arrow-right.png" alt="Read More Icon"></a>
                                    </div>
                                </figcaption>
                            </figure>
                        </li>
                            @endforeach
                            @endif
                    </ul>
                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideshow-item="next"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- home-blog-wellness END -->
<!-- ===========================================================================
    HOME SHOP | WELLNESS
    ================================================================================-->

<div class="partners uk-text-center">
    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div data-uk-slideset="{autoplay: true, autoplayInterval: 5000, default: 1, small: 3, medium: 4, large: 5}">
                    <div class="uk-slidenav-position">
                        <ul class="uk-grid uk-slideset" data-uk-grid-match>
                            @if(isset($companyPartner) && $companyPartner->isNotEmpty())
                                @foreach($companyPartner as $ck => $cv)
                            <li>
                                <figure>
                                    <img src="{{asset('companyLogo/'.$cv->image)}}" alt="Partner's Logo" width="167px" height="81px">
                                </figure>
                            </li>
                                @endforeach
                                @endif

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection