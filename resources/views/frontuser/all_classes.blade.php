@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative classes-breadcrumbs-wellness">
            <img class="uk-invisible" src="images/images_wellness/classes_wellness_breadcrumbs.jpg" alt="Classes Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">classes</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">classes</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="popular-classes-wellness section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">popular classes</h2>
                    <div class="title-line-lg"></div>
                    <div class="classes-thumbnail-wellness" data-uk-slideset="{small: 2, medium: 3, large: 3}">
                        <div class="uk-slidenav-position">
                            <ul class="uk-grid uk-slideset">
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_01.jpg" alt="Rafting">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">rafting</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_02.jpg" alt="Skiing">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">skiing</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_03.jpg" alt="Cycling">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">cycling</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_01.jpg" alt="Rafting">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">rafting</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_02.jpg" alt="Skiing">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">skiing</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_03.jpg" alt="Cycling">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">cycling</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slideset Initializer -->
        </div>
    </section>
    <!-- ===========================================================================
        TIMETABLE
        ================================================================================-->
    <section class="timetable  section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">timetable</h2>
                    <div class="title-line-lg"></div>
                    <div class="overflow-container">
                        <table class="uk-table uk-table-striped time-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>08:00</th>
                                <th>08:30</th>
                                <th>09:00</th>
                                <th>09:30</th>
                                <th>10:00</th>
                                <th>10:30</th>
                                <th>11:00</th>
                                <th>11:30</th>
                                <th>12:00</th>
                                <th>12:30</th>
                                <th>13:00</th>
                                <th>13:30</th>
                                <th>14:00</th>
                                <th>14:30</th>
                                <th>15:00</th>
                                <th>15:30</th>
                                <th>16:00</th>
                                <th>16:30</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="uk-text-uppercase">monday</td>
                                <td class="event-tag" colspan="3"><span>rafting</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>skiing</span></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>boxing</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="uk-text-uppercase">tuesday</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="3"><span>snowboarding</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>hiking</span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="uk-text-uppercase">wednesday</td>
                                <td></td>
                                <td class="event-tag" colspan="3"><span>wakeboarding</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="uk-text-uppercase">thursday</td>
                                <td class="event-tag" colspan="3"><span>skiing</span></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>hiking</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>yoga</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="uk-text-uppercase">friday</td>
                                <td class="event-tag" colspan="2"><span>hiking</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="3"><span>wakeboarding</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="uk-text-uppercase">saturday</td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>yoga</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>boxing</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="uk-text-uppercase">sunday</td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="3"><span>wakeboarding</span></td>
                                <td></td>
                                <td class="event-tag" colspan="3"><span>skiing</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="event-tag" colspan="2"><span>rafting</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- / table end -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / timetable end -->
    <!-- ===========================================================================
        ADS-WELLNESS
        ================================================================================-->
    <section class="ads-wellness uk-cover-background uk-position-relative">
        <div class="container-fluid uk-padding-remove">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <img class="uk-invisible" src="images/images_wellness/add.jpg" alt="Ads Background">
                    <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                        <div class="uk-container uk-container-center">
                            <div>
                                <h2 class="h-2">training schedule is always with you!</h2>
                                <p class="text-sixteen">Twee biodiesel kale chips occupy banh mi, mustache selvage butcher disrupt leggings kogi artisan marfa pinterest. Cronut bicycle rights hoodie, viral cold-pressed meh wolf.</p>
                                <button class="uk-button btn-custom">Download the App</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ===========================================================================
        PRICES
        ================================================================================-->
    <section class="prices price-plan-wellness section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">popular classes</h2>
                    <div class="title-line-lg"></div>
                    <div class="uk-grid uk-grid-width-small-1-3 package-panel" data-uk-grid-margin>
                        <div>
                            <div class="uk-panel panel-custom">
                                <h3 class="package-panel-header uk-text-center">minimal plan</h3>
                                <p class="price-tag">$80</p>
                                <div class="title-line-lg"></div>
                                <p class="packages-icon"><i class="uk-icon-check"></i></p>
                                <p class="feature-active">Lorem ipsum dolor sit amet</p>
                                <p class="packages-icon"><i class="uk-icon-close"></i></p>
                                <p class="feature-inactive">Lorem ipsum dolor sit amet</p>
                                <p class="packages-icon"><i class="uk-icon-close"></i></p>
                                <p class="feature-inactive">Lorem ipsum dolor sit amet</p>
                                <button class="uk-button btn-custom-black">buy now</button>
                            </div>
                        </div>
                        <div>
                            <div class="uk-panel panel-custom base">
                                <h3 class="package-panel-header uk-text-center">base plan</h3>
                                <p class="price-tag">$80</p>
                                <div class="title-line-lg"></div>
                                <p class="packages-icon"><i class="uk-icon-check"></i></p>
                                <p class="feature-active">Lorem ipsum dolor sit amet</p>
                                <p class="packages-icon"><i class="uk-icon-check"></i></p>
                                <p class="feature-inactive">Lorem ipsum dolor sit amet</p>
                                <p class="packages-icon"><i class="uk-icon-close"></i></p>
                                <p class="feature-inactive">Lorem ipsum dolor sit amet</p>
                                <button class="uk-button btn-custom-black">buy now</button>
                            </div>
                        </div>
                        <div>
                            <div class="uk-panel panel-custom">
                                <h3 class="package-panel-header uk-text-center">advanced plan</h3>
                                <p class="price-tag">$80</p>
                                <div class="title-line-lg"></div>
                                <p class="packages-icon"><i class="uk-icon-check"></i></p>
                                <p class="feature-active">Lorem ipsum dolor sit amet</p>
                                <p class="packages-icon"><i class="uk-icon-check"></i></p>
                                <p class="feature-inactive">Lorem ipsum dolor sit amet</p>
                                <p class="packages-icon"><i class="uk-icon-check"></i></p>
                                <p class="feature-inactive">Lorem ipsum dolor sit amet</p>
                                <button class="uk-button btn-custom-black">buy now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection