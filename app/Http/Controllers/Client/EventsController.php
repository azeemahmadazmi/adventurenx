<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Admin;
use App\Client;
use App\Event;
use App\EventAddress;
use App\EventTicket;
class EventsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $authuser=Auth::guard('client')->user();
        if (!empty($keyword)) {
            $events = Event::orWhere(function($query) use($keyword){
                $query->where('event_name', 'LIKE', "%$keyword%")
                    ->orWhere('event_display_name', 'LIKE', "%$keyword%")
                    ->orWhere('event_start_date', 'LIKE', "%$keyword%")
                    ->orWhere('event_end_date', 'LIKE', "%$keyword%")
                    ->orWhere('start_time', 'LIKE', "%$keyword%")
                    ->orWhere('event_venue', 'LIKE', "%$keyword%")
                    ->orWhere('event_descriptions', 'LIKE', "%$keyword%")
                    ->orWhere('status', 'LIKE', "%$keyword%");
               })->where('creator_id',$authuser->id)->latest()->paginate($perPage);

        } else {
            $events = Event::where('creator_id',$authuser->id)->latest()->paginate($perPage);
        }

        return view('client.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('client.events.create');
    }


    public function store(Request $request)
    {
        $requestData=$request->all();
         DB::beginTransaction();
        try
        {
            if ($request->hasFile('image')) {
                $filename = Input::file('image')->getClientOriginalName();
                $request->image->move(base_path('public/Events'), $filename);
                $requestData['image']=$filename;
            }
            $requestData['status']=0;
            $requestData['created_by']='CLIENT';
            $requestData['creator_id']=Auth::guard('client')->user()->id;
            $event = Event::create($requestData);
            $requestData['event_id']=$event->id;
            EventAddress::create($requestData);
            EventTicket::create($requestData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect('client/events')->with('flash_message', 'Event added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $authuser=Auth::guard('client')->user();
        $event = Event::where('creator_id',$authuser->id)->find($id);
        $eventTickets=EventTicket::where('event_id',$id)->get();
        $eventAddress=EventAddress::where('event_id',$id)->first();
        if($event){
            return view('client.events.show', compact('event','eventTickets','eventAddress'));
        }
        else{
            return redirect()->back();
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $authuser=Auth::guard('client')->user();
        $event = Event::where('creator_id',$authuser->id)->findOrFail($id);
        $eventTicket=EventTicket::where('event_id',$id)->first();
        $eventAddress=EventAddress::where('event_id',$id)->first();

        return view('client.events.edit', compact('event','eventTicket','eventAddress'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $authuser=Auth::guard('client')->user();
        $requestData = $request->all();
        $event = Event::where('creator_id',$authuser->id)->findOrFail($id);
        $eventTicket=EventTicket::where('event_id',$id)->first();
        $eventAddress=EventAddress::where('event_id',$id)->first();
        $requestData['creator_id']=Auth::guard('client')->user()->id;
        DB::beginTransaction();
        try
        {
            if ($request->hasFile('image')) {
                $filename = Input::file('image')->getClientOriginalName();
                $request->image->move(base_path('public/Events'), $filename);
                $requestData['image']=$filename;
            }
            $event->update($requestData);
            $eventTicket->update($requestData);
            $eventAddress->update($requestData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }

        return redirect('client/events')->with('flash_message', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $authuser=Auth::guard('client')->user();
        $event = Event::where('creator_id',$authuser->id)->findOrFail($id);
        $event->eventAddress()->delete();
        $event->eventTicket()->delete();
        Event::destroy($id);
        return redirect('client/events')->with('flash_message', 'Event deleted!');
    }

    public function createdBy($id){
        $event = Event::findOrFail($id);
        if($event->created_by=='ADMIN'){
            $user=Admin::find($event->creator_id);

        }
        else{
            $user=Client::find($event->creator_id);
        }
        $usertype=$event->created_by;
        return view('client.events.createdBy', compact('user','usertype'));

    }

    public function addTicket($id){
        $eventId=base64_decode($id);
        $event = Event::where('creator_id',Auth::guard('client')->user()->id)->findOrFail($eventId);
        return view('client.events.addmoreticket',compact('event'));
    }

    public function createNewTicket(Request $request){
        $data=$request->all();
        EventTicket::create($data);
        return redirect('client/events')->with('flash_message', 'New Event Ticket Added Successfully!');
    }

    public function editTicket($id){
        $ticketId=base64_decode($id);
        $eventTicket = EventTicket::whereHas('event',function ($q){
            $q->where('creator_id',Auth::guard('client')->user()->id);
        })->findOrFail($ticketId);
        return view('client.events.edit_ticket',compact('eventTicket'));
    }

    public function saveeditTicket(Request $request,$id){
        $data=$request->all();
        $ticketId=base64_decode($id);
        $eventTicket = EventTicket::findOrFail($ticketId);
        $eventTicket->update($data);
        return redirect('client/events')->with('flash_message', 'Event Ticket Edited Successfully!');

    }

}
