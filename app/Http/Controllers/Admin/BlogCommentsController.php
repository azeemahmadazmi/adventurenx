<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\BlogComment;
use Illuminate\Http\Request;
use App\Blog;
class BlogCommentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blogcomments = BlogComment::where('user_name', 'LIKE', "%$keyword%")
                ->orWhere('user_email', 'LIKE', "%$keyword%")
                ->orWhere('message', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $blogcomments = BlogComment::latest()->paginate($perPage);
        }

        return view('admin.blog-comments.index', compact('blogcomments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $blog=Blog::where('status',1)->pluck('name','id');
        return view('admin.blog-comments.create',compact('blog'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'user_name' => 'required'
		]);
        $requestData = $request->all();
        
        BlogComment::create($requestData);

        return redirect('admin/blog-comments')->with('flash_message', 'BlogComment added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $blogcomment = BlogComment::findOrFail($id);

        return view('admin.blog-comments.show', compact('blogcomment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $blog=Blog::where('status',1)->pluck('name','id');
        $blogcomment = BlogComment::findOrFail($id);

        return view('admin.blog-comments.edit', compact('blogcomment','blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'user_name' => 'required'
		]);
        $requestData = $request->all();
        
        $blogcomment = BlogComment::findOrFail($id);
        $blogcomment->update($requestData);

        return redirect('admin/blog-comments')->with('flash_message', 'BlogComment updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BlogComment::destroy($id);

        return redirect('admin/blog-comments')->with('flash_message', 'BlogComment deleted!');
    }
}
