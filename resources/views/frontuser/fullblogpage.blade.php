@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative blog-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Blog Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">blog</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">blog</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="all-posts-wellness section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <!-- Filter Controls -->
                    <ul id="all-posts-wellness" class="uk-subnav blog-nav">
                        <li data-uk-filter="" class="uk-active"><a href="#">all post</a></li>
                        @if(isset($blogcategory) && $blogcategory->isNotEmpty())
                            @foreach($blogcategory as $bkk => $bvv)
                                <li data-uk-filter="filter-{{isset($bvv->blogcategory->name)?$bvv->blogcategory->name:''}}" class=""><a href="#">{{isset($bvv->blogcategory->name)?$bvv->blogcategory->name:''}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                    <!-- Dynamic Grid -->
                    <div id="display-blog-posts" class="border-b2" data-uk-grid="{controls: '#all-posts-wellness'}">
                        @if(isset($blogcategory) && $blogcategory->isNotEmpty())
                            @foreach($blogcategory as $bkk => $bvv)
                               <?php
                                    $blogdisplay=App\Blog::where('category',$bvv->category)->get();
                                ?>

                                <div data-uk-filter="filter-{{isset($bvv->blogcategory->name)?$bvv->blogcategory->name:''}}">
                                    @foreach($blogdisplay as $bk => $bv)
                                    <div class="uk-overlay blog-content-wellness">
                                        <img src="{{asset('Blog/'.$bv->image)}}" alt="Blog Content" width="1130px" height="675px">
                                        <div class="blog-info uk-overlay-panel uk-overlay-bottom">
                                            <h3 class="h-3">{{$bv->name}}</h3>
                                            <div class="title-line-lg"></div>
                                            <ul class="blog-info-list">
                                                <li><a class="post-author" href="#">{{$bv->created_by}}</a></li>
                                                <li><a class="post-date" href="#">{{$bv->date}}</a></li>
                                                <li>In: <a class="post-category">{{isset($bv->blogcategory->name)?$bv->blogcategory->name:''}}</a></li>
                                            </ul>
                                            {!!  substr($bv->about, 0, 300)!!}...
                                            <a href="{{url('/blogpage?blog_id='.base64_encode($bv->id))}}" class="text-overview">Read more<img class="icon-right" src="images/images_wellness/icon-arrow-right.png" alt="Read More Icon"></a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- Pagination Starts -->
                    {{--<div class="blog-pagination">--}}
                        {{--<ul id="blog-pagination" class="uk-pagination uk-pagination-right pagination">--}}
                            {{--<li class="btn-active"><a href="#">1</a></li>--}}
                            {{--<li><a href="#">2</a></li>--}}
                            {{--<li><a href="#">3</a></li>--}}
                            {{--<li>--}}
                                {{--<a class="uk-text-bold" href="#">Next <i class="uk-icon-angle-right"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    <!-- / .blog-pagination -->
                </div>
            </div>
        </div>
    </section>
@endsection