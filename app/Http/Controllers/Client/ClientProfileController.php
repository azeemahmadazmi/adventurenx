<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ClientProfile;
use Illuminate\Http\Request;
class ClientProfileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $clientprofile = ClientProfile::where('image', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('mobile', 'LIKE', "%$keyword%")
                ->orWhere('pan_card_number', 'LIKE', "%$keyword%")
                ->orWhere('pan_card_image', 'LIKE', "%$keyword%")
                ->orWhere('adhaar_card_number', 'LIKE', "%$keyword%")
                ->orWhere('adhaar_card_image', 'LIKE', "%$keyword%")
                ->orWhere('bank_account_number', 'LIKE', "%$keyword%")
                ->orWhere('bank_account_type', 'LIKE', "%$keyword%")
                ->orWhere('bank_holder_account_name', 'LIKE', "%$keyword%")
                ->orWhere('bank_ifsc_code', 'LIKE', "%$keyword%")
                ->orWhere('address', 'LIKE', "%$keyword%")
                ->orWhere('previous_expeience', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $clientprofile = ClientProfile::latest()->paginate($perPage);
        }

        return view('client.client-profile.index', compact('clientprofile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('client.client-profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'email' => 'required|unique:client_profiles',
			'mobile' => 'required|unique:client_profiles',
			'pan_card_number' => 'required|unique:client_profiles',
			'adhaar_card_number' => 'required|unique:client_profiles'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('pan_card_image')) {
            $requestData['pan_card_image'] = $request->file('pan_card_image')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('adhaar_card_image')) {
            $requestData['adhaar_card_image'] = $request->file('adhaar_card_image')
                ->store('uploads', 'public');
        }

        ClientProfile::create($requestData);

        return redirect('client/client-profile')->with('flash_message', 'ClientProfile added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $clientprofile = ClientProfile::findOrFail($id);

        return view('client.client-profile.show', compact('clientprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $clientprofile = ClientProfile::findOrFail($id);

        return view('client.client-profile.edit', compact('clientprofile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'email' => 'required|unique:client_profiles',
			'mobile' => 'required|unique:client_profiles',
			'pan_card_number' => 'required|unique:client_profiles',
			'adhaar_card_number' => 'required|unique:client_profiles'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('pan_card_image')) {
            $requestData['pan_card_image'] = $request->file('pan_card_image')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('adhaar_card_image')) {
            $requestData['adhaar_card_image'] = $request->file('adhaar_card_image')
                ->store('uploads', 'public');
        }

        $clientprofile = ClientProfile::findOrFail($id);
        $clientprofile->update($requestData);

        return redirect('client/client-profile')->with('flash_message', 'ClientProfile updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ClientProfile::destroy($id);

        return redirect('client/client-profile')->with('flash_message', 'ClientProfile deleted!');
    }
}
