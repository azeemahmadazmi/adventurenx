<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Gallery;
use Illuminate\Http\Request;
use App\Folder;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image as Image;
class GalleryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $gallery = Gallery::where('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhereHas('folder',function ($q) use($keyword){
                    $q->where('name', 'LIKE', "%$keyword%");
                })->latest()->paginate($perPage);
        } else {
            $gallery = Gallery::latest()->paginate($perPage);
        }

        return view('admin.gallery.index', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $folder=Folder::pluck('name','id');
        return view('admin.gallery.create',compact('folder'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'image' => 'required',
            'folder_id' =>'required'
		]);
        if ($files = $request->file('image')) {
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $file->move('galleryImage', $filename);
                $image_resize = Image::make('galleryImage/'.$filename);
                $image_resize->resize(360, 240);
                $image_resize->save(public_path('galleryImage/thumbimage/' .'thumb-'.$filename));
                $requestData['image']=$filename;
                $requestData['folder_id']=$request['folder_id'];
                $requestData['thumbimage']='thumb-'.$filename;
                $requestData['status']=1;
                Gallery::create($requestData);
            }
        }

        return redirect('admin/gallery')->with('flash_message', 'Gallery added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);

        return view('admin.gallery.show', compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $folder=Folder::pluck('name','id');
        $gallery = Gallery::findOrFail($id);

        return view('admin.gallery.edit', compact('gallery','folder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename =  $request->image->getClientOriginalName();
            $request->image->move('galleryImage', $filename);
            $image_resize = Image::make('galleryImage/'.$filename);
            $image_resize->resize(360, 240);
            $image_resize->save(public_path('galleryImage/thumbimage/' .'thumb-'.$filename));
            $requestData['image']=$filename;
            $requestData['thumbimage']='thumb-'.$filename;
        }
        $gallery = Gallery::findOrFail($id);
        $gallery->update($requestData);
        return redirect('admin/gallery')->with('flash_message', 'Gallery updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Gallery::destroy($id);

        return redirect('admin/gallery')->with('flash_message', 'Gallery deleted!');
    }
}
