@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            AboutusCamp {{ $aboutuscamp->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/aboutus-camp') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/aboutus-camp/' . $aboutuscamp->id . '/edit') }}"
                           title="Edit AboutusCamp">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/aboutuscamp' . '/' . $aboutuscamp->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete AboutusCamp"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $aboutuscamp->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $aboutuscamp->name }} </td>
                                </tr>
                                <tr>
                                    <th> About</th>
                                    <td> {{ $aboutuscamp->about }} </td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td><img src="{{ asset('aboutus/camp/'.$aboutuscamp->image) }}"></td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $aboutuscamp->status==1?"Actice":"Disabled" }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
