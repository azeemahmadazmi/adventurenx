@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            EventCategory {{ $eventcategory->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/event-categories') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/event-categories/' . $eventcategory->id . '/edit') }}"
                           title="Edit EventCategory">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/eventcategories' . '/' . $eventcategory->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete EventCategory"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $eventcategory->id }}</td>
                                </tr>
                                <tr>
                                    <th> Category Name</th>
                                    <td> {{ $eventcategory->name }} </td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td> <img src="{{asset('Events/EventsCategories/'.$eventcategory->image)}}" height="400px"> </td>
                                </tr>
                                <tr>
                                    <th> Descriptions</th>
                                    <td> {!! $eventcategory->descriptions !!} </td>
                                </tr>
                                <tr>
                                    <th> About</th>
                                    <td> {!! $eventcategory->about !!} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $eventcategory->status==1?'Active':'Disabled' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
