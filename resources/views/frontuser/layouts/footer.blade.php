<footer class="mega-footer">
    <div class="uk-container uk-container-center">
        <!-- footer-top -->
        <section class="footer-top">
            <div class="uk-grid" data-uk-grid-margin>
                <!-- Footer 1st Column
                    ===============================-->
                <div class="uk-width-medium-4-10 footer-contact-info">
                    <h4 class="h-4">location:</h4>
                    <div class="uk-cover-background uk-position-relative gmap-wrapper">
                        <img class="" src="https://l450v.alamy.com/450v/h1022n/mumbai-pinned-on-a-map-of-india-h1022n.jpg" alt="Google Location Map">
                        <div class="" id="gmap_canvas"></div>
                    </div>
                    {{--<ul class="contact-info">--}}
                        {{--<li><img class="icon-left" src="images/images_wellness/location-icon.png" alt="Location Icon">Mumbai,Maharashtra-India</li>--}}
                        {{--<li><img class="icon-left" src="images/images_wellness/phone-icon.png" alt="Phone Icon">+91-9969695882</li>--}}
                        {{--<li><img class="icon-left" src="images/images_wellness/msg-icon.png" alt="Email Icon"><a href="http://www.3jon.com/cdn-cgi/l/email-protection" class="" data-cfemail="b1d0c5dcd0eed4dcd0d8ddf1d4dcd0d8dd9fd2dedc">adventurenx@gmail.com</a></li>--}}
                    {{--</ul>--}}
                </div>
                <!-- Footer 2nd Column
                    ===============================-->
                <div class="uk-width-medium-3-10 uk-width-small-1-2">
                    <!-- our club -->
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <h4 class="h-4">our club:</h4>
                            <ul class="our-club uk-grid uk-grid-small uk-grid-width-1-3">
                                <li><a href="#"><img src="images/images_wellness/club_photo_01.png" alt="Club Photos"></a></li>
                                <li><a href="#"><img src="images/images_wellness/club_photo_02.png" alt="Club Photos"></a></li>
                                <li><a href="#"><img src="images/images_wellness/club_photo_03.png" alt="Club Photos"></a></li>
                            </ul>
                        </div>
                    </div><br><br>
                    <!-- working hours -->
                    <div class="working-hour">
                        <h4 class="h-4">Contact us:</h4>
                        {{--<ul class="contact-info">--}}
                            {{--<li><span class="uk-text-bold">Mon-Fri</span>from 8:00am to 9:00pm</li>--}}
                            {{--<li><span class="uk-text-bold">Sat-Sun</span>from 10:00am to 6:00pm</li>--}}
                        {{--</ul>--}}
                        <ul class="contact-info">
                            <li><img class="icon-left" src="images/images_wellness/location-icon.png" alt="Location Icon">Mumbai,Maharashtra-India</li>
                            <li><img class="icon-left" src="images/images_wellness/phone-icon.png" alt="Phone Icon">+91-9969695882</li>
                            <li><img class="icon-left" src="images/images_wellness/msg-icon.png" alt="Email Icon"><a href="http://www.3jon.com/cdn-cgi/l/email-protection" class="" data-cfemail="b1d0c5dcd0eed4dcd0d8ddf1d4dcd0d8dd9fd2dedc">adventurenx@gmail.com</a></li>
                        </ul>
                    </div>
                    <!-- classes -->
                    {{--<div class="uk-grid footer-classes">--}}
                        {{--<h4 class="h-4">classes:</h4>--}}
                        {{--<div class="uk-width-1-2">--}}
                            {{--<ul class="uk-list">--}}
                                {{--<li>--}}
                                    {{--<a class="uk-text-capitalize uk-text-bold" href="#">rafting<i class="uk-icon-angle-right icon-right"></i></a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a class="uk-text-capitalize uk-text-bold" href="#">siing<i class="uk-icon-angle-right icon-right"></i></a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a class="uk-text-capitalize uk-text-bold" href="#">snowboarding<i class="uk-icon-angle-right icon-right"></i></a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="uk-width-1-2">--}}
                            {{--<ul class="uk-list">--}}
                                {{--<li>--}}
                                    {{--<a class="uk-text-capitalize uk-text-bold" href="#">hiking<i class="uk-icon-angle-right icon-right"></i></a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a class="uk-text-capitalize uk-text-bold" href="#">cycling<i class="uk-icon-angle-right icon-right"></i></a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a class="uk-text-capitalize uk-text-bold" href="#">camping<i class="uk-icon-angle-right icon-right"></i></a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <!-- Footer 3rd Column
                    ===============================-->
                <div class="uk-width-medium-3-10 uk-width-small-1-2 footer-third-col">
                    <h4 class="h-4">social networks:</h4>
                    <ul class="social-networks uk-grid uk-grid-width-1-5">
                        <li><a href="#"><img src="images/images_wellness/facebook-icon.png" alt="Facebook Icon"></a></li>
                        <li><a href="#"><img src="images/images_wellness/twitter-icon.png" alt="Twitter Icon"></a></li>
                        <li><a href="#"><img src="images/images_wellness/instagram-icon.png" alt="Instagram Icon"></a></li>
                        <li><a href="#"><img src="images/images_wellness/vimo-icon.png" alt="Vimo Icon"></a></li>
                    </ul>
                    <h4 class="h-4">from instagram:</h4>
                    <ul class="instagram-images uk-grid uk-grid-small uk-grid-width-1-3" data-uk-grid-margin>
                        <li><a href="#"><img src="images/images_wellness/instagrame_01.png" alt="Instagram Images"></a></li>
                        <li><a href="#"><img src="images/images_wellness/instagrame_02.png" alt="Instagram Images"></a></li>
                        <li><a href="#"><img src="images/images_wellness/instagrame_03.png" alt="Instagram Images"></a></li>
                        <li><a href="#"><img src="images/images_wellness/instagrame_04.png" alt="Instagram Images"></a></li>
                        <li><a href="#"><img src="images/images_wellness/instagrame_05.png" alt="Instagram Images"></a></li>
                        <li><a href="#"><img src="images/images_wellness/instagrame_06.png" alt="Instagram Images"></a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- / footer-top end -->
        <div class="footer-bottom">
            <div class="uk-grid">
                <div class="uk-width-1-2 uk-float-left">
                    &copy; {{date('Y')}} <a href="{{url('/')}}">Adventure<b>NX</b></a>
                </div>
                <div class="uk-width-1-2 uk-float-right">
                    <a href="#back-to-top" data-uk-smooth-scroll>
                        <p><i class="uk-icon-angle-up"></i></p>
                        <p>Back to Top</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>