<header class="main-header">
    <!-- MAIN NAV START-->
    <nav class="uk-navbar main-nav-wellness">
        <div class="uk-container uk-container-center">
            <a href="{{url('/')}}" class="uk-navbar-brand"><img src="images/newlogo1.png" alt="main-logo"></a>
            <!-- main-menu -->
            <ul class="uk-navbar-nav uk-visible-large uk-float-right">
                <li  class="uk-parent" data-uk-dropdown>
                    <a href="{{url('/')}}">home</a>
                </li>
                <li><a href="{{url('/about')}}" @if(url()->current()==env('APP_URL').'/about') class="active" @endif>about</a></li>
                <li><a href="{{url('/allevents')}}" @if(url()->current()==env('APP_URL').'/events') class="active" @endif>events</a></li>
                <li><a href="{{url('/showImageGallery')}}" @if(url()->current()==env('APP_URL').'/gallery') class="active" @endif>gallery</a></li>
                <li><a href="{{url('/fullblogpage')}}" @if(url()->current()==env('APP_URL').'/fullblogpage') class="active" @endif>blogs</a></li>
                <li><a href="{{url('/contactus')}}" @if(url()->current()==env('APP_URL').'/contactus') class="active" @endif>contact</a></li>
                {{--<li class="header-cart" data-uk-dropdown="{pos:'bottom-center', delay: 10, remaintime: 400}">--}}
                    {{--<a href="{{url('/cart')}}"><img src="images/images_sport/cart-icon.png" alt="Shopping"></a>--}}
                {{--</li>--}}
                <li class="header-account" data-uk-dropdown>
                    <a href="#" @if(url()->current()==env('APP_URL').'/myaccount' || url()->current()==env('APP_URL').'/login' || url()->current()==env('APP_URL').'/register') class="active" @endif><img src="images/images_sport/account-icon.png" alt="Account"></a>
                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-small">
                        <ul class="uk-nav uk-nav-navbar">
                            @if(Auth::check())
                                <li><a href="{{url('/myaccount')}}"  @if(url()->current()==env('APP_URL').'/myaccount') class="active" @endif>My Account</a></li>
                                <li> <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form></li>
                                @else
                            <li><a href="{{url('/login')}}"  @if(url()->current()==env('APP_URL').'/login') class="active" @endif>Login</a></li>
                            <li><a href="{{url('/register')}}"  @if(url()->current()==env('APP_URL').'/register') class="active" @endif>Register</a></li>
                                @endif
                        </ul>
                    </div>
                </li>

            </ul>
            <!-- main-menu end -->
            <a href="#my-offcanvas" class="uk-navbar-toggle uk-hidden-large uk-float-right" data-uk-offcanvas></a>
            <div class="uk-navbar-content uk-navbar-center"></div>
            <!-- offcanvas -->
            <div id="my-offcanvas" class="uk-offcanvas">
                <div class="uk-offcanvas-bar">
                    <div class="uk-panel">
                        <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{multiple:true}">
                            <li  class="uk-parent" data-uk-dropdown>
                                <a href="{{url('/')}}">home</a>
                            </li>
                            <li><a href="{{url('/about')}}" @if(url()->current()==env('APP_URL').'/about') class="active" @endif>about</a></li>
                            <li><a href="{{url('/allevents')}}" @if(url()->current()==env('APP_URL').'/events') class="active" @endif>events</a></li>
                            <li><a href="{{url('/showImageGallery')}}" @if(url()->current()==env('APP_URL').'/gallery') class="active" @endif>gallery</a></li>
                            <li><a href="{{url('/fullblogpage')}}" @if(url()->current()==env('APP_URL').'/fullblogpage') class="active" @endif>blogs</a></li>
                            <li><a href="{{url('/contactus')}}"  @if(url()->current()==env('APP_URL').'/contactus') class="active" @endif>contact</a></li>
                            {{--<li class="header-cart"><a href="{{url('/cart')}}"><img src="images/images_sport/cart-icon-white.png" alt="Shopping"></a></li>--}}
                            <li class="uk-parent">
                                <a href="#"><img src="images/images_sport/account-icon.png" alt="Account"></a>
                                <ul class="uk-nav-sub uk-nav-parent-icon" data-uk-nav>
                                    @if(Auth::check())
                                        <li><a href="{{url('/myaccount')}}">My Account</a></li>
                                        <li> <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                @csrf
                                            </form></li>
                                    @else
                                        <li><a href="{{url('/login')}}">Login</a></li>
                                        <li><a href="{{url('/register')}}">Register</a></li>
                                    @endif
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- offcanvas end -->
        </div>
    </nav>
    <!-- / MAIN NAV END-->
    <!-- ===========================================================================
        HOME SLIDESHOW | WELLNESS
        ================================================================================-->
    @yield('banner')

    <!-- / SLIDESHOW END -->
</header>