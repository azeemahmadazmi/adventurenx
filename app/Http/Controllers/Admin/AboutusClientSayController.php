<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AboutusClientSay;
use Illuminate\Http\Request;
class AboutusClientSayController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $aboutusclientsay = AboutusClientSay::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('designation', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $aboutusclientsay = AboutusClientSay::latest()->paginate($perPage);
        }

        return view('admin.aboutus-client-say.index', compact('aboutusclientsay'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.aboutus-client-say.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'title' => 'required'
		]);
        $requestData = $request->all();
        
        AboutusClientSay::create($requestData);

        return redirect('admin/aboutus-client-say')->with('flash_message', 'AboutusClientSay added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $aboutusclientsay = AboutusClientSay::findOrFail($id);

        return view('admin.aboutus-client-say.show', compact('aboutusclientsay'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $aboutusclientsay = AboutusClientSay::findOrFail($id);

        return view('admin.aboutus-client-say.edit', compact('aboutusclientsay'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'title' => 'required'
		]);
        $requestData = $request->all();
        
        $aboutusclientsay = AboutusClientSay::findOrFail($id);
        $aboutusclientsay->update($requestData);

        return redirect('admin/aboutus-client-say')->with('flash_message', 'AboutusClientSay updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AboutusClientSay::destroy($id);

        return redirect('admin/aboutus-client-say')->with('flash_message', 'AboutusClientSay deleted!');
    }
}
