<?php

namespace App\Http\Controllers\FrontUser;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Banner;
use App\EventTicket;
use App\Event;
use App\EventAddress;
use App\Order;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontuser.index');
    }
    public function cart(Request $request){
        if(isset($request['event_id'])) {
            $eventId = base64_decode($request['event_id']);
            if (isset($request['event_id']))
                $event = Event::find($eventId);
            $eventAddress = EventAddress::where('event_id', $eventId)->first();
            $eventTickets = EventTicket::where('event_id', $eventId)->get();
            $eventTicketsTotalCount= EventTicket::where('event_id', $eventId)->count();

            $banner = Banner::where('name', '=', 'cart')->latest()->first();
            $eventSchedule = date('d/m/Y', strtotime($event->event_start_date)) . ' to ' . date('d/m/Y', strtotime($event->event_end_date));
            $eventTimeSchedule = date('h:i:A', strtotime($event->event_start_time)) . ' to ' . date('h:i:A', strtotime($event->event_end_time));
            return view('frontuser.cart', compact('banner', 'event', 'eventAddress', 'eventTickets', 'eventSchedule', 'eventTimeSchedule','eventTicketsTotalCount'));
        }
        else{
            return redirect('/allevents');
        }
    }


    public function myaccount(){
        Session::forget('totalAmount');
        Session::forget('final_orderID');
        $banner=Banner::where('name','=','myaccount')->latest()->first();
        $user=Auth::user();
        return view('frontuser.myaccount',compact('user','banner'));
    }
    public function editprofile(){
        $banner=Banner::where('name','=','myaccount')->latest()->first();
        $user=Auth::user();
        return view('frontuser.editprofile',compact('user','banner'));
    }
    public function saveuserprofile(Request $request){
        $this->validate($request, [
            'name' => 'required|string|min:3|max:255',
            'mobile' => 'required|digits:10|numeric|unique:users,mobile,'.Auth::user()->id,
            'address' => 'required|string|min:20',
        ]);
        $data=$request->all();
        $user=Auth::user();
        $user->name=$data['name'];
        $user->mobile=$data['mobile'];
        $user->address=$data['address'];
        $user->save();
        return redirect('/myaccount');
    }

    public function orderhistories(){

        $banner=Banner::where('name','=','myaccount')->latest()->first();
        $user=Auth::user();
        $orders=Order::where('user_id',$user->id)->latest()->get();
        $user=Auth::user();
        return view('frontuser.orderhistories',compact('user','banner','orders'));

    }

    public function showHistories(Request $request){
        $orderId=base64_decode($request['order_id']);
        $order=Order::where('user_id',Auth::user()->id)->findOrFail($orderId);
        $userDetails=unserialize($order->user_details);
        $banner=Banner::where('name','=','myaccount')->latest()->first();
        $user=Auth::user();
        return view('frontuser.view_history_order',compact('order','userDetails','banner','user'));
    }
}
