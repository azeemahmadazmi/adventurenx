@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Teammember {{ $teammember->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/teammembers') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/teammembers/' . $teammember->id . '/edit') }}" title="Edit Teammember">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/teammembers' . '/' . $teammember->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Teammember"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $teammember->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $teammember->name }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    <td> {{ $teammember->mobile }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $teammember->email }} </td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td><img src="{{asset('Teammember/'.$teammember->image)}}"></td>
                                </tr>
                                <tr>
                                    <th> Desingnation</th>
                                    <td> {{ $teammember->desingnation }} </td>
                                </tr>
                                <tr>
                                    <th> About</th>
                                    <td> {!! $teammember->about !!} </td>
                                </tr>
                                <tr>
                                    <th> Descriptions</th>
                                    <td> {!! $teammember->descriptions !!} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
