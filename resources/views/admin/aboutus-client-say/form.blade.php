 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($aboutusclientsay->title) ? $aboutusclientsay->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($aboutusclientsay->description) ? $aboutusclientsay->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="name" style="font-size:18px;">{{ 'Client Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($aboutusclientsay->name) ? $aboutusclientsay->name : ''}}" >
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="designation" style="font-size:18px;">{{ 'Client Designation' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="designation" type="text" id="designation" value="{{ isset($aboutusclientsay->designation) ? $aboutusclientsay->designation : ''}}" >
     {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($aboutusclientsay->status) && $aboutusclientsay->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
