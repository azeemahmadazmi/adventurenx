 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="name" class="form-control" id="name" >
    @foreach (json_decode('{"about": "About-Us", "events": "All Events","eventpage":"Event Details Page","gallery":"Gallery","blog":"Blog","blogpage":"Single Blog page","teammember":"Team Memeber","contactus":"Contact US","cart":"Cart","cartpayment":"Cart Payment","thankyou":"Thankyou Page","myaccount":"My Account","reciept":"Order Reciept"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($banner->name) && $banner->name == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($banner->image) ? $banner->image : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($banner->status) && $banner->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
