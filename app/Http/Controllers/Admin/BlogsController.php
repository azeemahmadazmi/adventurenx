<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Blog;
use Illuminate\Http\Request;
use App\BlogCategory;
use Illuminate\Support\Facades\Input;
class BlogsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blogs = Blog::where('name', 'LIKE', "%$keyword%")
                ->orWhere('created_by', 'LIKE', "%$keyword%")
                ->orWhere('category', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('about', 'LIKE', "%$keyword%")
                ->orWhere('descriptions', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $blogs = Blog::latest()->paginate($perPage);
        }

        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $blogcategory=BlogCategory::pluck('name','id');
        return view('admin.blogs.create',compact('blogcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'created_by' => 'required',
			'category' => 'required',
			'date' => 'required',
			'descriptions' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/Blog'), $filename);
            $requestData['image']=$filename;
        }
        
        Blog::create($requestData);

        return redirect('admin/blogs')->with('flash_message', 'Blog added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $blogcategory=BlogCategory::pluck('name','id');
        $blog = Blog::findOrFail($id);

        return view('admin.blogs.edit', compact('blog','blogcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'created_by' => 'required',
			'category' => 'required',
			'date' => 'required',
			'descriptions' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/Blog'), $filename);
            $requestData['image']=$filename;
        }
        $blog = Blog::findOrFail($id);
        $blog->update($requestData);

        return redirect('admin/blogs')->with('flash_message', 'Blog updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Blog::destroy($id);

        return redirect('admin/blogs')->with('flash_message', 'Blog deleted!');
    }
}
