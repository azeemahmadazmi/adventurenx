@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative rafting-breadcrumbs">
            <img class="uk-invisible" src="images/images_wellness/classes_wellness_breadcrumbs.jpg" alt="Classes Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">classes</a></li>
                        <li class="uk-active"><a href="#">rafting</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">rafting</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="rafting section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-medium-7-10 classes-description-wellness text-sixteen">
                    <h2 class="h-2">about rafting and kayaking extreme</h2>
                    <div class="title-line-lg"></div>
                    <div class="uk-cover uk-position-relative video-classes-wellness">
                        <img class="uk-invisible" src="images/images_wellness/rafting_description.jpg" alt="Video classes">
                        <video id="video-element" class="uk-position-cover video-object uk-invisible">
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/webm">
                        </video>
                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                            <button class="play-btn">
                                <img src="images/play_btn_wellness.png" alt="Play Button">
                            </button>
                        </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, nemore disputando vix eu, eu luptatum sapientem mei, brute legimus ex per. Cum ne menandri postulant repudiandae, elit blandit tincidunt ne vis, pri ex facete fabellas. Dolore nostrud has eu, prodesset maiestatis abhorreant et mea. Vim elitr tation et, sed agam liberavisse voluptatibus at. Pro ea insolens mnesarchum, pri modo iisque similique ex, vix sanctus insolens at.</p>
                    <ul class="classes-list">
                        <li>Cum ne menandri postulant repudiandae, elit blandit tincidunt </li>
                        <li>Dolore nostrud has eu, prodesset maiestatis abhorreant tation </li>
                        <li>Cum ne menandri postulant repudiandae, elit blandit tincidunt </li>
                        <li>Dolore nostrud has eu, prodesset maiestatis abhorreant tation </li>
                    </ul>
                </div>
                <aside class="uk-width-medium-3-10">
                    <h4 class="h-4">information</h4>
                    <div class="title-line-lg"></div>
                    <div class="aside-content-wrapper">
                        <img src="images/images_wellness/classes_slider_01.jpg" alt="Rafting">
                        <ul class="aside-navigation">
                            <li>
                                <img class="icon-left" src="images/images_wellness/location-icon.png" alt="Location Icon">
                                7508 Pine Street Reisterstown, MD 21136
                            </li>
                            <li>
                                <img class="icon-left" src="images/images_wellness/schedule-icon.png" alt="Schedule Icon">
                                mon-fri, 6pm - 9pm
                            </li>
                            <li>
                                <span class="post-subtitle-red icon-left">Trainer: </span>
                                <a class="post-author">Richard Daugherty</a>
                            </li>
                            <li>
                                <img class="icon-left" src="images/images_wellness/phone-icon.png" alt="Phone Icon">
                                (593) 119-7864
                            </li>
                        </ul>
                        <button class="uk-button btn-custom-black">book now</button>
                    </div>
                </aside>
                <div class="border-b2 uk-width-1-1 ml-25 section-padding-top">
                </div>
            </div>
        </div>
        <!-- / container -->
    </section>
    <!-- / RAFTING END -->
    <!-- ===========================================================================
        OTHER CLASSES
        ================================================================================-->
    <section class="other-classes-wellness section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">other classes</h2>
                    <div class="title-line-lg"></div>
                    <div class="classes-thumbnail-wellness" data-uk-slideset="{small: 2, medium: 3, large: 3}">
                        <div class="uk-slidenav-position">
                            <ul class="uk-grid uk-slideset">
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_04.jpg" alt="Rafting">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">skydriving</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_02.jpg" alt="Skiing">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">skiing</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="classes_description_wellness.html">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_03.jpg" alt="Cycling">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">cycling</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_01.jpg" alt="Rafting">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">rafting</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_02.jpg" alt="Skiing">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">skiing</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/classdescriptions')}}">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse" src="images/images_wellness/classes_slider_03.jpg" alt="Cycling">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">cycling</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slideset Initializer -->
        </div>
    </section>
@endsection