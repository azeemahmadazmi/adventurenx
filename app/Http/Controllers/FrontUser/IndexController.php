<?php

namespace App\Http\Controllers\FrontUser;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\HomePageBanner;
use App\Event;
use App\Gallery;
use App\Banner;
use App\Contactus;
use App\Subscriber;
use Illuminate\Support\Facades\Auth;
use App\BlogComment;
use App\CompanyPartner;
use App\EventCategory;
use App\Blog;
use App\AboutusCamp;
use App\AboutusActivity;
use App\About;
use App\Teammember;
use App\AboutusClientSay;
use App\Folder;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{

    public function index()
    {
        $homebanner=HomePageBanner::where('status',1)->get();
        $events=Event::where('status',1)->take(3)->latest()->get();
        $companyPartner=CompanyPartner::where('status',1)->get();
        $eventCategory=EventCategory::where('status',1)->get();
        $blog=Blog::where('status',1)->get();
        return view('frontuser.index',compact('homebanner','events','companyPartner','eventCategory','blog'));
    }
    public function about()
    {
        $clientSay=AboutusClientSay::where('status',1)->get();
        $teamMember=Teammember::where('status',1)->get();
        $about=About::where('status',1)->latest()->first();
        $aboutactivity=AboutusActivity::where('status',1)->get();
        $aboutCamp=AboutusCamp::where('status',1)->get();
        $banner=Banner::where('name','=','about')->latest()->first();
        $companyPartner=CompanyPartner::where('status',1)->get();
        return view('frontuser.about',compact('banner','aboutCamp','aboutactivity','about','teamMember','clientSay','companyPartner'));
    }
    public function all_classes(){
        return view('frontuser.all_classes');
    }
    public function classdescriptions(){
       // return view('frontuser.classdescriptions');
    }
    public function events(){
        $banner=Banner::where('name','=','events')->latest()->first();
        $events=Event::where('status',1)->take(6)->latest()->paginate(12);
        return view('frontuser.events',compact('banner','events'));
    }
    public function fullblogpage(){
        $blog=Blog::where('status',1)->get();
        $blogcategory=Blog::where('status',1)->groupBy('category')->get();
        $banner=Banner::where('name','=','blog')->latest()->first();
        return view('frontuser.fullblogpage',compact('banner','blog','blogcategory'));
    }
    public function blogpage(Request $request){
        $blogid=base64_decode($request['blog_id']);
        $bv=Blog::find($blogid);
        $blogComment=BlogComment::where('blog_id',$blogid)->get();
        $eventCategory=EventCategory::where('status',1)->get();
        $banner=Banner::where('name','=','blogpage')->latest()->first();
        return view('frontuser.blogpage',compact('banner','bv','eventCategory','blogComment'));
    }
    public function contactus(){
        $banner=Banner::where('name','=','contactus')->latest()->first();
        return view('frontuser.contactus',compact('banner'));
    }
    public function allproducts(){
        return view('frontuser.allproducts');
    }
    public function products(){
        return view('frontuser.products');
    }

    public function checkout(){
        return view('frontuser.checkout');
    }
    public function team_member(Request $request){
        $teamMembersDetail=Teammember::where('status',1)->get();
        $id=base64_decode($request['member_id']);
        $teamMember=Teammember::find($id);
        $banner=Banner::where('name','=','teammember')->latest()->first();
        return view('frontuser.team_member',compact('banner','teamMember','teamMembersDetail'));
    }

    public function showImageGallery(){
        $banner=Banner::where('name','=','gallery')->latest()->first();
        $folder=Folder::where('status',1)->get();
        return view('frontuser.folderGallery',compact('folder','banner'));
    }

    public function gallery(Request $request){
        $banner=Banner::where('name','=','gallery')->latest()->first();
        $galleryImages=Gallery::latest()->where('folder_id',$request['folder_id'])->paginate(16);
        return view('frontuser.gallery',compact('galleryImages','banner'));
    }

    public function contactusform(Request $request){
        $data=$request->all();
        $todayedate=date('Y-m-d');

        $contact=Contactus::where('email',$data['email'])->where('created_at','LIKE',"%$todayedate%")->first();
        if($contact){
            return 2;
        }
        $data['status']=1;
        $cont=Contactus::create($data);
        if($cont){
            return 1;
        }
        else{
            return 3;
        }

    }


    public function subscriberform(Request $request){
        $data=$request->all();
        $contact=Subscriber::where('email',$data['email'])->first();
        if($contact){
            return 2;
        }
        $data['status']=1;
        $cont=Subscriber::create($data);
        if($cont){
            return 1;
        }
        else{
            return 3;
        }

    }


    public function blogcomments(Request $request){
        $data=$request->all();
        $data['user_id']=Auth::user()->id;
        $data['blog_id']=$request['blog_id'];
        $data['user_name']=$data['name'];
        $data['user_email']=$data['email'];
        $data['status']=1;
        $bgvalue=BlogComment::create($data);
        $users=Auth::user();
        $html='';
        if($bgvalue){
            $html.='   <li>
                                        <div class="uk-comment">
                                            <div class="uk-grid uk-grid-small">
                                                <div class="comments-profile-img uk-width-1-10">';
                                                  if(isset($users->image) && $users->image !=null){
                                                      $html.=' <img src="'.$users->image.'" alt="Profile Picture" width="65px" height="65px">';
                                                       }
                                                        else{
                                                    $html.='<img src="'.asset("dummy-profile-pic.jpg").'" alt="Profile Picture" width="65px" height="65px">';
                                                  }
                                                $html.='</div>
                                                <div class="uk-width-9-10">
                                                    <div class="post-comment-body light-sky-blue">
                                                        <ul class="blog-info-list">
                                                            <li><a class="post-author" href="#">'.$bgvalue->user_name.'</a></li>
                                                            <li class="post-date">'.date('d/m/Y',strtotime($bgvalue->created_at)).'</li>
                                                            <li class="post-date">at: '.date('h:i:A',strtotime($bgvalue->created_at)).'</li>
                                                        </ul>
                                                        <p class="text-sixteen">'.$bgvalue->message.'</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>';
            return $html;
        }
        else{
            return $html;
        }

    }


    public function savecurrenteventurl(Request $request){
        $url=$request['urlvalue'];
        Session::put('eventCurrenrUrl',$url);
        echo 1;
    }



}
