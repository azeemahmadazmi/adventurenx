 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($teammember->name) ? $teammember->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="mobile" style="font-size:18px;">{{ 'Mobile' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="mobile" type="number" id="mobile" value="{{ isset($teammember->mobile) ? $teammember->mobile : ''}}" >
     {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

 <label for="email" style="font-size:18px;">{{ 'Email' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="email" type="email" id="email" value="{{ isset($teammember->email) ? $teammember->email : ''}}" >
     {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="email" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
     <div class="form-line">
         <input class="form-control" name="image" type="file" id="image" value="{{ isset($teammember->image) ? $teammember->image : ''}}">
         {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
     </div>
 </div>

 <label for="desingnation" style="font-size:18px;">{{ 'Desingnation' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="desingnation" type="text" id="desingnation" value="{{ isset($teammember->desingnation) ? $teammember->desingnation : ''}}" required>
     {!! $errors->first('desingnation', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="about" style="font-size:18px;">{{ 'About' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="about" type="textarea" id="about">{{ isset($teammember->about) ? $teammember->about : ''}}</textarea>
     {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="descriptions" style="font-size:18px;">{{ 'Descriptions' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="descriptions" type="textarea" id="descriptions" >{{ isset($teammember->descriptions) ? $teammember->descriptions : ''}}</textarea>
     {!! $errors->first('descriptions', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($teammember->status) && $teammember->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
