<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AboutusCamp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
class AboutusCampController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $aboutuscamp = AboutusCamp::where('name', 'LIKE', "%$keyword%")
                ->orWhere('about', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $aboutuscamp = AboutusCamp::latest()->paginate($perPage);
        }

        return view('admin.aboutus-camp.index', compact('aboutuscamp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.aboutus-camp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/aboutus/camp'), $filename);
            $requestData['image']=$filename;
        }

        AboutusCamp::create($requestData);

        return redirect('admin/aboutus-camp')->with('flash_message', 'AboutusCamp added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $aboutuscamp = AboutusCamp::findOrFail($id);

        return view('admin.aboutus-camp.show', compact('aboutuscamp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $aboutuscamp = AboutusCamp::findOrFail($id);

        return view('admin.aboutus-camp.edit', compact('aboutuscamp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/aboutus/camp'), $filename);
            $requestData['image']=$filename;
        }

        $aboutuscamp = AboutusCamp::findOrFail($id);
        $aboutuscamp->update($requestData);

        return redirect('admin/aboutus-camp')->with('flash_message', 'AboutusCamp updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AboutusCamp::destroy($id);

        return redirect('admin/aboutus-camp')->with('flash_message', 'AboutusCamp deleted!');
    }
}
