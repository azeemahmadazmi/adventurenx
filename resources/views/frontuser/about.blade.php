@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative about-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="About Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">about</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">about</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="we-are-wellness section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">we are adventurenx camp</h2>
                    <div class="title-line-lg"></div>
                </div>
                <div class="uk-width-1-1">
                    <div class="uk-grid uk-grid-width-small-1-1 uk-grid-width-medium-1-3 classes-thumbnail-wellness"
                         data-uk-grid-margin>
                        @if(isset($aboutCamp) && $aboutCamp->isNotEmpty())
                            @foreach($aboutCamp as $ck => $cv)
                                <div class="thumbnails-wrapper">
                                    <a class="thumb-link">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="thumb-img-wrapper overflow-hidden">
                                                <img class="uk-animation-scale uk-animation-reverse"
                                                     src="{{asset('aboutus/camp/'.$cv->image)}}"
                                                     alt="hiking and camping" width="348px" height="250px">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="caption-title-18">{{$cv->name}}</h4>
                                                    <div class="title-line-sm"></div>
                                                    <p class="caption-text-16">{{$cv->about}}</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </div>

                            @endforeach
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / WE ARE END -->
    <!-- ===========================================================================
        ACHIVEMENTS
        ================================================================================-->
    <div class="achivements">
        <div class="container-fluid uk-padding-remove">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div class="uk-cover-background uk-position-relative achivements-bg-wellness">
                        <img class="uk-invisible achivements-image" src="{{asset('images/images_wellness/achivements_bg.jpg')}}"
                             alt="Achivements Background">
                        <div class="uk-container uk-container-center uk-flex uk-flex-center uk-flex-middle uk-position-cover uk-flex-space-between">
                            @if(isset($aboutactivity) && $aboutactivity->isNotEmpty())
                                @foreach($aboutactivity as $ak => $av)
                            <div class="uk-flex uk-flex-column uk-flex-middle achivements-counter">
                                <p class="achivements-counts counter">{{$av->count}}</p>
                                <div class="title-line-lg"></div>
                                <p class="achivements-titles">{{$av->name}}</p>
                            </div>
                                @endforeach
                            @endif

                            <!-- 	</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ACHIVEMENTS END-->
    <!-- ===========================================================================
        PHYLOSOPHY
        ================================================================================-->
    <section class="philosophy section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                @if($about)
                    <div class="uk-width-medium-6-10 uk-text-center">
                        <img src="{{asset('aboutus/'.$about->image)}}" alt="philosophy" width="664px" height="428px">
                    </div>
                    <div class="uk-width-medium-4-10 uk-flex uk-flex-column uk-flex-center">
                        {!! $about->descriptions !!}
                    </div>
                    @else
                <div class="uk-width-medium-6-10 uk-text-center">
                    <img src="images/images_wellness/philosophy_img.jpg" alt="philosophy">
                </div>
                <div class="uk-width-medium-4-10 uk-flex uk-flex-column uk-flex-center">
                    <h2 class="h-2">our philosophy</h2>
                    <p class="text-slogan">“ We find in life only what they themselves invest in it ”</p>
                    <p class="text-sixteen">
                        Vim elitr tation et, sed agam liberavisse voluptatibus at. Pro ea insolens mnesarchum, pri modo
                        iisque similique ex, vix sanctus insolens at. Veniam singulis ea per, deserunt dissentiet ad
                        nec. Sed no clita fierent sensibus, quot quas docendi duo ea. Latine apeirian definitiones eu
                        has. Ut periculis reprehendunt nec, te natum eripuit
                    </p>
                </div>
                    @endif
            </div>
        </div>
    </section>
    <!-- PHYLOSOPHY END -->
    <!-- ===========================================================================
        TEAM
        ================================================================================-->
    <section class="our-team-wellness section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">our team</h2>
                    <div class="title-line-lg"></div>
                </div>
                <div class="trainers-thumbnail uk-width-1-1" data-uk-slideset="{small: 3, medium: 4, large: 4}">
                    <div class="uk-slidenav-position">
                        <ul class="uk-grid uk-slideset">
                            @if(isset($teamMember) && $teamMember->isNotEmpty())
                                @foreach($teamMember as $tk => $tv)
                            <li class="thumbnails-wrapper">
                                <a href="{{url('/about-team-member?member_id='.base64_encode($tv->id))}}">
                                    <figure class="uk-overlay uk-overlay-hover">
                                        <div class="thumb-img-wrapper">
                                            <img src="{{asset('Teammember/'.$tv->image)}}" alt="Team Member" width="256px" height="400px">
                                        </div>
                                        <figcaption
                                                class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom uk-flex uk-flex-center uk-flex-middle">
                                            <div class="figcaption-wrapper uk-text-left">
                                                <h4 class="trainers-name">{{$tv->name}}</h4>
                                                <div class="title-line-lg"></div>
                                                <p class="trainers-title">{{$tv->desingnation}}</p>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                                @endforeach
                                @endif

                        </ul>
                        <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                        <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ===========================================================================
        TESTIMONIALS
        ================================================================================-->
    <section class="testimonials-wellness">
        <div class="container-fluid uk-padding-remove">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div class="uk-slidenav-position" data-uk-slideshow>
                        <ul class="uk-slideshow">
                            @if(isset($clientSay) && $clientSay->isNotEmpty())
                                @foreach($clientSay as $ck => $cv)
                            <li>
                                <div class="uk-cover-background uk-position-relative">
                                    <img class="uk-invisible testimonials-image"
                                         src="{{asset('images/images_wellness/testimonials_bg_02.jpg')}}"
                                         alt="Testimonials Background">
                                    <figure class="uk-position-cover uk-container-center uk-container uk-flex uk-flex-middle">
                                        <div class="testimonials-slider-area-wellness">
                                            <h2 class="h-2">{{$cv->title}}</h2>
                                            <div class="title-line-lg"></div>
                                           {!! $cv->description !!}
                                            <div class="uk-grid">
                                                <div class="uk-width" style="width: 104px;">
                                                    <div class="clients-image">
                                                        <img src="{{asset('dummy-profile-pic.jpg')}}"
                                                             alt="Testimonials Client">
                                                    </div>
                                                </div>
                                                <div class="uk-width uk-flex uk-flex-column uk-flex-center uk-padding-remove"
                                                     style="width: 104px;">
                                                    <h4 class="client-name">{{$cv->name}}</h4>
                                                    <p class="client-title">{{$cv->designation}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </li>
                                @endforeach
                                @endif
                        </ul>
                        <ul class="uk-dotnav uk-dotnav-contrast uk-flex-column uk-flex-center uk-position-cover">
                            @if(isset($clientSay) && $clientSay->isNotEmpty())
                                @foreach($clientSay as $ck => $cv)
                             <li data-uk-slideshow-item="{{$ck}}"><a href="#"></a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ===========================================================================
        PARTNERS
        ================================================================================-->
    <div class="partners uk-text-center">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div data-uk-slideset="{autoplay: true, autoplayInterval: 5000, default: 2, small: 3, medium: 4, large: 5}">
                        <div class="uk-slidenav-position">
                            <ul class="uk-grid uk-slideset" data-uk-grid-match>
                                @if(isset($companyPartner) && $companyPartner->isNotEmpty())
                                    @foreach($companyPartner as $ck => $cv)
                                <li>
                                    <figure>
                                        <img src="{{asset('companyLogo/'.$cv->image)}}" alt="Partner's Logo" width="167px" height="81px">
                                    </figure>
                                </li>
                                    @endforeach
                                    @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection