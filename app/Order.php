<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
class Order extends Model
{
    use SoftDeletes;
    /* The database table used by the model.
    *
    * @var string
    */

    protected $table = 'orders';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'order_status', 'cart_total', 'service_charge', 'discount', 'tax','grand_total','payment_method','payment_status','cart','event_id','transaction_id','user_details'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }

    public function saveOrder($data){
            DB::beginTransaction();
            try {
                $order=Order::create($data);
                DB::commit();
                return $order->id;
            } catch (\Exception $e) {
                DB::rollback();
                return false;
            }

    }


}
