 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($clientprofile->image) ? $clientprofile->image : ''}}" required>
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($clientprofile->name) ? $clientprofile->name : ''}}" >
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="email" style="font-size:18px;">{{ 'Email' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="email" type="email" id="email" value="{{ isset($clientprofile->email) ? $clientprofile->email : ''}}" >
     {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="mobile" style="font-size:18px;">{{ 'Mobile' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="mobile" type="number" id="mobile" value="{{ isset($clientprofile->mobile) ? $clientprofile->mobile : ''}}" >
     {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="pan_card_number" style="font-size:18px;">{{ 'Pan Card Number' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="pan_card_number" type="text" id="pan_card_number" value="{{ isset($clientprofile->pan_card_number) ? $clientprofile->pan_card_number : ''}}" >
     {!! $errors->first('pan_card_number', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="pan_card_image" style="font-size:18px;">{{ 'Pan Card Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="pan_card_image" type="file" id="pan_card_image" value="{{ isset($clientprofile->pan_card_image) ? $clientprofile->pan_card_image : ''}}" >
     {!! $errors->first('pan_card_image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="adhaar_card_number" style="font-size:18px;">{{ 'Adhaar Card Number' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="adhaar_card_number" type="number" id="adhaar_card_number" value="{{ isset($clientprofile->adhaar_card_number) ? $clientprofile->adhaar_card_number : ''}}" >
     {!! $errors->first('adhaar_card_number', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="adhaar_card_image" style="font-size:18px;">{{ 'Adhaar Card Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="adhaar_card_image" type="file" id="adhaar_card_image" value="{{ isset($clientprofile->adhaar_card_image) ? $clientprofile->adhaar_card_image : ''}}" >
     {!! $errors->first('adhaar_card_image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="bank_account_number" style="font-size:18px;">{{ 'Bank Account Number' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="bank_account_number" type="text" id="bank_account_number" value="{{ isset($clientprofile->bank_account_number) ? $clientprofile->bank_account_number : ''}}" >
     {!! $errors->first('bank_account_number', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="bank_account_type" style="font-size:18px;">{{ 'Bank Account Type' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="bank_account_type" type="text" id="bank_account_type" value="{{ isset($clientprofile->bank_account_type) ? $clientprofile->bank_account_type : ''}}" >
     {!! $errors->first('bank_account_type', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="bank_holder_account_name" style="font-size:18px;">{{ 'Bank Holder Account Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="bank_holder_account_name" type="text" id="bank_holder_account_name" value="{{ isset($clientprofile->bank_holder_account_name) ? $clientprofile->bank_holder_account_name : ''}}" >
     {!! $errors->first('bank_holder_account_name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="bank_ifsc_code" style="font-size:18px;">{{ 'Bank Ifsc Code' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="bank_ifsc_code" type="text" id="bank_ifsc_code" value="{{ isset($clientprofile->bank_ifsc_code) ? $clientprofile->bank_ifsc_code : ''}}" >
     {!! $errors->first('bank_ifsc_code', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="address" style="font-size:18px;">{{ 'Address' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="address" type="textarea" id="address" >{{ isset($clientprofile->address) ? $clientprofile->address : ''}}</textarea>
     {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="previous_expeience" style="font-size:18px;">{{ 'Previous Expeience' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="previous_expeience" type="textarea" id="previous_expeience" >{{ isset($clientprofile->previous_expeience) ? $clientprofile->previous_expeience : ''}}</textarea>
     {!! $errors->first('previous_expeience', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($clientprofile->status) && $clientprofile->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
