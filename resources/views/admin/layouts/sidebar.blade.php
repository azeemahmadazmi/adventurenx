<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            @if(Auth::guard('admin')->user()->image)
                <img src="{{asset('images/profile_image/'.Auth::guard('admin')->user()->image)}}" width="48" height="48" alt="User" />
                @else
            <img src="{{asset('images/user.png')}}" width="48" height="48" alt="User" />
                @endif
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::guard('admin')->user()->name}}</div>
            <div class="email">{{Auth::guard('admin')->user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">

                    <li><a href="{{url('/admin/profile')}}"><i class="material-icons">person</i>Profile</a></li>
                    <li role="seperator" class="divider"></li>
                    <li><a href="{{url('/admin/logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                    <li role="seperator" class="divider"></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{url('admin/home')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>

            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>Home Page</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/admin/home-page-banners')}}">Home Page Banner</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="{{url('/admin/events')}}">Events</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">Editable Tables</a>--}}
                    {{--</li>--}}
                </ul>
            </li>


            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>Events</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/admin/events/create')}}">Create Event</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/events')}}">View Events</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/event-categories')}}">Event Categories</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>Orders History</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/admin/admin-events/orders')}}">Admin Events</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/client-events/orders')}}">Client Events</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>About us</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/admin/about')}}">Aboutus</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/aboutus-camp')}}">About Camp</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/aboutus-activities')}}">About Activities</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/aboutus-client-say')}}">What Client say?</a>
                    </li>
                    <li>
                        <a href="{{url('admin/teammembers')}}">
                            Team Members
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>Blogs</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/admin/blog-category')}}">Blog Category</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/blogs')}}">Blogs</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/blog-comments')}}">Blog Comments</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>Gallery</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/admin/folders')}}">Image Folder</a>
                    </li>
                    <li>
                        <a href="{{url('admin/gallery')}}">Gallery Image</a>
                    </li>

                </ul>
            </li>


            <li class="">
                <a href="{{url('admin/client-profile')}}">
                    <i class="material-icons">person</i>
                    <span>Clients Profile</span>
                </a>
            </li>
            <li class="">
                <a href="{{url('admin/user-profile')}}">
                    <i class="material-icons">person</i>
                    <span>Users Profile</span>
                </a>
            </li>

            <li class="">
                <a href="{{url('admin/banners')}}">
                    <i class="material-icons">photo</i>
                    <span>Banners</span>
                </a>
            </li>
            <li class="">
                <a href="{{url('admin/contactus')}}">
                    <i class="material-icons">phone</i>
                    <span>Contact US</span>
                </a>
            </li>
            <li class="">
                <a href="{{url('admin/subscriber')}}">
                    <i class="material-icons">import_contacts</i>
                    <span>Subscriptions</span>
                </a>
            </li>
            <li class="">
                <a href="{{url('admin/company-partner')}}">
                    <i class="material-icons">track_changes</i>
                    <span>Company Partner</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; {{date('Y')}} <a href="javascript:void(0);">Adventure<b>NX</b></a>.
        </div>
    </div>
    <!-- #Footer -->
</aside>