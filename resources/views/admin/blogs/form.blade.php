 <label for="name" style="font-size:18px;">{{ 'Blog Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($blog->name) ? $blog->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

 <label for="created_by" style="font-size:18px;">{{ 'Created By' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="created_by" type="text" id="created_by" value="{{ isset($blog->created_by) ? $blog->created_by : ''}}" >
     {!! $errors->first('created_by', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="image" style="font-size:18px;">{{ 'Blog Image' }}</label>
 <div class="form-group">
     <div class="form-line">
         <input class="form-control" name="image" type="file" id="image" value="{{ isset($gallery->image) ? $gallery->image : ''}}">
         {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
     </div>
 </div>
 <label for="category" style="font-size:18px;">{{ 'Category' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="category" class="form-control" id="category" >
    @foreach ($blogcategory as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($blog->category) && $blog->category == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="date" style="font-size:18px;">{{ 'Date' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control datepicker" name="date" type="text" id="date" value="{{ isset($blog->date) ? $blog->date : ''}}" required>
     {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="about" style="font-size:18px;">{{ 'About' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="about" type="textarea" id="about" >{{ isset($blog->about) ? $blog->about : ''}}</textarea>
     {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="descriptions" style="font-size:18px;">{{ 'Descriptions' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="descriptions" type="textarea" id="descriptions">{{ isset($blog->descriptions) ? $blog->descriptions : ''}}</textarea>
     {!! $errors->first('descriptions', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($blog->status) && $blog->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
