@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative trainer-profile-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="About Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">about</a></li>
                        <li class="uk-active"><a href="#">Team Member profile</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">{{$teamMember->name}} profile</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <div class="profile section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <aside class="uk-width-medium-3-10 trainer-profile-sidebar">
                    <div class="trainer-profile-sidebar-wrapper">
                        <img src="{{asset('Teammember/'.$teamMember->image)}}" alt="Trainer Profile Picture" width="256px" height="400px">
                        <ul class="trainer-social-profiles uk-grid uk-grid-width-1-4">
                            <li class="uk-display-inline-block">
                                <a href="#"><i class="uk-icon-facebook"></i></a>
                            </li>
                            <li class="uk-display-inline-block">
                                <a href="#"><i class="uk-icon-twitter"></i></a>
                            </li>
                            <li class="uk-display-inline-block">
                                <a href="#"><i class="uk-icon-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </aside>
                <div class="uk-width-medium-7-10">
                    <article class="uk-article about-trainer text-sixteen">
                        <h2 class="h-2">{{$teamMember->name}}</h2>
                        <p class="uk-article-meta">
                            <a class="post-category uk-text-capitalize"> {{$teamMember->email}}</a>
                        </p>
                        <hr class="uk-article-divider">
                        <p class="uk-article-lead">
                            <span><img class="icon-left" src="images/images_wellness/phone-icon.png" alt="Phone Icon"></span>
                            {{$teamMember->mobile}}
                        </p>
                        <hr class="uk-article-divider">
                        <h3> About {{$teamMember->name}}</h3>
                        {!!  $teamMember->about !!}
                        <h3>Descriptions</h3>
                        {!!  $teamMember->descriptions !!}

                    </article>
                </div>
            </div>
        </div>
    </div>
    <!-- ===========================================================================
        OTHER TRAINERS
        ================================================================================-->
    <section class="other-instructors-wellness light-sky-blue section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">other team member</h2>
                    <div class="title-line-lg"></div>
                    <div class="trainers-thumbnail"  data-uk-slideset="{small: 2, medium: 3, large: 4}">
                        <div class="uk-slidenav-position">
                            <ul class="uk-grid uk-slideset">
                                @if(isset($teamMembersDetail) && $teamMembersDetail->isNotEmpty())
                                    @foreach($teamMembersDetail as $tk => $tv)
                                <li class="thumbnails-wrapper">
                                    <a href="{{url('/about-team-member?member_id='.base64_encode($tv->id))}}">
                                        <figure class="uk-overlay uk-overlay-hover">
                                            <div class="thumb-img-wrapper">
                                                <img src="{{asset('Teammember/'.$tv->image)}}" alt="Team Member" width="256px" height="400px">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom uk-flex uk-flex-center uk-flex-middle">
                                                <div class="figcaption-wrapper uk-text-left">
                                                    <h4 class="trainers-name">{{$tv->name}}</h4>
                                                    <div class="title-line-lg"></div>
                                                    <p class="trainers-title">{{$tv->desingnation}}</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                                    @endforeach
                                    @endif

                            </ul>
                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection