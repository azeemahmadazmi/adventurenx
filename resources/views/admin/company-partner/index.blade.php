@extends('admin.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Companypartner
                        </h2>
                    </div>
                    <br>
                    <a href="{{ url('/admin/company-partner/create') }}" class="btn btn-success btn-sm waves-effect"
                       title="Add New CompanyPartner" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                    </a>
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/company-partner', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                                 <i class="material-icons" style="height: 27px !important;">search</i>
                             </button>
                              </span>
                    </div>
                    {!! Form::close() !!}
                    <div class="body">
                        <br>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companypartner as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td><img src="{{asset('companyLogo/'.$item->image)}}"></td>
                                        <td>{{ $item->status==1?'Active':'Disabled' }}</td>
                                        <td class="text-center">
                                            <a href="{{ url('/admin/company-partner/' . $item->id) }}"
                                               title="View CompanyPartner">
                                                <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                    View
                                                </button>
                                            </a>
                                            <a href="{{ url('/admin/company-partner/' . $item->id . '/edit') }}"
                                               title="Edit CompanyPartner">
                                                <button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i>
                                                    Edit
                                                </button>
                                            </a>

                                            <form method="POST"
                                                  action="{{ url('/admin/company-partner' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-xs"
                                                        title="Delete CompanyPartner"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="material-icons">delete</i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $companypartner->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
