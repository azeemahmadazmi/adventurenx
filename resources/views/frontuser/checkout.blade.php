@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative payment-breadcrumbs">
            <img class="uk-invisible" src="images/images_sport/payment.jpg" alt="About Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">shop</a></li>
                        <li><a href="#">shopping bag</a></li>
                        <li class="uk-active"><a href="#">payment</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">payment</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="payment-steps-form section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <form id="msform">
                        <div class="uk-grid">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active"><span>sign in</span></li>
                                <li><span>delivery</span></li>
                                <li><span>payment</span></li>
                                <li><span>confirmation</span></li>
                            </ul>
                        </div>
                        <!-- fieldsets -->
                        <div class="uk-grid uk-margin-top-remove">
                            <fieldset class="uk-width-medium-6-10 uk-width-large-4-10 payment-sign-in uk-form">
                                <h4 class="h-4">sign in</h4>
                                <div class="title-line-lg"></div>
                                <div class="uk-grid uk-grid-width-1-1 custom-grid-margin">
                                    <div><input class="input-custom uk-width-1-1" type="email" placeholder="E-mail"></div>
                                    <div><input class="input-custom uk-width-1-1" type="password" placeholder="Password"></div>
                                    <div><input class="uk-button btn-custom-black next uk-width-1-1" type="button" name="next" value="sign in"></div>
                                    <div>
                                        <p><span class="uk-text-bold">Forgot your password?</span>  or  <span class="uk-text-bold">Create an Account</span></p>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="uk-width-large-6-10 payment-delivery-info uk-form">
                                <h4 class="h-4">delivery information</h4>
                                <div class="title-line-lg"></div>
                                <div class="uk-grid custom-grid-margin">
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="First Name"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="Last Name"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="email" placeholder="E-mail"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="Phone Number"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="password" placeholder="Create Password"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="password" placeholder="Confirm Password"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="Country"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="City"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="Street / Apartment"></div>
                                    <div class="uk-width-small-1-2"><input class="input-custom uk-width-1-1" type="text" placeholder="Zip / Postcode"></div>
                                    <div class="uk-width-1-1"><input class="uk-button btn-custom-black next" type="button" name="next" value="apply"></div>
                                </div>
                            </fieldset>
                            <fieldset class="uk-width-medium-1-1 payment-payment uk-form">
                                <div class="uk-grid custom-grid-margin">
                                    <!-- order-summary -->
                                    <div class="uk-width-large-1-2 order-summary">
                                        <h4 class="h-4">your order summary</h4>
                                        <div class="title-line-lg"></div>
                                        <ul class="order-entries border-b2">
                                            <li class="uk-grid">
                                                <p class="uk-width-2-5">Gray sweatshirt with logo</p>
                                                <p class="uk-width-1-5 uk-text-center">Qty: 2</p>
                                                <p class="uk-width-1-5 uk-text-right">Total:</p>
                                                <p class="uk-width-1-5 uk-text-bold uk-text-right">$90</p>
                                            </li>
                                            <li class="uk-grid">
                                                <p class="uk-width-2-5">Man sleeveless t-shirt</p>
                                                <p class="uk-width-1-5 uk-text-center">Qty: 1</p>
                                                <p class="uk-width-1-5 uk-text-right">Total:</p>
                                                <p class="uk-width-1-5 uk-text-bold uk-text-right">$23</p>
                                            </li>
                                        </ul>
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2">
                                                Edit
                                            </div>
                                            <div class="uk-width-small-1-2">
                                                <ul class="cost-voucher">
                                                    <li class="uk-grid">
                                                        <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">item total</p>
                                                        <p class="uk-width-1-3 uk-text-bold uk-text-right">$113</p>
                                                    </li>
                                                    <li class="uk-grid">
                                                        <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">delivery</p>
                                                        <p class="uk-width-1-3 uk-text-bold uk-text-right">$10</p>
                                                    </li>
                                                    <li class="uk-grid">
                                                        <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">discount</p>
                                                        <p class="uk-width-1-3 uk-text-bold uk-text-right">$0</p>
                                                    </li>
                                                    <li class="uk-grid">
                                                        <p class="uk-width-2-3 uk-text-capitalize">+add discount code</p>
                                                        <p class="uk-width-1-3 uk-text-bold uk-text-right"></p>
                                                    </li>
                                                </ul>
                                                <p class="sum-total"><span>total</span><span class="uk-float-right">$123</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- payment-amount -->
                                    <div class="uk-width-large-1-2 payment-amount">
                                        <h4 class="h-4">payment amount</h4>
                                        <div class="title-line-lg"></div>
                                        <div class="card-info-form">
                                            <div class="uk-grid uk-grid-small custom-grid-margin" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                    <div class="uk-form-select uk-width-1-1" data-uk-form-select>
                                                        <span class="select-place-holder">Card Type</span>
                                                        <i class="uk-icon-angle-down uk-float-right"></i>
                                                        <select>
                                                            <option value="">Card Type</option>
                                                            <option value="">Card Type</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <input class="input-custom uk-width-1-1" type="text" placeholder="Card Number">
                                                </div>
                                                <div class="uk-width-1-1">
                                                    <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                                                        <div class="uk-width-small-1-4">
                                                            <div class="uk-form-select uk-width-1-1" data-uk-form-select>
                                                                <span class="select-place-holder">Month</span>
                                                                <i class="uk-icon-angle-down uk-float-right"></i>
                                                                <select>
                                                                    <option value="">Month</option>
                                                                    <option value="">Jan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-small-1-4">
                                                            <div class="uk-form-select uk-width-1-1" data-uk-form-select>
                                                                <span class="select-place-holder">Year</span>
                                                                <i class="uk-icon-angle-down uk-float-right"></i>
                                                                <select>
                                                                    <option value="">Year</option>
                                                                    <option value="">2016</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-small-1-2">
                                                            <input class="input-custom uk-width-1-1" type="text" placeholder="Security Code">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-1-1">
                                                    <p class="uk-text-bold">Need a help?</p>
                                                </div>
                                                <div class="uk-width-3-5">
                                                    <input class="uk-button btn-custom-black next" type="button" name="next" value="pay now">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="uk-width-8-10 payment-confirmation uk-form">
                                <figure class="uk-overlay">
                                    <img src="images/images_sport/payment-confirmation.jpg" alt="Payment Confirmation">
                                    <figcaption class="uk-overlay-panel uk-flex uk-flex-middle">
                                        <div>
                                            <h2 class="h-2">thank you</h2>
                                            <div class="title-line-lg"></div>
                                            <p>Your payment <span class="uk-text-bold">#12345</span> was successful</p>
                                            <p>Delivery information we sent on your e-mail</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </fieldset>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
