<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('event_id')->nullable();
            $table->string('event_ticket_name')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('minimum_per_booking')->nullable();
            $table->integer('maximum_per_booking')->nullable();
            $table->string('type');
            $table->integer('price')->nullable();
            $table->string('adventurenx_fees');
            $table->string('gateway_fees');
            $table->string('ticket_sale_start_date')->nullable();
            $table->string('ticket_sale_start_time')->nullable();
            $table->string('ticket_sale_end_date')->nullable();
            $table->string('ticket_sale_end_time')->nullable();
            $table->text('ticket_description')->nullable();
            $table->text('message_to_attendee')->nullable();
            $table->string('status');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_tickets');
    }
}
