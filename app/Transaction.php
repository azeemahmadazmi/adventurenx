<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    /* The database table used by the model.
    *
    * @var string
    */

    protected $table = 'transactions';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['razorpay_payment_id', 'order_id', 'status','user_id'];

    public function order(){
        return $this->belongsTo('App\Order');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
