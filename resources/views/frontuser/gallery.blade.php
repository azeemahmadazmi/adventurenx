@extends('frontuser.layouts.master')
@section('css')
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/light-gallery/css/lightgallery.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/style.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/themes/all-themes.css')}}" rel="stylesheet" />
    <style>
        .active * {
            color: white !important;
            font-size: 15px !important;
        }
        .pagination > li > span {
            padding: 6px 22px !important;
        }
    </style>
@endsection
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative blog-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Blog Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">gallery</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">gallery</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                @if(isset($galleryImages) && $galleryImages->isNotEmpty())
                                    @foreach($galleryImages as $gk => $gv)
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <a href="{{asset('galleryImage/'.$gv->image)}}" data-sub-html="Demo Description">
                                            <img class="img-responsive thumbnail" src="{{asset('galleryImage/thumbimage/'.$gv->thumbimage)}}" width="360px" height="240px">
                                        </a>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-wrapper pull-right"> {!! $galleryImages->appends($_GET)->render() !!} </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('plugins/light-gallery/js/lightgallery-all.js')}}"></script>
    <script src="{{asset('js/pages/medias/image-gallery.js')}}"></script>
@endsection