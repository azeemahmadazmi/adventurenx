@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative product-page-breadcrumbs">
            <img class="uk-invisible" src="images/images_sport/product_page_breadcrumbs.jpg" alt="Product Page Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">shop</a></li>
                        <li class="uk-active"><a href="#">product Page</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">product Page</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="product-features section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-6-10">
                    <div class="uk-grid">
                        <div class="uk-width-5-6 product-slider-carousel">
                            <!-- Place somewhere in the <body> of your page -->
                            <div class="flexslider flex-slider">
                                <ul class="slides">
                                    <li>
                                        <img src="images/images_sport/product_preview_01.jpg" alt="Product Image" />
                                    </li>
                                    <li>
                                        <img src="images/images_sport/product_preview_02.jpg" alt="Product Image" />
                                    </li>
                                    <li>
                                        <img src="images/images_sport/product_preview_03.jpg" alt="Product Image" />
                                    </li>
                                    <li>
                                        <img src="images/images_sport/product_preview_04.jpg" alt="Product Image" />
                                    </li>
                                </ul>
                            </div>
                            <div class="flexslider flex-carousel">
                                <ul class="slides">
                                    <li>
                                        <img src="images/images_sport/product_preview_01.jpg" alt="Product Image" />
                                    </li>
                                    <li>
                                        <img src="images/images_sport/product_preview_02.jpg" alt="Product Image" />
                                    </li>
                                    <li>
                                        <img src="images/images_sport/product_preview_03.jpg" alt="Product Image" />
                                    </li>
                                    <li>
                                        <img src="images/images_sport/product_preview_04.jpg" alt="Product Image" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-4-10">
                    <h3 class="h-3">gray sweatshirt with logo</h3>
                    <div class="title-line-lg"></div>
                    <p class="shop-item-price">$45</p>
                    <div class="uk-grid">
                        <div class="uk-width-8-10 product-filter">
                            <div class="uk-grid uk-grid-small" data-uk-grid-margin="{cls:'uk-margin-top'}">
                                <div class="uk-width-large-1-2">
                                    <div class="uk-form-select uk-width-1-1 form-select-border" data-uk-form-select>
                                        <span></span>
                                        <i class="uk-icon-angle-down uk-float-right"></i>
                                        <select>
                                            <option value="">Choose Color</option>
                                            <option value="">Grey</option>
                                            <option value="">Blue</option>
                                            <option value="">Black</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <div class="uk-form-select uk-width-1-1 form-select-border" data-uk-form-select>
                                        <span></span>
                                        <i class="uk-icon-angle-down uk-float-right"></i>
                                        <select>
                                            <option value="">Select Size</option>
                                            <option value="">M</option>
                                            <option value="">L</option>
                                            <option value="">S</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-1-1"><button class="uk-button btn-custom-black">Add to Cart</button></div>
                            </div>
                        </div>
                    </div>
                    <ul class="uk-tab" data-uk-tab="{connect:'#product-features-tabs'}">
                        <li><a href="#">Size &amp; Fit Information</a></li>
                        <li><a href="#">Product Description</a></li>
                    </ul>
                    <!-- This is the container of the content items -->
                    <ul id="product-features-tabs" class="uk-switcher uk-margin">
                        <li>
                            <ul class="product-features-list">
                                <li>Fits true to size, take your normal size</li>
                                <li>Cut for a slightly loose fit</li>
                                <li>Mid-weight, slightly stretchy fabric</li>
                                <li>Model is 175cm/ 5'9" and is wearing a UK size 8</li>
                            </ul>
                            <p class="text-sixteen uk-text-bold">Size Guide <i class="uk-icon-angle-right"></i></p>
                            <p class="text-sixteen border-b2 product-description">Organic swag hoodie twee humblebrag, hella artisan helvetica yr stumptown art party authentic tattooed chicharrones. Flannel flexitarian truffaut bitters. Lumbersexual wayfarers fanny pack swag cronut raw denim</p>
                            <p class="text-sixteen product-meta">Product Code: <a class="post-category">12345</a></p>
                            <p class="text-sixteen product-meta">Category: <a class="post-category">sport wear</a></p>
                            <p class="text-sixteen product-meta">Tags:
                                <a class="post-category">sport wear,</a>
                                <a class="post-category">sweatshirt,</a>
                                <a class="post-category">women,</a>
                                <a class="post-category">top,</a>
                                <a class="post-category">wear,</a>
                                <a class="post-category">casual</a>
                            </p>
                        </li>
                        <li>
                            <p class="text-sixteen">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus unde fugit quod, tempore enim obcaecati quam eius explicabo voluptates quo consequatur! Ad iste consequuntur dolorem, minima at cupiditate veniam saepe.
                            </p>
                            <ul class="product-features-list">
                                <li>Fits true to size, take your normal size</li>
                                <li>Cut for a slightly loose fit</li>
                                <li>Mid-weight, slightly stretchy fabric</li>
                                <li>Model is 175cm/ 5'9" and is wearing a UK size 8</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- ===========================================================================
        POPULAR PRODUCTS
        ================================================================================-->
    <section class="popular-products section-padding-top section-padding-bottom light-sky-blue">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2 uk-text-center">popular products</h2>
                    <div class="uk-grid uk-grid-width-medium-1-5 uk-grid-width-small-1-3 uk-flex-center products-thumbnail" data-uk-grid-margin>
                        <div class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden">
                                    <a href="{{url('/products')}}">
                                        <img class="uk-animation-hover uk-animation-scale uk-animation-reverse" src="images/images_sport/popular_product_01.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                            </figure>
                            <h5 class="h-5">trident composite Helmet</h5>
                            <div class="title-line-sm"></div>
                            <p class="shop-item-price uk-text-center">$136</p>
                            <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                        </div>
                        <div class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden">
                                    <a href="{{url('/products')}}">
                                        <img class="uk-animation-hover uk-animation-scale uk-animation-reverse" src="images/images_sport/popular_product_02.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                            </figure>
                            <h5 class="h-5">cataract carbon razor oar blade</h5>
                            <div class="title-line-sm"></div>
                            <p class="shop-item-price uk-text-center">$73</p>
                            <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                        </div>
                        <div class="thumbnails-wrapper">
                            <figure>
                                <!-- <button class="uk-button btn-custom-sale" type="button">sale</button> -->
                                <div class="overflow-hidden">
                                    <a href="{{url('/products')}}">
                                        <img class="uk-animation-hover uk-animation-scale uk-animation-reverse" src="images/images_sport/popular_product_03.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                            </figure>
                            <h5 class="h-5">lounger chair 2 colors</h5>
                            <div class="title-line-sm"></div>
                            <p class="shop-item-price uk-text-center">$128</p>
                            <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                        </div>
                        <div class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden">
                                    <a href="{{url('/products')}}">
                                        <img class="uk-animation-hover uk-animation-scale uk-animation-reverse" src="images/images_sport/popular_product_04.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                            </figure>
                            <h5 class="h-5">fitbit blaze smart fitness watch </h5>
                            <div class="title-line-sm"></div>
                            <p class="shop-item-price uk-text-center">$215</p>
                            <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                        </div>
                        <div class="thumbnails-wrapper">
                            <figure>
                                <div class="overflow-hidden">
                                    <a href="{{url('/products')}}">
                                        <img class="uk-animation-hover uk-animation-scale uk-animation-reverse" src="images/images_sport/popular_product_05.jpg" alt="Shop Item Images">
                                    </a>
                                </div>
                            </figure>
                            <h5 class="h-5">mud-spike for dh racing</h5>
                            <div class="title-line-sm"></div>
                            <p class="shop-item-price uk-text-center">$58</p>
                            <button class="uk-button uk-align-center btn-custom" type="button">Add to Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
