@extends('frontuser.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('wp-content/plugins/elementor/assets/css/frontend.min91ac.css')}}" />

    <style>
        .elementor-section {
            background: #af7c7c !important;
        }
    </style>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
@endsection
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative shopping-bag-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Shopping Bag Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">event</a></li>
                        <li class="uk-active"><a href="#">ticket purchase</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">payment Page</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <br><br>
    <section class="calculate-shopping section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-5-10 calculate-delivery">
                    {{--<p class="wall">Discount</p>--}}
                    {{--<form class="uk-form form-custom discount-form" id="couponform">--}}
                        {{--<div class="uk-grid uk-grid-small" data-uk-grid-margin>--}}
                            {{--<div class="uk-width-small-7-10">--}}
                                {{--<input class="input-custom uk-width-1-1" type="text" placeholder="Enter Promocode">--}}
                                {{--<strong style="color: red;display: none;" id="copondetails">This Coupon is not valid</strong>--}}
                            {{--</div>--}}
                            {{--<div class="uk-width-small-3-10">--}}
                                {{--<input class="uk-button btn-custom" type="button" value="Apply" id="couponbutton">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}

                    <h3>Event Ticket Details</h3>
                    <table>
                        <tr>
                            <th>Sr.No</th>
                            <th>Ticket Name</th>
                            <th>Unit Price</th>
                            <th>Tax</th>
                            <th>Service Charge</th>
                        </tr>
                        @php  $countcol=0;@endphp
                        @foreach ($qun as $qk => $qv)
                            <?php
                            $eventTicket = App\EventTicket::findOrFail($qv['event_ticket_id']);

                            if ($eventTicket->adventurenx_fees == 1) {
                                $taxevent = 0;
                            } else {
                                $taxevent = 2 * ($eventTicket->price * 1) / 100;
                            }

                            if ($eventTicket->gateway_fees == 1) {
                                $servicetaxevent = 0;
                            } else {
                                $servicetaxevent = 2 * ($eventTicket->price * 1) / 100;
                            }
                            $countcol++;

                            ?>
                        <tr>
                            <td>{{$countcol}}</td>
                            <td>{{$eventTicket->event_ticket_name}}</td>
                            <td>₹ {{$eventTicket->price}}</td>
                            <td>₹ {{$taxevent}}</td>
                            <td>₹ {{$servicetaxevent}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="uk-width-medium-5-10">
                    <div class="price-total wall">
                        <h4 class="h-4">price total</h4>
                        <div class="title-line-lg"></div>
                        <ul class="cost-voucher">
                            <li class="uk-grid">
                                <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">item total</p>
                                <p class="uk-width-1-3 uk-text-bold uk-text-right">₹ {{$totalPayment - $tax - $servicetax}}</p>
                            </li>
                            <li class="uk-grid">
                                <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">tax</p>
                                <p class="uk-width-1-3 uk-text-bold uk-text-right">₹ {{$tax}}</p>
                            </li>
                            <li class="uk-grid">
                                <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">Service charge</p>
                                <p class="uk-width-1-3 uk-text-bold uk-text-right">₹ {{$servicetax}}</p>
                            </li>
                            <li class="uk-grid">
                                <p class="uk-width-2-3 uk-text-uppercase uk-text-bold">discount</p>
                                <p class="uk-width-1-3 uk-text-bold uk-text-right">₹ 0</p>
                            </li>
                        </ul>
                        <p class="sum-total"><span>total</span><span class="uk-float-right">₹ {{$totalPayment}}</span></p>
                        <a href="#" class="uk-button btn-custom-black link-btn proceespayment">payment proceed</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="elementor-element elementor-element-46c11eb8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="46c11eb8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-row">
                <div class="elementor-element elementor-element-41151609 elementor-column elementor-col-100 elementor-top-column" data-id="41151609" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <section class="elementor-element elementor-element-6a0c561f elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="6a0c561f" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-element elementor-element-1ebbf40e elementor-column elementor-col-25 elementor-inner-column" data-id="1ebbf40e" data-element_type="column">
                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-45d70b64 elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="45d70b64" data-element_type="widget" data-widget_type="image-box.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img src="../wp-content/uploads/2019/07/privacy.html" title="" alt=""></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">We Protect Your Privacy</h3></div></div>		</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-1ea04018 elementor-column elementor-col-25 elementor-inner-column" data-id="1ea04018" data-element_type="column">
                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-37b4db43 elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="37b4db43" data-element_type="widget" data-widget_type="image-box.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="128" height="123" src="../wp-content/uploads/2019/07/guarantee.png" class="attachment-full size-full" alt=""></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">100% Satisfaction Guaranteed</h3></div></div>		</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-5aeb2f7b elementor-column elementor-col-25 elementor-inner-column" data-id="5aeb2f7b" data-element_type="column">
                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-67557b14 elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="67557b14" data-element_type="widget" data-widget_type="image-box.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img src="../wp-content/uploads/2019/07/secure.html" title="" alt=""></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Your Data is Safe With Us</h3></div></div>		</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-d64e24c elementor-column elementor-col-25 elementor-inner-column" data-id="d64e24c" data-element_type="column">
                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-3ede4459 elementor-widget elementor-widget-image" data-id="3ede4459" data-element_type="widget" data-widget_type="image.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img width="300" height="216" src="../wp-content/uploads/2019/07/security-public.png" class="attachment-large size-large" alt="">											</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        var thankyouUrl = '{{url('/razor-thank-you')}}';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('body').on('click', '.proceespayment', function(e){
            e.preventDefault();
            $.ajax({
                url: '/createorder',
                type: 'post',
                success: function (msg) {
                    if(msg.status==true){
                            var cart_total = msg.total_amount;
                            var service_charge = msg.service_charge;
                            var discount = msg.discount;
                            var tax = msg.tax;
                            var grand_total = msg.grand_total_amount;
                            var cart = msg.cart;
                            var event_id = msg.event_id;
                            var user_id = msg.user_id;
                        var options = {
                            "key": "rzp_test_z66rmHMx6WY522",
                            "amount": (grand_total*100), // 2000 paise = INR 20
                            "name": "AdventureNX",
                            "description": "Paymet",
                            "image": '/IMG_20190814_111324_413.jpg',
                            "handler": function (response){
                                $.ajax({
                                    url: '/paysuccess',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {
                                        razorpay_payment_id: response.razorpay_payment_id ,
                                        cart_total : cart_total,
                                        service_charge : service_charge,
                                        discount : discount,
                                        tax : tax,
                                        grand_total : grand_total,
                                        cart : cart,
                                        event_id : event_id,
                                        user_id :user_id,
                                        payment_method:'razorpay'
                                    },
                                    success: function (msg) {
                                        if(msg.status==true){
                                            console.log(response);
                                            window.location.replace(thankyouUrl);
                                        }
                                        else{

                                        }

                                    }
                                });

                            },
                            "prefill": {
                                "contact": '9969695882',
                                "email":   'adventurenx@gmail.com',
                            },
                            "theme": {
                                "color": "#af7c7c"
                            }
                        };
                        var rzp1 = new Razorpay(options);
                        rzp1.open();
                        e.preventDefault();

                    }
                    else{

                    }
                }
            });
        });
        // document.getElementsClass('buy_plan1').onclick = function(e){
        //  rzp1.open();
        //  e.preventDefault();
        //  }


        $('#couponbutton').click(function () {
             $('#copondetails').show();
             $('#couponform').trigger('reset');
            $('#copondetails').delay(2200).fadeOut(200);
        });
    </script>
    @endsection
