<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Teammember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
class TeammembersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $teammembers = Teammember::where('name', 'LIKE', "%$keyword%")
                ->orWhere('mobile', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('desingnation', 'LIKE', "%$keyword%")
                ->orWhere('about', 'LIKE', "%$keyword%")
                ->orWhere('descriptions', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $teammembers = Teammember::latest()->paginate($perPage);
        }

        return view('admin.teammembers.index', compact('teammembers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.teammembers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'mobile' => 'required',
			'email' => 'required|email',
			'desingnation' => 'required',
			'about' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/Teammember'), $filename);
            $requestData['image']=$filename;
        }
        
        Teammember::create($requestData);

        return redirect('admin/teammembers')->with('flash_message', 'Teammember added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $teammember = Teammember::findOrFail($id);

        return view('admin.teammembers.show', compact('teammember'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $teammember = Teammember::findOrFail($id);

        return view('admin.teammembers.edit', compact('teammember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'mobile' => 'required',
			'email' => 'required|email',
			'desingnation' => 'required',
			'about' => 'required'
		]);
        $requestData = $request->all();
        
        $teammember = Teammember::findOrFail($id);
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/Teammember'), $filename);
            $requestData['image']=$filename;
        }
        $teammember->update($requestData);

        return redirect('admin/teammembers')->with('flash_message', 'Teammember updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Teammember::destroy($id);

        return redirect('admin/teammembers')->with('flash_message', 'Teammember deleted!');
    }
}
