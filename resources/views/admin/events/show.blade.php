@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Event {{ $event->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/events') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/events/' . $event->id . '/edit') }}" title="Edit Event">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>
                        <form method="POST" action="{{ url('admin/events' . '/' . $event->id) }}" accept-charset="UTF-8"
                              style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Event"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <h3 style="color:deeppink">Event Details :</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $event->id }}</td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td><a href="{{url('Events/'.$event->image)}}" target="_blank"><img
                                                    src="{{asset('Events/'.$event->image)}}" width="200px"
                                                    height="200px"></a></td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $event->event_name }} </td>
                                </tr>
                                <tr>
                                    <th> Event Display Name</th>
                                    <td> {{ $event->event_display_name }} </td>
                                </tr>

                                <tr>
                                    <th> Event Visibility</th>
                                    <td> {{ $event->event_visibility==1?'Public':'Private' }} </td>
                                </tr>
                                <tr>
                                    <th> Event Start Date</th>
                                    <td> {{ $event->event_start_date}} </td>
                                </tr>
                                <tr>
                                    <th> Event Start Time</th>
                                    <td> {!! date('h:i A', strtotime($event->event_start_time)) !!} </td>
                                </tr>
                                <tr>
                                    <th> Event End Data</th>
                                    <td> {{ $event->event_end_date}} </td>
                                </tr>
                                <tr>
                                    <th> Event End Time</th>
                                    <td> {!! date('h:i A', strtotime($event->event_end_time)) !!} </td>
                                </tr>
                                <tr>
                                    <th> Location</th>
                                    <td> {{ $event->location  }} </td>
                                </tr>
                                <tr>
                                    <th> Event Description</th>
                                    <td> {!! $event->event_descriptions  !!} </td>
                                </tr>

                                </tbody>
                            </table>
                            <h3 style="color:deeppink">Event Address :</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th> Event Venue</th>
                                        <td>{{$eventAddress->event_venue}}</td>
                                    </tr>
                                    <tr>
                                        <th> Event Street Line 1</th>
                                        <td>{!! $eventAddress->event_street_line_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th> Event Street Line 1</th>
                                        <td> {!! $eventAddress->event_street_line_2 !!} </td>
                                    </tr>

                                    <tr>
                                        <th> City</th>
                                        <td> {{$eventAddress->city }} </td>
                                    </tr>

                                    </tbody>
                                </table>

                                @if(isset($eventTickets) && $eventTickets->isNotEmpty())
                                    @foreach($eventTickets as $key => $eventTicket)

                                <h3 style="color:deeppink">Event Ticket Details : {{$key+1}} <span><a href="{{url('admin/events/editTicket/'.base64_encode($eventTicket->id))}}"><button class="btn btn-success btn-xs pull-right"><i class="material-icons">mode_edit</i> Edit</button></a></span></h3>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <tbody>
                                        <tr>
                                            <th> Event Ticket Name</th>
                                            <td>{{$eventTicket->event_ticket_name}}</td>
                                        </tr>
                                        <tr>
                                            <th> Price (in Rs)</th>
                                            <td> {{$eventTicket->price }} </td>
                                        </tr>
                                        <tr>
                                            <th> Quantity</th>
                                            <td>{!! $eventTicket->quantity !!}</td>
                                        </tr>
                                        <tr>
                                            <th> Minimum per Booking</th>
                                            <td> {!! $eventTicket->minimum_per_booking !!} </td>
                                        </tr>

                                        <tr>
                                            <th> Maximum Per Booking</th>
                                            <td> {{$eventTicket->maximum_per_booking }} </td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<th> Event Type</th>--}}
                                            {{--<td>{{$eventTicket->type==1?'Normal':'Donation'}}</td>--}}
                                        {{--</tr>--}}
                                        <tr>
                                            <th> Adventurenx Fees paid by</th>
                                            <td>{{$eventTicket->adventurenx_fees==1?'ME':'Attendee'}}</td>
                                        </tr>
                                        <tr>
                                            <th> Gateway Fees paid by</th>
                                            <td>{{$eventTicket->gateway_fees==1?'ME':'Attendee'}}</td>
                                        </tr>

                                        <tr>
                                            <th> Ticket Sale Start Date</th>
                                            <td> {!! $eventTicket->ticket_sale_start_date !!} </td>
                                        </tr>

                                        <tr>
                                            <th> Ticket Sale Start Time</th>
                                            <td> {!! date('h:i A', strtotime($eventTicket->ticket_sale_start_time)) !!} </td>
                                        </tr>

                                        <tr>
                                            <th> Ticket Sale End Date</th>
                                            <td> {!! $eventTicket->ticket_sale_end_date !!} </td>
                                        </tr>

                                        <tr>
                                            <th> Ticket Sale End Time</th>
                                            <td> {!! date('h:i A', strtotime($eventTicket->ticket_sale_end_time)) !!} </td>
                                        </tr>

                                        <tr>
                                            <th> Ticket Description</th>
                                            <td> {!! $eventTicket->ticket_description !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Message To Attendee</th>
                                            <td> {!! $eventTicket->message_to_attendee !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Term and Conditions</th>
                                            <td> {!! $eventTicket->term_and_conditions !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Form Fields</th>
                                            <?php
                                                $form_fields=unserialize($eventTicket->form_fields);
                                            ?>
                                            <td> Name <br> Email <br>
                                                @if(!empty($form_fields))
                                                    @foreach($form_fields as $value)
                                                        {{ucwords(str_replace('form','',str_replace('_',' ',$value)))}}<br>
                                                        @endforeach
                                                    @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    @endforeach
                                    @endif
                                </div>


                            <h3 style="color:deeppink">Event Status :</h3>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover">
                                            <tbody>
                                            <tr>
                                                <th> Created By</th>
                                                <td>
                                                    <a href="{{url('admin/events/createdby/'.$event->id)}}">{{$event->created_by }}</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th> Status</th>
                                                <td> {!! $event->status==1?'<strong style="color:green">Enabled</strong>':'<strong style="color:red">Disabled</strong>'  !!} </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <h3 style="color:deeppink">Event Location Map :</h3>
                                        <?php
                                            $eventLocation=$eventAddress->event_venue.' '.$eventAddress->event_street_line_1.' '. $eventAddress->event_street_line_2.' '.$eventAddress->city;
                                        ?>
                                        <div class="mapouter">
                                            <div class="gmap_canvas">
                                                 <iframe width="876" height="608" id="gmap_canvas" src="https://maps.google.com/maps?q={{urlencode($eventLocation)}}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                                 </iframe><a href="https://www.embedgooglemap.org"></a>
                                            </div><style>.mapouter{position:relative;text-align:right;height:608px;width:876px;}.gmap_canvas {overflow:hidden;background:none!important;height:608px;width:876px;}</style>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
