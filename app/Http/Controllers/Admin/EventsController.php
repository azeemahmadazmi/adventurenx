<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Admin;
use App\Client;
use App\Event;
use App\EventAddress;
use App\EventTicket;
class EventsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $events = Event::where('event_name', 'LIKE', "%$keyword%")
                ->orWhere('event_display_name', 'LIKE', "%$keyword%")
                ->orWhere('event_start_date', 'LIKE', "%$keyword%")
                ->orWhere('event_end_date', 'LIKE', "%$keyword%")
                ->orWhere('start_time', 'LIKE', "%$keyword%")
                ->orWhere('event_venue', 'LIKE', "%$keyword%")
                ->orWhere('event_descriptions', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $events = Event::latest()->paginate($perPage);
        }

        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.events.create');
    }


    public function store(Request $request)
    {
        $requestData=$request->all();
        $formDetails=array();
        if(isset($requestData['form_mobile'])){
            array_push($formDetails,$requestData['form_mobile']);
        }
        if(isset($requestData['form_title'])){
            array_push($formDetails,$requestData['form_title']);
        }
        if(isset($requestData['form_gender'])){
            array_push($formDetails,$requestData['form_gender']);
        }
        if(isset($requestData['form_designations'])){
            array_push($formDetails,$requestData['form_designations']);
        }
        if(isset($requestData['form_organisations'])){
            array_push($formDetails,$requestData['form_organisations']);
        }
        if(isset($requestData['form_address'])){
            array_push($formDetails,$requestData['form_address']);
        }
        if(isset($requestData['form_city'])){
            array_push($formDetails,$requestData['form_city']);
        }
        if(isset($requestData['form_state'])){
            array_push($formDetails,$requestData['form_state']);
        }
        if(isset($requestData['form_country'])){
            array_push($formDetails,$requestData['form_country']);
        }
        if(isset($requestData['form_pincode'])){
            array_push($formDetails,$requestData['form_pincode']);
        }
        if(isset($requestData['form_emergency_contact_number'])){
            array_push($formDetails,$requestData['form_emergency_contact_number']);
        }
        if(isset($requestData['form_emergency_contact_name'])){
            array_push($formDetails,$requestData['form_emergency_contact_name']);
        }
        if(isset($requestData['form_emergency_contact_relations'])){
            array_push($formDetails,$requestData['form_emergency_contact_relations']);
        }
        if(isset($requestData['form_dob'])){
            array_push($formDetails,$requestData['form_dob']);
        }
        if(isset($requestData['form_blood_group'])){
            array_push($formDetails,$requestData['form_blood_group']);
        }
        if(isset($requestData['form_tshirt_size'])){
            array_push($formDetails,$requestData['form_tshirt_size']);
        }
        if(isset($requestData['form_attendee_photo'])){
            array_push($formDetails,$requestData['form_attendee_photo']);
        }
        if(isset($requestData['form_attende_id_proof'])){
            array_push($formDetails,$requestData['form_attende_id_proof']);
        }
        $requestData['form_fields']=serialize($formDetails);
         DB::beginTransaction();
        try
        {
            if ($request->hasFile('image')) {
                $filename = Input::file('image')->getClientOriginalName();
                $request->image->move(base_path('public/Events'), $filename);
                $requestData['image']=$filename;
            }
            $requestData['status']=0;
            $requestData['created_by']='ADMIN';
            $requestData['creator_id']=Auth::guard('admin')->user()->id;
            $event = Event::create($requestData);
            $requestData['event_id']=$event->id;
            EventAddress::create($requestData);
            EventTicket::create($requestData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect('admin/events')->with('flash_message', 'Event added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

        $event = Event::findOrFail($id);
        $eventTickets=EventTicket::where('event_id',$id)->get();
        $eventAddress=EventAddress::where('event_id',$id)->first();

        return view('admin.events.show', compact('event','eventTickets','eventAddress'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $eventTicket=EventTicket::where('event_id',$id)->first();
        $eventAddress=EventAddress::where('event_id',$id)->first();
        $formfield=unserialize($eventTicket->form_fields);
        return view('admin.events.edit', compact('event','eventTicket','eventAddress','formfield'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $formDetails=array();
        if(isset($requestData['form_mobile'])){
            array_push($formDetails,$requestData['form_mobile']);
        }
        if(isset($requestData['form_title'])){
            array_push($formDetails,$requestData['form_title']);
        }
        if(isset($requestData['form_gender'])){
            array_push($formDetails,$requestData['form_gender']);
        }
        if(isset($requestData['form_designations'])){
            array_push($formDetails,$requestData['form_designations']);
        }
        if(isset($requestData['form_organisations'])){
            array_push($formDetails,$requestData['form_organisations']);
        }
        if(isset($requestData['form_address'])){
            array_push($formDetails,$requestData['form_address']);
        }
        if(isset($requestData['form_city'])){
            array_push($formDetails,$requestData['form_city']);
        }
        if(isset($requestData['form_state'])){
            array_push($formDetails,$requestData['form_state']);
        }
        if(isset($requestData['form_country'])){
            array_push($formDetails,$requestData['form_country']);
        }
        if(isset($requestData['form_pincode'])){
            array_push($formDetails,$requestData['form_pincode']);
        }
        if(isset($requestData['form_emergency_contact_number'])){
            array_push($formDetails,$requestData['form_emergency_contact_number']);
        }
        if(isset($requestData['form_emergency_contact_name'])){
            array_push($formDetails,$requestData['form_emergency_contact_name']);
        }
        if(isset($requestData['form_emergency_contact_relations'])){
            array_push($formDetails,$requestData['form_emergency_contact_relations']);
        }
        if(isset($requestData['form_dob'])){
            array_push($formDetails,$requestData['form_dob']);
        }
        if(isset($requestData['form_blood_group'])){
            array_push($formDetails,$requestData['form_blood_group']);
        }
        if(isset($requestData['form_tshirt_size'])){
            array_push($formDetails,$requestData['form_tshirt_size']);
        }
        if(isset($requestData['form_attendee_photo'])){
            array_push($formDetails,$requestData['form_attendee_photo']);
        }
        if(isset($requestData['form_attende_id_proof'])){
            array_push($formDetails,$requestData['form_attende_id_proof']);
        }
        $requestData['form_fields']=serialize($formDetails);

        $event = Event::findOrFail($id);
        $eventTicket=EventTicket::where('event_id',$id)->first();
        $eventAddress=EventAddress::where('event_id',$id)->first();
        $requestData['creator_id']=Auth::guard('admin')->user()->id;
        DB::beginTransaction();
        try
        {
            if ($request->hasFile('image')) {
                $filename = Input::file('image')->getClientOriginalName();
                $request->image->move(base_path('public/Events'), $filename);
                $requestData['image']=$filename;
            }
            $event->update($requestData);
            $eventTicket->update($requestData);
            $eventAddress->update($requestData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }

        return redirect('admin/events')->with('flash_message', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->eventAddress()->delete();
        $event->eventTicket()->delete();
        Event::destroy($id);
        return redirect('admin/events')->with('flash_message', 'Event deleted!');
    }

    public function createdBy($id){
        $event = Event::findOrFail($id);
        if($event->created_by=='ADMIN'){
            $usertype=$event->created_by;
            $user=Admin::find($event->creator_id);
            return view('admin.events.createdBy', compact('user','usertype'));
        }
        else{
            $user=Client::find($event->creator_id);
            return redirect('admin/client-profile/'.$user->id);
        }



    }

    public function chnageeventstatus(Request $request){
        $event_id=$request['event_id'];
        $event = Event::findOrFail($event_id);
        if($event->status==1){
            $event->status=0;

        }
        else{
            $event->status=1;
        }
        $event->save();
        return 1;
    }

    public function addTicket($id){
        $eventId=base64_decode($id);
        $event = Event::findOrFail($eventId);
        return view('admin.events.addmoreticket',compact('event'));
    }

    public function createNewTicket(Request $request){
        $requestData=$request->all();
        $formDetails=array();
        if(isset($requestData['form_mobile'])){
            array_push($formDetails,$requestData['form_mobile']);
        }
        if(isset($requestData['form_title'])){
            array_push($formDetails,$requestData['form_title']);
        }
        if(isset($requestData['form_gender'])){
            array_push($formDetails,$requestData['form_gender']);
        }
        if(isset($requestData['form_designations'])){
            array_push($formDetails,$requestData['form_designations']);
        }
        if(isset($requestData['form_organisations'])){
            array_push($formDetails,$requestData['form_organisations']);
        }
        if(isset($requestData['form_address'])){
            array_push($formDetails,$requestData['form_address']);
        }
        if(isset($requestData['form_city'])){
            array_push($formDetails,$requestData['form_city']);
        }
        if(isset($requestData['form_state'])){
            array_push($formDetails,$requestData['form_state']);
        }
        if(isset($requestData['form_country'])){
            array_push($formDetails,$requestData['form_country']);
        }
        if(isset($requestData['form_pincode'])){
            array_push($formDetails,$requestData['form_pincode']);
        }
        if(isset($requestData['form_emergency_contact_number'])){
            array_push($formDetails,$requestData['form_emergency_contact_number']);
        }
        if(isset($requestData['form_emergency_contact_name'])){
            array_push($formDetails,$requestData['form_emergency_contact_name']);
        }
        if(isset($requestData['form_emergency_contact_relations'])){
            array_push($formDetails,$requestData['form_emergency_contact_relations']);
        }
        if(isset($requestData['form_dob'])){
            array_push($formDetails,$requestData['form_dob']);
        }
        if(isset($requestData['form_blood_group'])){
            array_push($formDetails,$requestData['form_blood_group']);
        }
        if(isset($requestData['form_tshirt_size'])){
            array_push($formDetails,$requestData['form_tshirt_size']);
        }
        if(isset($requestData['form_attendee_photo'])){
            array_push($formDetails,$requestData['form_attendee_photo']);
        }
        if(isset($requestData['form_attende_id_proof'])){
            array_push($formDetails,$requestData['form_attende_id_proof']);
        }

        $requestData['form_fields']=serialize($formDetails);
        EventTicket::create($requestData);
        return redirect('admin/events')->with('flash_message', 'New Event Ticket Added Successfully!');
    }

    public function editTicket($id){
        $ticketId=base64_decode($id);
        $eventTicket = EventTicket::findOrFail($ticketId);
        $formfield=unserialize($eventTicket->form_fields);
        return view('admin.events.edit_ticket',compact('eventTicket','formfield'));
    }

    public function saveeditTicket(Request $request,$id){
        $requestData=$request->all();
        $formDetails=array();
        if(isset($requestData['form_mobile'])){
            array_push($formDetails,$requestData['form_mobile']);
        }
        if(isset($requestData['form_title'])){
            array_push($formDetails,$requestData['form_title']);
        }
        if(isset($requestData['form_gender'])){
            array_push($formDetails,$requestData['form_gender']);
        }
        if(isset($requestData['form_designations'])){
            array_push($formDetails,$requestData['form_designations']);
        }
        if(isset($requestData['form_organisations'])){
            array_push($formDetails,$requestData['form_organisations']);
        }
        if(isset($requestData['form_address'])){
            array_push($formDetails,$requestData['form_address']);
        }
        if(isset($requestData['form_city'])){
            array_push($formDetails,$requestData['form_city']);
        }
        if(isset($requestData['form_state'])){
            array_push($formDetails,$requestData['form_state']);
        }
        if(isset($requestData['form_country'])){
            array_push($formDetails,$requestData['form_country']);
        }
        if(isset($requestData['form_pincode'])){
            array_push($formDetails,$requestData['form_pincode']);
        }
        if(isset($requestData['form_emergency_contact_number'])){
            array_push($formDetails,$requestData['form_emergency_contact_number']);
        }
        if(isset($requestData['form_emergency_contact_name'])){
            array_push($formDetails,$requestData['form_emergency_contact_name']);
        }
        if(isset($requestData['form_emergency_contact_relations'])){
            array_push($formDetails,$requestData['form_emergency_contact_relations']);
        }
        if(isset($requestData['form_dob'])){
            array_push($formDetails,$requestData['form_dob']);
        }
        if(isset($requestData['form_blood_group'])){
            array_push($formDetails,$requestData['form_blood_group']);
        }
        if(isset($requestData['form_tshirt_size'])){
            array_push($formDetails,$requestData['form_tshirt_size']);
        }
        if(isset($requestData['form_attendee_photo'])){
            array_push($formDetails,$requestData['form_attendee_photo']);
        }
        if(isset($requestData['form_attende_id_proof'])){
            array_push($formDetails,$requestData['form_attende_id_proof']);
        }
        $requestData['form_fields']=serialize($formDetails);
        $ticketId=base64_decode($id);
        $eventTicket = EventTicket::findOrFail($ticketId);
        $eventTicket->update($requestData);
        return redirect('admin/events')->with('flash_message', 'Event Ticket Edited Successfully!');

    }



    public function add_dynamic_form(Request $request){


$html='';
        if($request['name']=='form_mobile'){
            $html.=' <div class="'.$request["name"].'"><label for="form_mobile" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif ($request['name']=='form_title'){
            $html .='<div class="'.$request["name"].'"><label for="form_title" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line"><select class="form-control" id="status" >';
      foreach (json_decode('{"mr": "MR", "mrs": "MRS","miss":"MISS","dr":"DR","engg":"ER","prof":"PROF"}', true) as $optionKey => $optionValue){
          $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
        }
            $html .='</select></div>
            </div></div>';
        }
        elseif ($request['name']=='form_gender'){
            $html .='<div class="'.$request["name"].'"><label for="form_gender" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line"><select class="form-control" id="status" >';
            foreach (json_decode('{"male": "Male", "female": "Female","other":"Other"}', true) as $optionKey => $optionValue){
                $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
            }
            $html .='</select></div>
            </div></div>';
        }
        elseif($request['name']=='form_designations'){
            $html.=' <div class="'.$request["name"].'"><label for="form_designations" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_organisations'){
            $html.=' <div class="'.$request["name"].'"><label for="form_organisations" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_address'){
            $html.=' <div class="'.$request["name"].'"><label for="form_address" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_city'){
            $html.=' <div class="'.$request["name"].'"><label for="form_city" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_state'){
            $html.=' <div class="'.$request["name"].'"><label for="form_state" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_country'){
            $html.=' <div class="'.$request["name"].'"><label for="form_country" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_pincode'){
            $html.=' <div class="'.$request["name"].'"><label for="form_pincode" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_emergency_contact_number'){
            $html.=' <div class="'.$request["name"].'"><label for="form_emergency_contact_number" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_emergency_contact_name'){
            $html.=' <div class="'.$request["name"].'"><label for="form_emergency_contact_name" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_emergency_contact_relations'){
            $html.=' <div class="'.$request["name"].'"><label for="form_emergency_contact_relations" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="text">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_dob'){
            $html.=' <div class="'.$request["name"].'"><label for="form_dob" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control datepicker"  type="text">  
      </div>
 </div></div>';
        }

        elseif ($request['name']=='form_blood_group'){
            $html .='<div class="'.$request["name"].'"><label for="form_blood_group" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line"><select class="form-control">';
            foreach (json_decode('{"A+": "A+", "O+": "O+","B+":"B+","B+":"B+","AB+":"AB+","A-":"A-","O-":"O-","B-":"B-","AB-":"AB-"}', true) as $optionKey => $optionValue){
                $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
            }
            $html .='</select></div>
            </div></div>';
        }
        elseif ($request['name']=='form_tshirt_size'){
            $html .='<div class="'.$request["name"].'"><label for="form_tshirt_size" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line"><select class="form-control">';
            foreach (json_decode('{"XS": "XS", "S": "S","M":"M","L":"L","XL":"XL","XLL":"XLL","XLLL":"XLLL"}', true) as $optionKey => $optionValue){
                $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
            }
            $html .='</select></div>
            </div></div>';
        }
        elseif($request['name']=='form_attendee_photo'){
            $html.=' <div class="'.$request["name"].'"><label for="form_attendee_photo" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="file">  
      </div>
 </div></div>';
        }
        elseif($request['name']=='form_attende_id_proof'){
            $html.=' <div class="'.$request["name"].'"><label for="form_attende_id_proof" style="font-size:18px;">'.$request["dispaly_name"].'</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control"  type="file">  
      </div>
 </div></div>';
        }



            return $html;
    }

    public function delete_dynamic_form(Request $request){
       return $request["name"];
    }

}
