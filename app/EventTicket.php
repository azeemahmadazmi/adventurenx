<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EventTicket extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */

       protected $table = 'event_tickets';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id', 'event_ticket_name', 'quantity', 'minimum_per_booking', 'maximum_per_booking', 'type', 'price', 'adventurenx_fees', 'gateway_fees', 'ticket_sale_start_date', 'ticket_sale_start_time', 'ticket_sale_end_date', 'ticket_sale_end_time', 'ticket_description', 'message_to_attendee', 'term_and_conditions','form_fields','status'];

    public function event(){
        return $this->belongsTo('App\Event','event_id');
    }
}
