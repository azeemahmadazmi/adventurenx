<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Order;
use App\Event;
use App\EventTicket;
use Illuminate\Support\Facades\Auth;
class OrderController extends Controller
{

    public function clientOrder(Request $request){
        $keyword = $request->get('search');
        $perPage = 20;
        $events=Event::where('created_by','=','CLIENT')->where('creator_id',Auth::guard('client')->user()->id)->pluck('id');
        if (!empty($keyword)) {
            $orders = Order::orWhere(function ($query) use($keyword){
                $query->where('id', 'LIKE', "%$keyword%")
                    ->orWhereHas('user',function ($q) use($keyword){
                        $q->where('name', 'LIKE', "%$keyword%")
                            ->orWhere('email', 'LIKE', "%$keyword%")
                            ->orWhere('mobile', 'LIKE', "%$keyword%");
                    })
                    ->orWhereHas('event',function ($ev)  use($keyword){
                        $ev->where('event_name', 'LIKE', "%$keyword%")
                            ->orWhere('event_display_name', 'LIKE', "%$keyword%");
                    });
            })->whereIn('event_id',$events)
                ->latest()->paginate($perPage);
        } else {
            $orders=Order::whereIn('event_id',$events)->latest()->paginate($perPage);        }

        return view('client.orders.clientOrders',compact('orders'));

    }
    public function viewclientOrder($id){
        $order_id=base64_decode($id);
        $order=Order::where('id',$order_id)->whereHas('event',function ($query){
            $query->where('created_by','=','CLIENT')
                ->where('creator_id',Auth::guard('client')->user()->id);
        })->first();
        if($order){
            $cartvalue=unserialize($order->cart);
            $quantity=$cartvalue['quantity'];
            $userDetails=unserialize($order->user_details);
            return view('client.orders.viewclientOrder',compact('order','quantity','userDetails'));
        }
        else{
            return redirect()->back();
        }

    }


    public function reciept(Request $request)
    {
        $orderId=base64_decode($request['order_id']);
        $order=Order::find($orderId);
        $cartDetails=unserialize($order->cart);
        $event = Event::find($cartDetails['event_id']);
        $totalQuantity=$cartDetails['quantity'];
        $eventTicket = EventTicket::where('event_id', $event->id)->first();
        $users=User::where('id',$order->user_id)->first();
        return view('frontuser.cart.reciept', compact('order','event','totalQuantity','eventTicket','users'));
    }

}
