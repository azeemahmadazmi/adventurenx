@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            AboutusActivity {{ $aboutusactivity->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/aboutus-activities') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/aboutus-activities/' . $aboutusactivity->id . '/edit') }}"
                           title="Edit AboutusActivity">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/aboutusactivities' . '/' . $aboutusactivity->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete AboutusActivity"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $aboutusactivity->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $aboutusactivity->name }} </td>
                                </tr>
                                <tr>
                                    <th> Count</th>
                                    <td> {{ $aboutusactivity->count }} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $aboutusactivity->status==1?'Active':'Disabled' }}  </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
