@extends('frontuser.layouts.master')
@section('css')

    <style>
        .elementor-section {
            background: #af7c7c !important;
        }
    </style>
@endsection
@section('banner')
    <section class="header-breadcrumbs" id="cart_banner">
        <div class="uk-cover-background uk-position-relative shopping-bag-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}"
                 alt="Shopping Bag Breadcrumbs">

            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">cart</a></li>
                        <li class="uk-active"><a href="#">shopping bag</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">your cart</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <input type="hidden" id="totaleventcount" value="{{$eventTicketsTotalCount}}">
    <div class="displayattendeeform">
    <section class="shopping-bag section-padding-top uk-overflow-container">
            <form method="post" id="cart_form">
            @csrf
            <div class="uk-container uk-container-center">
                <div class="uk-grid">

                    <div class="uk-width-1-1">
                        <div class="overflow-container">
                            <div class="shopping-bag-cart">
                                <div class="uk-grid wall">
                                    <div class="uk-width-3-10">
                                        Ticket name
                                    </div>
                                    <div class="uk-width-2-10">
                                        Available Ticket
                                    </div>
                                    <div class="uk-width-1-10">
                                        unit price
                                    </div>
                                    <div class="uk-width-2-10">
                                        qty
                                    </div>
                                    <div class="uk-width-1-10">
                                        total
                                    </div>
                                    {{--<div class="uk-width-1-10">--}}
                                        {{--remove--}}
                                    {{--</div>--}}
                                </div>
                                <ul class="shopping-bag-list">
                                    @if(isset($eventTickets) && $eventTickets->isNotEmpty())
                                        <?php $totalNumber=0;?>
                                        @foreach($eventTickets as $ek => $eventTicket)
                                                <?php $totalNumber++;
                                                $totalusedTicket=0;
                                                $order=App\Order::where('event_id',$event->id)->get();
                                                if(isset($order) && $order->isNotEmpty()){
                                                    $order->map(function ($q){
                                                        $q->user_details=unserialize($q->user_details);
                                                    });
                                                    foreach ($order as $ok => $ov){
                                                       foreach ($ov->user_details as $ak =>$av){
                                                           if(isset($av['event_ticket_id']) && $av['event_ticket_id']==$eventTicket->id){
                                                               $totalusedTicket +=1;
                                                           }
                                                       }
                                                    }
                                                }
                                                $totalRemainingTicket=$eventTicket->quantity-$totalusedTicket;
                                        ?>
                                    <li class="uk-grid shopping-bag-list-item">
                                        <div class="uk-width-3-10">
                                            <div class="uk-grid">
                                                <div class="uk-width-1-3 shopping-bag-product-img">
                                                    <img src="{{url('Events/'.$event->image)}}"
                                                         alt="Shopping Bag Item">
                                                </div>
                                                <div class="uk-width-2-3 shopping-bag-product uk-overlay">
                                                    <div class="uk-overlay-panel uk-overlay-top">
                                                        <a href="{{'/eventdescriptions?eventid='.base64_encode($event->id)}}"><h5 class="h-5">{{$eventTicket->event_ticket_name}}</h5></a>

                                                        <div class="title-line-lg"></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-2-10 shopping-bag-details">
                                            <span class="uk-text-bottom"><center><b style="color:green">{{$totalRemainingTicket}}</b></center></span>
                                        </div>
                                        <div class="uk-width-1-10 uk-text-center">
                                            ₹ <span id="singleprize-{{$totalNumber}}">{{$eventTicket->price}}</span>
                                        </div>
                                        <input type="hidden" name="eventTicket_id[]" value="{{$eventTicket->id}}">

                                        <input type="hidden" value="{{$event->id}}" name="event_id">

                                        <div class="uk-width-2-10 uk-text-center shopping-bag-qty">
                                            @if($totalRemainingTicket==0)
                                                <h5 class="h-5" style="background:red;color:white">SOLD OUT</h5>
                                                @else
                                            <div class="input-group spinner one">
                                                <input type="text" class="form-control totalquantity" id="productquantity-{{$totalNumber}}" value="0"
                                                       name="quantity[{{$eventTicket->id}}]">

                                                <div class="input-group-btn-vertical">
                                                    <button class="btn" type="button"><i class="uk-icon-angle-up" data-remainingticket="{{$totalRemainingTicket}}" data-id="{{$totalNumber}}" data-minimum="{{$eventTicket->minimum_per_booking}}" data-maximum="{{$eventTicket->maximum_per_booking}}"></i>
                                                    </button>
                                                    <button class="btn" type="button"><i class="uk-icon-angle-down" data-remainingticket="{{$totalRemainingTicket}}" data-id="{{$totalNumber}}" data-minimum="{{$eventTicket->minimum_per_booking}}" data-maximum="{{$eventTicket->maximum_per_booking}}"></i>
                                                    </button>
                                                </div>
                                            </div>
                                                @endif
                                        </div>
                                        <div class="uk-width-1-10 uk-text-center">
                                            <p class="uk-text-bold">₹ <span id="totalprize-{{$totalNumber}}">0</span></p>
                                        </div>
                                        {{--<div class="uk-width-1-10 uk-text-center">--}}
                                            {{--<button class="cart-item-close">--}}
                                                {{--<img src="images/images_sport/icon-close.png" alt="Icon Close">--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    </li>
                                        @endforeach
                                        @endif
                                </ul>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-10 uk-text-center uk-vertical-align continue-shipping">
                                <p class="uk-text-bold uk-vertical-align-middle"><a href="#"></a></p>
                            </div>
                            <div class="uk-width-medium-2-10 update-bag-btn">
                                <button class="uk-button btn-custom-black" type="submit">proceed for payment</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            if($('body').on('.waves-effect','click')) {
                window.onbeforeunload = null;
                return true;
            }
            else if($('body').on('.btn-custom-black','click')) {
                window.onbeforeunload = null;
                return true;
            }
            else if($('body').on('#cart_form','submit')) {
                window.onbeforeunload = null;
                return true;
            }
            else if($('body').on('.next_users_detaisl','submit')) {
                window.onbeforeunload = null;
                return true;
            }
            else{
                window.onbeforeunload = function() {
                return "Data will be lost if you leave the page, are you sure?";
                };
            }
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#cart_form').submit(function(event){
            event.preventDefault();
            var totalticket = $('#totaleventcount').val();
            var i;
            var quantity =0;
            for (i = 1; i <= totalticket; i++) {
                quantity += parseInt($('#productquantity-'+i).val(),10);
            }
            if(quantity==0){
                alert('Please select at lest one quantity');
                return false;
            }

            //var formData=$(this).serialize();
            var formData = new FormData(this);
            $.ajax({
                url: '/cartpayment',
                type: 'post',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                {{--beforeSend: function() {--}}
                    {{--$('.displayattendeeform').html('<img src="{{asset('ajax-loader.gif')}}">');--}}
                {{--},--}}
                success: function (msg) {
                   $('.displayattendeeform').html(msg);
                    $('#form_validation').validate({
                        rules: {
                            'checkbox': {
                                required: true
                            },
                            'gender': {
                                required: true
                            }
                        },
                        highlight: function (input) {
                            $(input).parents('.form-line').addClass('error');
                        },
                        unhighlight: function (input) {
                            $(input).parents('.form-line').removeClass('error');
                        },
                        errorPlacement: function (error, element) {
                            $(element).parents('.form-group').append(error);
                        }
                    });
                    $('#datepicker').datepicker({
                        uiLibrary: 'bootstrap'
                    });

                    $('#users_mobiles').keyup(function () {
                        var mobile = $('#users_mobiles').val();
                        var emergency = $('#emergency_contacts').val();
                        if(mobile==emergency){
                            alert('Mobile Number and Emegency number should be different');
                            return false;
                        }
                    });
                    $('#emergency_contacts').keyup(function () {
                        var mobile = $('#users_mobiles').val();
                        var emergency = $('#emergency_contacts').val();
                        if(mobile==emergency){
                            alert('Mobile Number and Emegency number should be different');
                            return false;
                        }
                    });


                }
            });

        });


        $('body').on('submit','.next_users_detaisl',function(event){
            event.preventDefault();
           // var formData=$(this).serialize()[0];
            var formData = new FormData(this);

            $.ajax({
                url: '/next_users_detaisl',
                type: 'post',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                {{--beforeSend: function() {--}}
                    {{--$('.displayattendeeform').html('<img src="{{asset('ajax-loader.gif')}}">');--}}
                {{--},--}}
                success: function (msg) {
                    $('.displayattendeeform').html(msg);
                    $('#form_validation').validate({
                        rules: {
                            'checkbox': {
                                required: true
                            },
                            'gender': {
                                required: true
                            }
                        },
                        highlight: function (input) {
                            $(input).parents('.form-line').addClass('error');
                        },
                        unhighlight: function (input) {
                            $(input).parents('.form-line').removeClass('error');
                        },
                        errorPlacement: function (error, element) {
                            $(element).parents('.form-group').append(error);
                        }
                    });
                    $('#datepicker').datepicker({
                        uiLibrary: 'bootstrap'
                    });
                    $('#users_mobiles').keyup(function () {
                        var mobile = $('#users_mobiles').val();
                        var emergency = $('#emergency_contacts').val();
                        if(mobile==emergency){
                            alert('Mobile Number and Emegency number should be different');
                            return false;
                        }
                    });
                    $('#emergency_contacts').keyup(function () {
                        var mobile = $('#users_mobiles').val();
                        var emergency = $('#emergency_contacts').val();
                        if(mobile==emergency){
                            alert('Mobile Number and Emegency number should be different');
                            return false;
                        }
                    });

                }
            });

        });

    </script>

@endsection