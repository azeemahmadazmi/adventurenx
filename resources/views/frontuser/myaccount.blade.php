@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative account-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Account Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">account</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">account</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <div class="account section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <aside class="uk-width-medium-3-10">
                    <h4 class="h-4">my account</h4>
                    <div class="title-line-lg"></div>
                    <ul class="aside-navigation">
                        <li class="active"><a href="#">Account Information<i class="uk-icon-angle-right icon-right"></i></a></li>
                        <li><a href="{{url('/orderhistories')}}">My Orders<i class="uk-icon-angle-right icon-right"></i></a></li>
                        {{--<li><a href="#">My Profile<i class="uk-icon-angle-right icon-right"></i></a></li>--}}
                    </ul>
                    <div class="uk-grid uk-grid-small uk-grid-width-large-1-2" data-uk-grid-margin>
                        <div>
                            <a href="{{url('/editprofile')}}"><button class="uk-button btn-custom">edit account</button></a>
                        </div>
                        <div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="uk-button btn-custom-black" type="submit"  style="background: red !important;">sign out</button>
                            </form>

                        </div>
                    </div>
                </aside>
                <div class="uk-width-medium-7-10">
                    <p class="text-sixteen border-b2 account-welcome">
                        Welcome <span class="post-author">{{$user->name}}</span>. This is your account. Click on the sidebar sections to manage your personal information, check the status of an order or download event's order receipt.
                    </p>
                    <ul class="account-info">
                        <li class="uk-grid">
                            <p class="uk-width-large-1-5 uk-width-small-1-3 uk-text-capitalize uk-padding-remove">User name:</p>
                            <p class="uk-width-2-5 uk-text-capitalize uk-text-bold">{{$user->name}}</p>
                        </li>

                        <li class="uk-grid">
                            <p class="uk-width-large-1-5 uk-width-small-1-3 uk-text-capitalize uk-padding-remove">e-mail:</p>
                            <p class="uk-width-2-5 uk-text-bold">{{$user->email}}</p>
                        </li>
                        <li class="uk-grid">
                            <p class="uk-width-large-1-5 uk-width-small-1-3 uk-text-capitalize uk-padding-remove">phone number:</p>
                            <p class="uk-width-2-5 uk-text-bold">{{$user->mobile}}</p>
                        </li>
                        <li class="uk-grid">
                            <p class="uk-width-large-1-5 uk-width-small-1-3 uk-text-capitalize uk-padding-remove">Address:</p>
                            <div class="uk-width-large-4-5 uk-width-small-2-3">
                                <p class="uk-width-1-1 uk-text-capitalize uk-text-bold account-info-lg">{{$user->address}}</p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
