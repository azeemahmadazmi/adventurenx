 <label for="user_name" style="font-size:18px;">{{ 'User Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="user_name" type="text" id="user_name" value="{{ isset($blogcomment->user_name) ? $blogcomment->user_name : ''}}" required>
     {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="user_email" style="font-size:18px;">{{ 'User Email' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="user_email" type="text" id="user_email" value="{{ isset($blogcomment->user_email) ? $blogcomment->user_email : ''}}" >
     {!! $errors->first('user_email', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="blog_id" style="font-size:18px;">{{ 'Blog Name' }}</label>
 <div class="form-group">
     <div class="form-line">
         <select name="blog_id" class="form-control" id="blog_id" >
             @foreach ($blog as $optionKey => $optionValue)
                 <option value="{{ $optionKey }}" {{ (isset($blogcomment->blog_id) && $blogcomment->blog_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
             @endforeach
         </select>
         {!! $errors->first('blog_id', '<p class="help-block">:message</p>') !!}
     </div>
 </div>
 <label for="message" style="font-size:18px;">{{ 'Message' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="message" type="textarea" id="message" >{{ isset($blogcomment->message) ? $blogcomment->message : ''}}</textarea>
     {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($blogcomment->status) && $blogcomment->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
