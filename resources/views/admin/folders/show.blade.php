@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Folder {{ $folder->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/folders') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/folders/' . $folder->id . '/edit') }}" title="Edit Folder">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>
                        <form method="POST" action="{{ url('admin/folders' . '/' . $folder->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Folder"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $folder->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $folder->name }} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{$folder->status==1?'Active':'Disabled' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
