@extends('frontuser.layouts.master')
@section('css')
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/light-gallery/css/lightgallery.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/style.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/themes/all-themes.css')}}" rel="stylesheet" />
    <style>
        .active * {
            color: white !important;
            font-size: 15px !important;
        }
        .pagination > li > span {
            padding: 6px 22px !important;
        }
    </style>
@endsection
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative blog-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Blog Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">gallery</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">gallery</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            @if(isset($folder) && $folder->isNotEmpty())
                                @foreach($folder as $fk =>$fv)
                                <a href="{{url('/gallery?folder_id='.$fv->id)}}"><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><img src="{{asset('thumbnail_large.png')}}" width="250px" height="200px"><strong>{{$fv->name}}</strong></div></a>
                                @endforeach

                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
