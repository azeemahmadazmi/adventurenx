@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Activitiescount {{ $activitiescount->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/activitiescount') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/activitiescount/' . $activitiescount->id . '/edit') }}"
                           title="Edit Activitiescount">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>
                        <form method="POST" action="{{ url('admin/activitiescount' . '/' . $activitiescount->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Activitiescount"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $activitiescount->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $activitiescount->name }} </td>
                                </tr>
                                <tr>
                                    <th> Total</th>
                                    <td> {{ $activitiescount->total }} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $activitiescount->status }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
