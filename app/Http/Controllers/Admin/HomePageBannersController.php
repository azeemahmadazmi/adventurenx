<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\HomePageBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
class HomePageBannersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homepagebanners = HomePageBanner::where('title1', 'LIKE', "%$keyword%")
                ->orWhere('title2', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homepagebanners = HomePageBanner::latest()->paginate($perPage);
        }

        return view('admin.home-page-banners.index', compact('homepagebanners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.home-page-banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title1' => 'required',
			'title2' => 'required',
			'image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/images/HomepageBanner'), $filename);
            $requestData['image']=$filename;
        }

        HomePageBanner::create($requestData);

        return redirect('admin/home-page-banners')->with('flash_message', 'HomePageBanner added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homepagebanner = HomePageBanner::findOrFail($id);

        return view('admin.home-page-banners.show', compact('homepagebanner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homepagebanner = HomePageBanner::findOrFail($id);

        return view('admin.home-page-banners.edit', compact('homepagebanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title1' => 'required',
			'title2' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/images/HomepageBanner'), $filename);
            $requestData['image']=$filename;
        }

        $homepagebanner = HomePageBanner::findOrFail($id);
        $homepagebanner->update($requestData);

        return redirect('admin/home-page-banners')->with('flash_message', 'HomePageBanner updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        HomePageBanner::destroy($id);

        return redirect('admin/home-page-banners')->with('flash_message', 'HomePageBanner deleted!');
    }
}
