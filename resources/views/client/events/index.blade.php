@extends('client.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Events
                        </h2>
                    </div>
                    <br>
                    <a href="{{ url('/client/events/create') }}" class="btn btn-success btn-sm waves-effect"
                       title="Add New Event" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                    </a>
                    {!! Form::open(['method' => 'GET', 'url' => '/client/events', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               style="border: ridge">
                        <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                    </div>
                    {!! Form::close() !!}
                    <div class="body">
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Add More Ticket +</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($events as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->event_name }}</td>
                                        <td><a href="{{url('Events/'.$item->image)}}" target="_blank"><img src="{{asset('Events/'.$item->image)}}" width="200px" height="200px"></a></td>
                                        <td>{{ $item->event_start_date }}</td>
                                        <td>{{ $item->event_end_date }}</td>
                                        <td>{!!  $item->status==1?'Enabled':'<button type="button" class="btn btn-primary btn-block waves-effect"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Your Event Status approved shortly by Adventurenx Admin"> Disabled </button>' !!}</td>
                                        <td>
                                            <a href="{{ url('/client/events/addTicket/'.base64_encode($item->id)) }}" class="btn btn-success btn-xs waves-effect"
                                               title="Add More Ticket" style="margin-left: 22px;">
                                                <i class="material-icons">add_circle</i> Add Ticket
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ url('/client/events/' . $item->id) }}" title="View Event">
                                                <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                    View
                                                </button>
                                            </a>
                                            <a href="{{ url('/client/events/' . $item->id . '/edit') }}"
                                               title="Edit Event">
                                                <button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i>
                                                    Edit
                                                </button>
                                            </a>
                                            <form method="POST" action="{{ url('/client/events' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-xs" title="Delete Event"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="material-icons">delete</i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $events->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
