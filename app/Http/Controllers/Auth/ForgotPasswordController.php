<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request){
        $this->validate($request, [
            'email' => 'required|exists:users'
        ]);
        $data =$request->all();
        $url = URL::route('password.reset',['token' => $data['_token']]);

        $token = DB::table('password_resets')
            ->where('email','=',$data['email'])
            ->first();
        if(!$token){
            DB::table('password_resets')->insert([
                'email' => $data['email'], 'token' => bcrypt($data['_token']),'created_at'=>date('Y-m-d H:i:s')
            ]);
        }
        else{
            DB::table('password_resets')
                ->where('email', $data['email'])->update([
                    'email' => $data['email'], 'token' => bcrypt($data['_token']),'created_at'=>date('Y-m-d H:i:s')
                ]);
        }
        $data=['link' => $url,'to_email' =>$request['email'],'from_email'=>'admin@adventurenx.com','subject' =>'Reset Password','title' =>'Reset password','view' =>'frontuser.email.resetpassword'];
        $mailstatus=$this->sendemail($data);
        if($mailstatus==1) {
            return redirect()->back()->with('status', 'Password Reset Link Send Successfully !');
        }
        else{
            return redirect()->back()->with('status','Some thing wrong! . Please try again!');
        }

    }
}
