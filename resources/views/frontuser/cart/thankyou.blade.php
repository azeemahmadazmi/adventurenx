@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative payment-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="About Header Breadcrumbs">

            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">event</a></li>
                        <li><a href="#">event ticket</a></li>
                        <li class="uk-active"><a href="#">payment</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">thank you</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="payment-steps-form section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <fieldset class="uk-width-8-10 payment-confirmation uk-form">
                        <figure class="uk-overlay">
                            <img src="images/images_sport/payment-confirmation.jpg" alt="Payment Confirmation">
                            <figcaption class="uk-overlay-panel uk-flex uk-flex-middle">
                                <div>
                                    <h2 class="h-2">thank you</h2>
                                    <div class="title-line-lg"></div>
                                    @if(Session::get('totalAmount') && Session::get('totalAmount'))
                                    <p>Your payment of <span class="uk-text-bold"> Rs {{Session::get('totalAmount')}}</span> for Order id <span class="uk-text-bold"> {{Session::get('final_orderID')}}</span> was successful.</p>
                                    <p>All Details related to your event booking send successfully on your email id.</p>
                                    <p>Please also check sms on your mobile for event booking confirmation.</p>
                                    <p>You can also download event receipt from your order history.</p>
                                    @endif
                                </div>
                            </figcaption>
                        </figure><br><br>
                        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                        <center><a href="{{url('/myaccount')}}"><button class="btn btn-success btn-md">Go to my profile</button></a></center>
                    </fieldset>
                </div>
            </div>
        </div>
    </section>
    <br><br>
@endsection
