@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative account-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Account Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">account</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">user details</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href=" https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        @media screen and (max-width: 768px){
            .card {
                width: 580px;
                margin-left: -60px;
            }
        }
        @media screen and (max-width: 414px){
            .card {
                width: 400px;
            }
        }
        @media screen and (max-width: 375px){
            .card {
                width: 350px;
            }
        }
        @media screen and (max-width: 360px){
            .card {
                width: 340px;
            }
        }
        @media screen and (max-width: 320px){
            .card {
                width: 260px;
            }
        }

    </style>
@endsection
@section('content')
    <div class="account section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>

                <div class="uk-width-medium-12-12">
                    <p class="text-sixteen border-b2 account-welcome">
                        Welcome <span class="post-author">{{$user->name}}</span>!empty($userDetails))

                            @php  $count=0;@endphp
                            @foreach($userDetails as $ok => $ov)
                                @php
                                    $count++;
                                    $eventTicket=App\EventTicket::find($ov['event_ticket_id']);
                                @endphp
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            User Detail-{{$count}}
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover">
                                                <tbody>
                                                <tr>
                                                    <th> Event Ticket Name</th>
                                                    <td>{{$eventTicket->event_ticket_name}}</td>
                                                </tr>
                                                <tr>
                                                    <th> Event Unit price</th>
                                                    <td>{{$eventTicket->price}}</td>
                                                </tr>
                                                <tr>
                                                    <th> Name</th>
                                                    <td>{{$ov['fullname']}}</td>
                                                </tr>

                                                <tr>
                                                    <th> Email</th>
                                                    <td>{{$ov['email']}}</td>
                                                </tr>
                                                <tr>
                                                    <th> Date of Birth</th>
                                                    <td>{{date('d F Y',strtotime($ov['dob']))}} </td>
                                                </tr>
                                                <tr>
                                                    <th> Mobile</th>
                                                    <td> {{$ov['mobile']}} </td>
                                                </tr>
                                                <tr>
                                                    <th> Gender</th>
                                                    <td>{{ucwords($ov['gender'])}}  </td>
                                                </tr>
                                                <tr>
                                                    <th> Blood Group</th>
                                                    <td> {{$ov['blood_group']}} </td>
                                                </tr>
                                                <tr>
                                                    <th> T-shirt Size</th>
                                                    <td>  {{$ov['tshirt']}}</td>
                                                </tr>
                                                <tr>
                                                    <th> Emergency Contact Name</th>
                                                    <td> {{$ov['emergency_name']}} </td>
                                                </tr>
                                                <tr>
                                                    <th> Emergency Contact Number</th>
                                                    <td> {{$ov['emergency_contact']}} </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <h3 style="color:red;text-transform: capitalize;">There is no previous order found.</h3>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
    <script src=" https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>




@endsection
