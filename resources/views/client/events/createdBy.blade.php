@extends('client.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            User-Name: {{ $user->name }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/client/events') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>User-ID</th>
                                    <td>{{ $usertype.'-'.$user->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $user->name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $user->email }} </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
