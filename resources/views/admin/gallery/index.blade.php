@extends('admin.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Gallery
                        </h2>
                    </div>
                    <br>
                    <a href="{{ url('/admin/gallery/create') }}" class="btn btn-success btn-sm waves-effect"
                       title="Add New Gallery" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                    </a>
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/gallery', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               style="border: ridge">
                        <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                    </div>
                    {!! Form::close() !!}
                    <div class="body">
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Folder Name</th>
                                    <th>Image</th>
                                    <th>Thumb Image</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($gallery as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ucwords($item->folder->name)}}</td>
                                        <td><img src="{{asset('galleryImage/'.$item->image)}}" width="200px" height="180px"></td>
                                        <td><img src="{{asset('galleryImage/thumbimage/'.$item->thumbimage)}}" width="150px" height="130px"></td>
                                        <td>{{ $item->status==1?'Enabled':'Disabled' }}</td>
                                        <td class="text-center">
                                            <a href="{{ url('/admin/gallery/' . $item->id) }}" title="View Gallery">
                                                <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                    View
                                                </button>
                                            </a>
                                            <a href="{{ url('/admin/gallery/' . $item->id . '/edit') }}"
                                               title="Edit Gallery">
                                                <button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i>
                                                    Edit
                                                </button>
                                            </a>
                                            <form method="POST" action="{{ url('/admin/gallery' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-xs"
                                                        title="Delete Gallery"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="material-icons">delete</i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $gallery->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
