 <label for="title1" style="font-size:18px;">{{ 'Title1' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title1" type="text" id="title1" value="{{ isset($homepagebanner->title1) ? $homepagebanner->title1 : ''}}" required>
     {!! $errors->first('title1', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="title2" style="font-size:18px;">{{ 'Title2' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title2" type="text" id="title2" value="{{ isset($homepagebanner->title2) ? $homepagebanner->title2 : ''}}" required>
     {!! $errors->first('title2', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($homepagebanner->image) ? $homepagebanner->image : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homepagebanner->status) && $homepagebanner->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
