<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Coupon extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */

       protected $table = 'coupons';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'value', 'start_date', 'end_date', 'minimum_cart_value', 'category', 'status'];

    
}
