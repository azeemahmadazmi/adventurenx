@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Subscriber  {{ $subscriber->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/subscriber') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                         <a href="{{ url('/admin/subscriber/' . $subscriber->id . '/edit') }}" title="Edit Subscriber"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/subscriber' . '/' . $subscriber->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Subscriber" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $subscriber->id }}</td>
                                    </tr>
                                    <tr><th> Email </th><td> {{ $subscriber->email }} </td></tr><tr><th> Status </th><td> {{ $subscriber->status==1?'Active':'Disabled' }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
