<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CompanyPartner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
class CompanyPartnerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $companypartner = CompanyPartner::where('name', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $companypartner = CompanyPartner::latest()->paginate($perPage);
        }

        return view('admin.company-partner.index', compact('companypartner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.company-partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/companyLogo'), $filename);
            $requestData['image']=$filename;
        }

        CompanyPartner::create($requestData);

        return redirect('admin/company-partner')->with('flash_message', 'CompanyPartner added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $companypartner = CompanyPartner::findOrFail($id);

        return view('admin.company-partner.show', compact('companypartner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $companypartner = CompanyPartner::findOrFail($id);

        return view('admin.company-partner.edit', compact('companypartner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename = Input::file('image')->getClientOriginalName();
            $request->image->move(base_path('public/companyLogo'), $filename);
            $requestData['image']=$filename;
        }

        $companypartner = CompanyPartner::findOrFail($id);
        $companypartner->update($requestData);

        return redirect('admin/company-partner')->with('flash_message', 'CompanyPartner updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CompanyPartner::destroy($id);

        return redirect('admin/company-partner')->with('flash_message', 'CompanyPartner deleted!');
    }
}
