
<label for="status" style="font-size:18px;">{{ 'Select Folder Name' }}</label>
<div class="form-group">
    <div class="form-line">
        <select name="folder_id" class="form-control" id="folder_id" >
            @foreach ($folder as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($gallery->folder_id) && $gallery->folder_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('folder_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

 <label for="image" style="font-size:18px;">{{ 'Upload Multiple Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" @if(isset($gallery)) name="image" @else name="image[]" @endif type="file" id="image" value="{{ isset($gallery->image) ? $gallery->image : ''}}" multiple>
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

 {{--<label for="image" style="font-size:18px;">{{ 'Thumb Image' }}</label>--}}
 {{--<div class="form-group">--}}
     {{--<div class="form-line">--}}
         {{--<input class="form-control" name="thumbimage" type="file" id="thumbimage" value="{{ isset($gallery->thumbimage) ? $gallery->thumbimage : ''}}">--}}
         {{--{!! $errors->first('thumbimage', '<p class="help-block">:message</p>') !!}--}}
     {{--</div>--}}
 {{--</div>--}}
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($gallery->status) && $gallery->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
