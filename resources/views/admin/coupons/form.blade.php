 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($coupon->name) ? $coupon->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="type" style="font-size:18px;">{{ 'Type' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="type" class="form-control" id="type" required>
    @foreach (json_decode('{"flat": "Flat", "percent": "Percentage"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($coupon->type) && $coupon->type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="value" style="font-size:18px;">{{ 'Value' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="value" type="number" id="value" value="{{ isset($coupon->value) ? $coupon->value : ''}}" required>
     {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="start_date" style="font-size:18px;">{{ 'Start Date' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="start_date" type="text" id="start_date" value="{{ isset($coupon->start_date) ? $coupon->start_date : ''}}" required>
     {!! $errors->first('start_date', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="end_date" style="font-size:18px;">{{ 'End Date' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="end_date" type="text" id="end_date" value="{{ isset($coupon->end_date) ? $coupon->end_date : ''}}" required>
     {!! $errors->first('end_date', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="minimum_cart_value" style="font-size:18px;">{{ 'Minimum Cart Value' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="minimum_cart_value" type="number" id="minimum_cart_value" value="{{ isset($coupon->minimum_cart_value) ? $coupon->minimum_cart_value : ''}}" required>
     {!! $errors->first('minimum_cart_value', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="category" style="font-size:18px;">{{ 'Category' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="category" class="form-control" id="category" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($coupon->category) && $coupon->category == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($coupon->status) && $coupon->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
