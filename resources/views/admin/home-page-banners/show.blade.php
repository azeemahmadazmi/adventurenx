@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            HomePageBanner {{ $homepagebanner->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/home-page-banners') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/home-page-banners/' . $homepagebanner->id . '/edit') }}"
                           title="Edit HomePageBanner">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>
                        <form method="POST" action="{{ url('admin/homepagebanners' . '/' . $homepagebanner->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete HomePageBanner"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $homepagebanner->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title1</th>
                                    <td> {{ $homepagebanner->title1 }} </td>
                                </tr>
                                <tr>
                                    <th> Title2</th>
                                    <td> {{ $homepagebanner->title2 }} </td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td><a href="{{url('images/HomepageBanner/'.$homepagebanner->image)}}" target="_blank"><img src="{{asset('images/HomepageBanner/'.$homepagebanner->image)}}" width="200px" height="200px"></a></td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $homepagebanner->status==1?'Enabled':'Disabled' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
