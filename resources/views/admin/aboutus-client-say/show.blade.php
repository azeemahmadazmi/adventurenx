@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            AboutusClientSay {{ $aboutusclientsay->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/aboutus-client-say') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/aboutus-client-say/' . $aboutusclientsay->id . '/edit') }}"
                           title="Edit AboutusClientSay">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/aboutusclientsay' . '/' . $aboutusclientsay->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete AboutusClientSay"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $aboutusclientsay->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $aboutusclientsay->title }} </td>
                                </tr>
                                <tr>
                                    <th> Description</th>
                                    <td> {!! $aboutusclientsay->description !!}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $aboutusclientsay->name }} </td>
                                </tr>
                                <tr>
                                    <th> Designation</th>
                                    <td> {{ $aboutusclientsay->designation }} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $aboutusclientsay->status==1?'Active':'Disabled' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
