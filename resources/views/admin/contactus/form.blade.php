 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($contactus->name) ? $contactus->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="email" style="font-size:18px;">{{ 'Email' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="email" type="email" id="email" value="{{ isset($contactus->email) ? $contactus->email : ''}}" >
     {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="message" style="font-size:18px;">{{ 'Message' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="message" type="textarea" id="message" required>{{ isset($contactus->message) ? $contactus->message : ''}}</textarea>
     {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($contactus->status) && $contactus->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
