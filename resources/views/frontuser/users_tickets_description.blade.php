
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Adventure | NX</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon" />
    <link rel="stylesheet" href="{{asset('css/uikit.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/uikit.components.min.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/css/jquery-ui.min.css')}}" />
    <link rel="stylesheet" href="{{asset('custom/css/fonts.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/css/flexslider.css')}}" />
    <link rel="stylesheet" href="{{asset('custom/css/style.css')}}" />
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"
          rel="stylesheet"/>

    <!-- Bootstrap DatePicker Css -->
    <link href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet"/>

    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
@include('frontuser.layouts.header')
<body>
<span style="display: none">
        <div class="">
            <ul class="list">
                <li class="active">
                </li>
            </ul>
        </div>
</span>

<section>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-offset-3  col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                <h4 style="background: green;color: white">
                    <center>ATTENDEE #1</center>
                </h4>
                <div class="card">
                    <div class="body">
                        <form id="form_validation" method="POST">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="fullname" required>
                                    <label class="form-label">Full Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" name="email" required>
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="dob" id="datepicker" required autocomplete="off">
                                    <label class="form-label">Date of Birth</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" name="mobile" required>
                                    <label class="form-label">Mobile</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="radio" name="gender" id="male" class="with-gap" value="male">
                                <label for="male">MALE</label>

                                <input type="radio" name="gender" id="female" class="with-gap" value="female">
                                <label for="female" class="m-l-20">FEMALE</label>

                                <input type="radio" name="gender" id="other" class="with-gap" value="female">
                                <label for="other" class="m-l-20">OTHER</label>
                            </div>
                            {{--<div class="form-group form-float">--}}
                                {{--<div class="form-line">--}}
                                    {{--<textarea name="address" cols="30" rows="3" class="form-control no-resize" required></textarea>--}}
                                    {{--<label class="form-label">Address</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select type="text" class="form-control" name="blood_group" required>
                                        <option value=""></option>
                                        <option label="O+" value="O+">O+</option>
                                        <option label="AB-" value="AB-">AB-</option>
                                        <option label="O-" value="O-">O-</option>
                                        <option label="A+" value="A+">A+</option>
                                        <option label="A-" value="A-">A-</option>
                                        <option label="B+" value="B+">B+</option>
                                        <option label="B-" value="B-">B-</option>
                                        <option label="AB+" value="AB+">AB+</option>
                                    </select>
                                    <label class="form-label">Blood Group</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select type="text" class="form-control" name="tshirt" required>
                                        <option value=""></option>
                                        <option value="XS">XS</option>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                        <option value="L">L</option>
                                        <option value="XL">XL</option>
                                        <option value="XXL">XXL</option>
                                    </select>
                                    <label class="form-label">T-Shirt Size</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="emergency_name" required>
                                    <label class="form-label">Emergency Contact Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" name="emergency_contact" required>
                                    <label class="form-label">Emergency Contact Number</label>
                                </div>
                            </div>

                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('plugins/node-waves/waves.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/admin.js')}}"></script>
<script src="{{asset('js/pages/forms/form-validation.js')}}"></script>
<script src="{{asset('js/uikit.min.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap'
    });

</script>

</body>
</html>
