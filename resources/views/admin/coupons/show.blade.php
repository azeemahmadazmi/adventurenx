@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Coupon  {{ $coupon->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/coupons') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                         <a href="{{ url('/admin/coupons/' . $coupon->id . '/edit') }}" title="Edit Coupon"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/coupons' . '/' . $coupon->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Coupon" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $coupon->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $coupon->name }} </td></tr><tr><th> Type </th><td> {{ $coupon->type }} </td></tr><tr><th> Value </th><td> {{ $coupon->value }} </td></tr><tr><th> Start Date </th><td> {{ $coupon->start_date }} </td></tr><tr><th> End Date </th><td> {{ $coupon->end_date }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
