@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative rafting-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Classes Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">event</a></li>
                        <li class="uk-active"><a href="#">{{$event->event_name}}</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">{{$event->event_name}}</h1>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('content')
    <section class="rafting section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-medium-7-10 classes-description-wellness text-sixteen">
                    <h2 class="h-2">about {{$event->event_name}}</h2>
                    <div class="title-line-lg"></div>
                    <div class="uk-cover uk-position-relative">
                        <img class="" src="{{url('Events/'.$event->image)}}" width="785" height="520">
                    </div>
                    <h3 style="background: #00B0E8">Event Descriptions</h3>
                    {!! $event->event_descriptions !!}
                    <h3 style="background: #00B0E8">Ticket Descriptions</h3>
                    {!! isset($event->eventTicket->ticket_description)?$event->eventTicket->ticket_description:'' !!}
                    <h3 style="background: #00B0E8">Message to Attendee</h3>
                    {!! isset($event->eventTicket->message_to_attendee)?$event->eventTicket->message_to_attendee:'' !!}
                </div>
                <aside class="uk-width-medium-3-10">
                    <h4 class="h-4">information</h4>
                    <div class="title-line-lg"></div>
                    <div class="aside-content-wrapper">
                        <img src="{{url('Events/'.$event->image)}}" width="348" height="250" alt="Rafting">
                        <ul class="aside-navigation">
                            <li>
                                <img class="icon-left" src="{{'images/images_wellness/location-icon.png'}}" alt="Location Icon">
                                {{$eveAddresss}}
                            </li>
                            <li>
                                <img class="icon-left" src="{{'images/images_wellness/schedule-icon.png'}}" alt="Schedule Icon">
                                {{$eventSchedule}}
                            </li>
                            <li>
                                <img class="icon-left" src="{{'images/images_wellness/schedule-icon.png'}}" alt="Schedule Icon">
                                {{$eventTimeSchedule}}
                            </li>
                            <li>
                                <span class="post-subtitle-red icon-left">Event Fees: </span>
                                <a class="post-author">₹ {{$eventFess}}</a>
                            </li>

                        </ul>
                       <a href="{{url('/cart?event_id='.base64_encode($event->id))}}">
                           <button class="uk-button btn-custom-black">book now</button>
                       </a>
                     <br><br>
                        <h4>Event Location Map</h4>
                        <div class="mapouter"><div class="gmap_canvas"><iframe width="300" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q={{urlencode($eveAddresss)}}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.utilitysavingexpert.com">Utility Saving Expert</a></div><style>.mapouter{position:relative;text-align:right;height:303px;width:300px;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:300px;}</style></div>
                    </div>

                </aside>
                <div class="border-b2 uk-width-1-1 ml-25 section-padding-top">
                </div>
            </div>
        </div>
        <!-- / container -->
    </section>
    <!-- / RAFTING END -->
    <!-- ===========================================================================
        OTHER CLASSES
        ================================================================================-->
    <section class="other-classes-wellness section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">Other Events</h2>
                    <div class="title-line-lg"></div>
                    <div class="classes-thumbnail-wellness" data-uk-slideset="{small: 2, medium: 3, large: 3}">
                        <div class="uk-slidenav-position">
                            <ul class="uk-grid uk-slideset">
                                @if(isset($allevents) && $allevents->isNotEmpty())
                                    @foreach($allevents as $ek => $ev)
                                        <li class="thumbnails-wrapper">
                                            <a href="{{url('/eventdescriptions?eventid='.base64_encode($ev->id))}}">
                                                <figure class="uk-overlay uk-animation-hover">
                                                    <div class="thumb-img-wrapper overflow-hidden">
                                                        <img class="uk-animation-scale uk-animation-reverse" src="{{url('Events/'.$ev->image)}}" alt="Rafting" width="364px" height="250px">
                                                    </div>
                                                    <figcaption class="uk-overlay-panel uk-overlay-bottom">
                                                        <div class="figcaption-wrapper uk-text-left">
                                                            <h4 class="caption-title-18" style="background: deepskyblue">{{$ev->event_name}}</h4>
                                                            <div class="title-line-sm"></div>
                                                            {{--<p class="caption-text-16">Sun- Tue, 9am  - 9pm</p>--}}
                                                            <p style="background: deepskyblue">{{isset($ev->eventAddress->event_venue)?ucwords($ev->eventAddress->event_venue):''}} | {{date('d/m/Y',strtotime($ev->event_start_date))}} at {{ date('h:i:A',strtotime($ev->event_start_time))}}</p>
                                                            <p style="background: deepskyblue">Event Fees : ₹{{isset($ev->eventTicket->price)?$ev->eventTicket->price:0}}</p>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slideset Initializer -->
        </div>
    </section>
@endsection
