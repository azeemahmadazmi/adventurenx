@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Client Profile's <strong>{{ $clientprofile->name }}</strong>
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/client-profile') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>Unique ID</th>
                                    @if(isset($clientprofile->client_id))
                                        <td>{{ 1000000000+$clientprofile->client_id}}</td>
                                    @else
                                        <td>{{ 1000000000+$clientprofile->id}}</td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Profile Image</th>
                                    @if(isset($clientprofile->image))
                                        <td><img src="{{asset('ClientProfiles/ProfileImage/'.$clientprofile->image)}}"></td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $clientprofile->name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $clientprofile->email }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    @if(isset($clientprofile->mobile))
                                        <td> {{ $clientprofile->mobile }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Pan Card Number</th>
                                    @if(isset($clientprofile->pan_card_number))
                                        <td> {{ $clientprofile->pan_card_number }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Pan Card Image</th>
                                    @if(isset($clientprofile->pan_card_image))
                                        <td><img src="{{asset('ClientProfiles/PanCard/'.$clientprofile->pan_card_image)}}"></td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> Aadhaar Card Number</th>
                                    @if(isset($clientprofile->adhaar_card_number))
                                        <td> {{ $clientprofile->adhaar_card_number }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Pan Card Image</th>
                                    @if(isset($clientprofile->adhaar_card_image))
                                        <td><img src="{{asset('ClientProfiles/AdhaarCard/'.$clientprofile->adhaar_card_image)}}"></td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Bank Account Number</th>
                                    @if(isset($clientprofile->bank_account_number))
                                        <td> {{ $clientprofile->bank_account_number }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> Bank Account Type</th>
                                    @if(isset($clientprofile->bank_account_type))
                                        <td> {{ $clientprofile->bank_account_type }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> Bank account holder name</th>
                                    @if(isset($clientprofile->bank_holder_account_name))
                                        <td> {{ $clientprofile->bank_holder_account_name }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> IFSC code</th>
                                    @if(isset($clientprofile->bank_ifsc_code))
                                        <td> {{ $clientprofile->bank_ifsc_code }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Residence Address</th>
                                    @if(isset($clientprofile->address))
                                        <td width="600px"> {!! $clientprofile->address !!} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Office Address</th>
                                    @if(isset($clientprofile->office_address ))
                                        <td width="600px"> {!! $clientprofile->office_address  !!} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Previous Experience</th>
                                    @if(isset($clientprofile->previous_expeience ))
                                        <td width="600px"> {!! $clientprofile->previous_expeience  !!} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th> Status</th>
                                    @if(isset($clientprofile->status ))
                                        <td> {!! $clientprofile->status ==1?'Enabled':'Disabled' !!} </td>
                                    @else
                                        <td>----------</td>
                                    @endif
                                </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
