<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class ForgotPasswordController extends Controller
{


    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('client_guest');
    }
    public function showLinkRequestForm(){
        return view('client.auth.passwords.email');
    }
    public function sendResetLinkEmail(Request $request){
        $this->validate($request, [
            'email' => 'required|exists:clients'
        ]);
        $data =$request->all();
        $url = URL::route('client.password.reset',['token' => $data['_token']]);

        $token = DB::table('client_password_resets')
            ->where('email','=',$data['email'])
            ->first();
         if(!$token){
               DB::table('client_password_resets')->insert([
                   'email' => $data['email'], 'token' => bcrypt($data['_token']),'created_at'=>date('Y-m-d H:i:s')
            ]);
         }
        else{
            DB::table('client_password_resets')
                ->where('email', $data['email'])->update([
                'email' => $data['email'], 'token' => bcrypt($data['_token']),'created_at'=>date('Y-m-d H:i:s')
            ]);
        }
        $data=['link' => $url,'to_email' =>$request['email'],'from_email'=>'admin@adventurenx.com','subject' =>'Reset Password','title' =>'Reset password','view' =>'client.email.resetpassword'];
        $mailstatus=$this->sendemail($data);
        if($mailstatus==1) {
            return redirect()->back()->with('status', 'Password Reset Link Send Successfully !');
        }
        else{
            return redirect()->back()->with('status','Some thing wrong! . Please try again!');
        }

    }
}
