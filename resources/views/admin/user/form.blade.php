 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($user->name) ? $user->name : ''}}" >
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="email" style="font-size:18px;">{{ 'Email' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="email" type="textarea" id="email" >{{ isset($user->email) ? $user->email : ''}}</textarea>
     {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="mobile" style="font-size:18px;">{{ 'Mobile' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="mobile" type="number" id="mobile" value="{{ isset($user->mobile) ? $user->mobile : ''}}" >
     {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
