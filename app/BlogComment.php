<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BlogComment extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */

       protected $table = 'blog_comments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_name', 'user_email', 'message', 'status','blog_id','user_id'];

    public function blog(){
        return $this->belongsTo('App\Blog');
    }

    
}
