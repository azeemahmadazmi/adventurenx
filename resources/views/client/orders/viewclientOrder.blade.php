@extends('client.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="body">
        <div class="row clearfix">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <button class="btn btn-success btn-lg btn-block waves-effect" type="button">Order's-ID <span class="badge">{{$order->id}}</span></button>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <a href="{{url('client/events/'.$order->event_id)}}" target="_blank"> <button class="btn btn-primary btn-lg btn-block waves-effect" type="button">Event's-ID <span class="badge">{{$order->event_id}}</span></button></a>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <button class="btn btn-warning btn-lg btn-block waves-effect" type="button">Total Quantity <span class="badge">{{$quantity}}</span></button>
            </div>
        </div>
    </div>
    <div class="row clearfix"></div><br><br>
    <div class="row clearfix">
        <!-- Visitors -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-pink">
                    <div class="m-b--35 font-bold"><center>Order Details</center></div>
                    <ul class="dashboard-stat-list">
                        <li>
                            Payment Status
                            <span class="pull-right"><b>{!! $order->payment_status==0?'<span class="label bg-red"><strong>Failed</strong></span>':'<span class="label bg-green"><strong>Completed</strong></span>' !!}</b></span>
                        </li>
                        <li>
                            Order Status
                            <span class="pull-right"><b>{!! $order->order_status==0?'<span class="label bg-red"><strong>In-Complete</strong></span>':'<span class="label bg-green"><strong>Completed</strong></span>' !!}</b></span>
                        </li>
                        <li>
                            Cart Total
                            <span class="pull-right"><b>₹ {{number_format($order->cart_total)}}/-</b></span>
                        </li>
                        <li>
                            Service Charge
                            <span class="pull-right"><b>₹ {{number_format($order->service_charge)}}/-</b></span>
                        </li>
                        <li>
                            Tax
                            <span class="pull-right"><b>₹ {{number_format($order->tax)}}/-</b></span>
                        </li>
                        <li>
                            Discount
                            <span class="pull-right"><b>₹ {{number_format($order->discount)}}/-</b></span>
                        </li>
                        <li>
                            Grand Total
                            <span class="pull-right"><b>₹ {{number_format($order->grand_total)}}/-</b></span>
                        </li>
                        <?php
                        //$a=new \NumberFormatter('en',\NumberFormatter::SPELLOUT);
                        // $b=ucwords($a->format($order->grand_total));
                        $b='';
                        ?>
                        <li>
                            <span class="label bg-green"><strong>{{$b}}</strong></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# Visitors -->
        <!-- Latest Social Trends -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-cyan">
                    <div class="m-b--35 font-bold"><center>Event Details</center></div>
                    <ul class="dashboard-stat-list">
                        <li>
                            Event Name
                            <span class="pull-right"><b>{{isset($order->event->event_name)?$order->event->event_name:''}}</b></span>
                        </li>
                        <li>
                            Event Created By
                            <span class="pull-right"><span class="label bg-green"><b>{{isset($order->event->created_by)?$order->event->created_by:''}}</b></span></span>
                        </li>
                        <li>
                            Event Start date
                            <span class="pull-right"><b>{{isset($order->event->event_start_date)?date('d/m/Y',strtotime($order->event->event_start_date)):''}}</b></span>
                        </li>
                        <li>
                            Event Start Time
                            <span class="pull-right"><b>{{isset($order->event->event_start_time)?date('h:i:A',strtotime($order->event->event_start_time)):''}}</b></span>
                        </li>
                        <li>
                            Event End Date
                            <span class="pull-right"><b>{{isset($order->event->event_end_date)?date('d/m/Y',strtotime($order->event->event_end_date)):''}}</b></span>
                        </li>
                        <li>
                            Event End Time
                            <span class="pull-right"><b>{{isset($order->event->event_end_time)?date('h:i:A',strtotime($order->event->event_end_time)):''}}</b></span>
                        </li>
                        <li>
                            Event Venue
                            <span class="pull-right"><b>{{isset($order->event->event_venue)?$order->event->event_venue:''}}</b></span>
                        </li>
                        <li>
                            <span class="label bg-green"><strong>{{isset($order->event->event_display_name )?$order->event->event_display_name:''}}</strong></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# Latest Social Trends -->
        <!-- Answered Tickets -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-teal">
                    <div class="font-bold m-b--35"><center>Event Ticket Details</center></div>
                    <ul class="dashboard-stat-list">
                        <li>
                            Ticket Name
                            <span class="pull-right"><b>{{isset($order->event->eventTicket->event_ticket_name)?$order->event->eventTicket->event_ticket_name:''}}</b></span>
                        </li>
                        <li>
                            Ticket Type
                            <span class="pull-right"><b>{{isset($order->event->eventTicket->type)?$order->event->eventTicket->type==1?"Normal":"Donation":''}}</b></span>
                        </li>
                        <li>
                            Event Price
                            <span class="pull-right"><b>₹ {{isset($order->event->eventTicket->price)?number_format($order->event->eventTicket->price):''}}/-</b></span>
                        </li>
                        <li>
                            AdventureNx Fees
                            <span class="pull-right"><b>{!! isset($order->event->eventTicket->adventurenx_fees)?$order->event->eventTicket->adventurenx_fees==1?'<span class="label bg-green">Owner</span>':'<span class="label bg-red">Attendee</span>':'' !!}</b></span>
                        </li>
                        <li>
                            Gateway Fees
                            <span class="pull-right"><b>{!! isset($order->event->eventTicket->gateway_fees)?$order->event->eventTicket->gateway_fees==1?'<span class="label bg-green">Owner</span>':'<span class="label bg-red">Attendee</span>':'' !!}</b></span>
                        </li>
                        <li>
                            Total Quantity
                            <span class="pull-right"><b>{{$quantity}}</b></span>
                        </li>
                        <li>
                            Booking Date
                            <span class="pull-right"><b>{{date('d M Y',strtotime($order->created_at))}}</b></span>
                        </li>
                        <li>
                            Booking Time
                            <span class="pull-right"><b>{{date('h:i:A',strtotime($order->created_at))}}</b></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# Answered Tickets -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2>TICKET TRANSACTION DETAILS</h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover dashboard-task-infos">
                        <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>Razor Payment-ID</th>
                            <th>Payment Status</th>
                            <th>Receipt</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{isset($order->transaction->id)?$order->transaction->id:''}}</td>
                            <td>{{isset($order->transaction->razorpay_payment_id)?$order->transaction->razorpay_payment_id:''}}</td>
                            <td>{!! isset($order->transaction->status)?$order->transaction->status==1?'<span class="label bg-green">Completed</span>':'<span class="label bg-red">Failed</span>':'' !!}</td>
                            <td>
                                <a href="{{ url('client/reciept?order_id='.base64_encode($order->id)) }}" title="Print Receipt" target="_blank">
                                    <button class="btn btn-info btn-xs"><i class="material-icons">print</i>
                                        Receipt
                                    </button>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2>USER's DETAILS</h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover dashboard-task-infos">
                        <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Mobile</th>
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($userDetails) && !empty($userDetails))
                            @php  $count=0; @endphp
                            @foreach($userDetails as $uk => $uv)
                                @php  $count++; @endphp
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$uv['fullname']}}</td>
                                    <td>{{$uv['email']}}</td>
                                    <td>{{date('d M Y',strtotime($uv['dob']))}}</td>
                                    <td>{{$uv['mobile']}}</td>
                                    <td>{{ucwords($uv['gender'])}}</td>
                                    <td><a href="#" title="View User's Detail" data-toggle="modal" data-target="#myModal-{{$count}}">
                                            <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                View
                                            </button>
                                        </a></td>

                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if(isset($userDetails) && !empty($userDetails))
        @php  $counts=0; @endphp
        @foreach($userDetails as $ok => $ov)
            @php
                $counts++;
                $eventTicket=App\EventTicket::find($ov['event_ticket_id']);
            @endphp
            <div class="modal" id="myModal-{{$counts}}">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-body">
                            <h3>Details of <span style="color:green">{{$ov['fullname']}}</span></h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th> Event Ticket Name</th>
                                        <td>{{$eventTicket->event_ticket_name}}</td>
                                    </tr>
                                    <tr>
                                        <th> Event Unit price</th>
                                        <td>{{$eventTicket->price}}</td>
                                    </tr>
                                    <tr>
                                        <th> Name</th>
                                        <td>{{$ov['fullname']}}</td>
                                    </tr>
                                    <tr>
                                        <th> Email</th>
                                        <td>{{$ov['email']}}</td>
                                    </tr>
                                    <tr>
                                        <th> Date of Birth</th>
                                        <td>{{date('d F Y',strtotime($ov['dob']))}} </td>
                                    </tr>
                                    <tr>
                                        <th> Mobile</th>
                                        <td> {{$ov['mobile']}} </td>
                                    </tr>
                                    <tr>
                                        <th> Gender</th>
                                        <td>{{ucwords($ov['gender'])}}  </td>
                                    </tr>
                                    <tr>
                                        <th> Blood Group</th>
                                        <td> {{$ov['blood_group']}} </td>
                                    </tr>
                                    <tr>
                                        <th> T-shirt Size</th>
                                        <td>  {{$ov['tshirt']}}</td>
                                    </tr>
                                    <tr>
                                        <th> Emergency Contact Name</th>
                                        <td> {{$ov['emergency_name']}} </td>
                                    </tr>
                                    <tr>
                                        <th> Emergency Contact Number</th>
                                        <td> {{$ov['emergency_contact']}} </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection
