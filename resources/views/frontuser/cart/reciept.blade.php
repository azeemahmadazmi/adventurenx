<html>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
    .text-danger strong {
        color: #9f181c;
    }

    .receipt-main {
        background: #ffffff none repeat scroll 0 0;
        border-bottom: 12px solid #333333;
        border-top: 12px solid #9f181c;
        margin-top: 50px;
        margin-bottom: 50px;
        padding: 40px 30px !important;
        position: relative;
        box-shadow: 0 1px 21px #acacac;
        color: #333333;
        font-family: open sans;
    }

    .receipt-main p {
        color: #333333;
        font-family: open sans;
        line-height: 1.42857;
    }

    .receipt-footer h1 {
        font-size: 15px;
        font-weight: 400 !important;
        margin: 0 !important;
    }

    .receipt-main::after {
        background: #414143 none repeat scroll 0 0;
        content: "";
        height: 5px;
        left: 0;
        position: absolute;
        right: 0;
        top: -13px;
    }

    .receipt-main thead {
        background: #414143 none repeat scroll 0 0;
    }

    .receipt-main thead th {
        color: #fff;
    }

    .receipt-right h5 {
        font-size: 16px;
        font-weight: bold;
        margin: 0 0 7px 0;
    }

    .receipt-right p {
        font-size: 12px;
        margin: 0px;
    }

    .receipt-right p i {
        text-align: center;
        width: 18px;
    }

    .receipt-main td {
        padding: 9px 20px !important;
    }

    .receipt-main th {
        padding: 13px 20px !important;
    }

    .receipt-main td {
        font-size: 13px;
        font-weight: initial !important;
    }

    .receipt-main td p:last-child {
        margin: 0;
        padding: 0;
    }

    .receipt-main td h2 {
        font-size: 20px;
        font-weight: 900;
        margin: 0;
        text-transform: uppercase;
    }

    .receipt-header-mid .receipt-left h1 {
        font-weight: 100;
        margin: 34px 0 0;
        text-align: right;
        text-transform: uppercase;
    }

    .receipt-header-mid {
        margin: 24px 0;
        overflow: hidden;
    }

    #container {
        background-color: #dcdcdc;
    }

    @media(max-width: 768px){
    #recieptsize{
        font-size: 20px !important;
    }
        .totalamount{
            font-size: 16px !important;
        }
    }
    @media(max-width: 375px){
        #recieptsize{
            font-size: 18px !important;
        }
        .totalamount{
            font-size: 14px !important;
        }
    }


</style>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
    window.alert = function () {
    };
    var defaultCSS = document.getElementById('bootstrap-css');
    function changeCSS(css) {
        if (css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="' + css + '" type="text/css" />');
        else $('head > link').filter(':first').replaceWith(defaultCSS);
    }

</script>

<body>

    <div class="container">
        <div class="row">

            <div class="receipt-main col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
                <div class="row">
                    <div class="receipt-header">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="receipt-left">
                                @if(isset($users->image) && $users->image !=null)
                                <img class="img-responsive" alt="iamgurdeeposahan"
                                     src="{{$users->image}}"
                                     style="width: 71px; border-radius: 43px;">
                                    @else
                                    <img class="img-responsive" alt="iamgurdeeposahan"
                                         src="{{asset('dummy-profile-pic.jpg')}}"
                                         style="width: 71px; border-radius: 43px;">
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                            <div class="receipt-right">
                                <h5>AdventureNX</h5>

                                <p>+91-9969695882 <i class="fa fa-phone"></i></p>

                                <p>adventurenx@gmail.com <i class="fa fa-envelope-o"></i></p>

                                <p>Mumbai,Maharashtra-India <i class="fa fa-location-arrow"></i></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="receipt-header receipt-header-mid">
                        <div class="col-xs-8 col-sm-8 col-md-8 text-left">
                            <div class="receipt-right">
                                <h5>{{$users->name}}

                                </h5>
                                <p><b>Mobile :</b> +91-{{$users->mobile}}</p>

                                <p><b>Email :</b> {{$users->email}}</p>

                                <p><b>Address :</b> {{$users->address}}</p>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="receipt-left">
                                <h1 id="recieptsize">Receipt</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="col-md-9">Order ID</td>
                            <td class="col-md-3"><strong>{{$order->id}}</strong></td>
                        </tr>
                        <tr>
                            <td class="col-md-9">Event ID</td>
                            <td class="col-md-3"><strong>{{$event->id}}</strong></td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td class="col-md-9">Event Price</td>--}}
                            {{--<td class="col-md-3"><i class="fa fa-inr"></i> <strong>{{number_format($eventTicket->price)}}/-</strong></td>--}}
                        {{--</tr>--}}


                        <tr>
                            <td class="col-md-9">Total Quantity</td>
                            <td class="col-md-3"><strong>{{$totalQuantity}}</strong></td>
                        </tr>
                        <tr>
                            <td class="col-md-9">Order Status</td>
                            <td class="col-md-3">{!! $order->order_status==0?'<span style="color:red"><strong>In-Complete</strong></span>':'<span style="color:green"><strong>Completed</strong></span>' !!}</td>
                        </tr>
                        <tr>
                            <td class="col-md-9">Payment Status</td>
                            <td class="col-md-3">{!! $order->payment_status==0?'<span style="color:red"><strong>Failed</strong></span>':'<span style="color:green"><strong>Completed</strong></span>' !!}</td>
                        </tr>


                        <tr>
                            <td class="text-right">
                                <p>
                                    <strong>Total Amount: </strong>
                                </p>

                                <p>
                                    <strong>Tax: </strong>
                                </p>

                                <p>
                                    <strong>Service Charge: </strong>
                                </p>

                                <p>
                                    <strong>Discount: </strong>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <strong><i class="fa fa-inr"></i> {{number_format($order->cart_total)}}/-</strong>
                                </p>

                                <p>
                                    <strong><i class="fa fa-inr"></i> {{number_format($order->tax)}}/-</strong>
                                </p>

                                <p>
                                    <strong><i class="fa fa-inr"></i> {{number_format($order->service_charge)}}/-</strong>
                                </p>

                                <p>
                                    <strong><i class="fa fa-inr"></i> {{number_format($order->discount)}}/-</strong>
                                </p>
                            </td>
                        </tr>
                        <tr>

                            <td class="text-right"><h2><strong class="totalamount">Total: </strong></h2></td>
                            <td class="text-left text-danger"><h2><strong class="totalamount"><i class="fa fa-inr"></i> {{number_format($order->grand_total)}}/-</strong>
                                </h2></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <?php
                                   // $a=new \NumberFormatter('en',\NumberFormatter::SPELLOUT);
                                   // $b=ucwords($a->format($order->grand_total));
                            $b='';
                            ?>
                            <td class="col-md-4"><strong>Amounts in Words</strong></td>
                            <td class="col-md-8"><strong style="color: green">{{$b}}</strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="receipt-header receipt-header-mid receipt-footer">
                        <div class="col-xs-8 col-sm-8 col-md-8 text-left">
                            <div class="receipt-right">
                                <p><b>Booking Date :</b> {{date('d F Y',strtotime($order->created_at))}}</p>
                                <h5 style="color: rgb(140, 140, 140);">Thanks from AdventureNx Team!</h5>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="receipt-left">
                                <h1>Signature</h1>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script src="{{asset('vendor/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('vendor/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
        <script>
            $(document).ready(function(){
                window.print();
            });
        </script>

    </body>
</html>




