<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\ClientProfile;
use App\Event;
use App\Order;


class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('client_auth');
    }

    public function index()
    {
        $event=Event::where('created_by','CLIENT')->where('creator_id',Auth::guard('client')->user()->id)->count();
        $eventnumber=Event::where('created_by','CLIENT')->where('creator_id',Auth::guard('client')->user()->id)->pluck('id');
        $orders=Order::whereIn('event_id',$eventnumber)->count();
        return view('client.home',compact('event','orders'));
    }

    public function profile()
    {
        return view('client.profile');
    }

    public function saveprofile(Request $request)
    {
        $user_list = Auth::guard('client')->user();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:clients,email,' . $user_list->id,
            ]);
        } else {
            if (!Hash::check($request['old_password'], $user_list->password)) {
                return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
            }

            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:clients,email,' . $user_list->id,
                'password' => 'required|string|min:6|confirmed|different:old_password',
                'old_password' => 'required|string|min:6'
            ]);
        }
        $data = $request->all();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'image' => $filename
                ]);
            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                ]);
            }

        } else {

            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'image' => $filename
                ]);

            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password'])
                ]);
            }
        }
        return redirect('/client/profile')->with('flash_message', 'Profile updated Successfully!');
    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }


    public function show()
    {
        $clientId=Auth::guard('client')->user()->id;
        $clientprofile = ClientProfile::where('client_id',$clientId)->first();
        if($clientprofile){
            $clientprofile = ClientProfile::where('client_id',$clientId)->first();
        }
        else{
            $clientprofile = Auth::guard('client')->user();
        }

        return view('client.client-profile.show', compact('clientprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $clientId=Auth::guard('client')->user()->id;
        $clientprofile = ClientProfile::where('client_id',$clientId)->first();
        if($clientprofile){
            $clientprofile = ClientProfile::where('client_id',$clientId)->first();
        }
        else{
            $clientprofile = Auth::guard('client')->user();
        }
        return view('client.client-profile.edit', compact('clientprofile'));

    }

    public function update(Request $request)
    {

        $clientId=Auth::guard('client')->user()->id;
        $clientprofile = ClientProfile::where('client_id',$clientId)->first();

        if($clientprofile){
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:client_profiles,email,'.$clientprofile->id,
                'mobile' => 'required|unique:client_profiles,mobile,'.$clientprofile->id,
                'pan_card_number' => 'required|unique:client_profiles,pan_card_number,'.$clientprofile->id,
                'adhaar_card_number' => 'required|unique:client_profiles,adhaar_card_number,'.$clientprofile->id
            ]);
        }
        else{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:client_profiles',
                'mobile' => 'required|unique:client_profiles',
                'pan_card_number' => 'required|unique:client_profiles',
                'adhaar_card_number' => 'required|unique:client_profiles'
            ]);
        }

        $requestData = $request->all();
        if ($request->hasFile('image')) {
        $filename = $this->getFileName($request->image);
       $request->image->move(base_path('public/ClientProfiles/ProfileImage'), $filename);
        $requestData['image']=$filename;
        }
        if ($request->hasFile('pan_card_image')) {
            $filename = $this->getFileName($request->pan_card_image);
           $request->pan_card_image->move(base_path('public/ClientProfiles/PanCard'), $filename);
            $requestData['pan_card_image']=$filename;
        }
        if ($request->hasFile('adhaar_card_image')) {
            $filename = $this->getFileName($request->adhaar_card_image);
            $request->adhaar_card_image->move(base_path('public/ClientProfiles/AdhaarCard'), $filename);
            $requestData['adhaar_card_image']=$filename;
        }
        $requestData['client_id']=$clientId;
        if($clientprofile){
            $clientprofile->update($requestData);
        }
        else{
            ClientProfile::create($requestData);
        }

        return redirect('client/show-client-profile')->with('flash_message', 'ClientProfile updated!');
    }


}
