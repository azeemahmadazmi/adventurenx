@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative single-post-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Single Post Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">blog</a></li>
                        <li class="uk-active"><a href="#">{{$bv->name}}</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">{{$bv->name}}</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="single-post section-padding-top blog-single-wellness">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">{{$bv->name}}</h2>
                    <div class="title-line-lg"></div>
                    <div class="uk-grid single-post-body">
                        <div class="uk-width-1-1">
                            <img class="single-post-inner-image" src="{{asset('Blog/'.$bv->image)}}" alt="Single Post Image" width="1130px" height="675px">
                        </div>
                        <div class="uk-width-medium-8-10 single-post-article">
                            <div class="text-sixteen single-post-content">
                                <blockquote>
                                    {!! $bv->about !!}
                                </blockquote>
                            </div>
                            {!! $bv->descriptions !!}
                            <!-- / .single-post-content -->
                            <section class="comments">
                                <h4 class="uk-text-uppercase h-4">comments</h4>
                                <span class="title-line-lg"></span>
                                <ul class="uk-comment-list appendrecentblock">
                                @if(isset($blogComment) && $blogComment->isNotEmpty())
                                    @foreach($blogComment as $key => $bgvalue)
                                        <?php
                                                $users=App\User::find($bgvalue->user_id);
                                        ?>
                                    <li>
                                        <div class="uk-comment">
                                            <div class="uk-grid uk-grid-small">
                                                <div class="comments-profile-img uk-width-1-10">
                                                    @if(isset($users->image) && $users->image !=null)
                                                        <img src="{{$users->image}}" alt="Profile Picture" width="65px" height="65px">
                                                        @else
                                                    <img src="{{asset('dummy-profile-pic.jpg')}}" alt="Profile Picture" width="65px" height="65px">
                                                        @endif
                                                </div>
                                                <div class="uk-width-9-10">
                                                    <div class="post-comment-body light-sky-blue">
                                                        <ul class="blog-info-list">
                                                            <li><a class="post-author" href="#">{{$bgvalue->user_name}}</a></li>
                                                            <li class="post-date">{{date('d/m/Y',strtotime($bgvalue->created_at))}}</li>
                                                            <li class="post-date">at: {{date('h:i:A',strtotime($bgvalue->created_at))}}</li>
                                                        </ul>
                                                        <p class="text-sixteen">{!! $bgvalue->message !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                        @endforeach
                                    @endif
                                </ul>
                                <!-- / comments-list -->
                            </section>
                            <!-- / .comments -->
                            <section class="write-comment-form">
                                <h4 class="h-4">leave a comment</h4>
                                <div class="title-line-lg"></div>
                                <h3 id="formsubmitmessage"></h3>
                                <form class="uk-form form-custom" id="commentform">
                                    <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                                        <input type="hidden" id="authuser" @if(Auth::check()) value="1" @else value="0" @endif>
                                        <input type="hidden" name="blog_id" value="{{$bv->id}}">
                                        <div class="uk-width-medium-1-2">
                                            <input type="text" @if(Auth::check()) value="{{Auth::user()->name}}" @endif placeholder="Name" name="name" class="uk-width-1-1  input-custom" required>
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            <input type="email" @if(Auth::check()) value="{{Auth::user()->email}}" @endif placeholder="E-mail" name="email" class="uk-width-1-1 input-custom" required>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <textarea placeholder="Message" class="uk-width-1-1 input-custom" name="message" required></textarea>
                                        </div>
                                        <div class="uk-width-1-1 form-btn-grid">
                                            <input class="uk-button btn-custom-black" type="submit" value="send">
                                        </div>
                                    </div>
                                </form>
                            </section>
                            <!-- / .write-comment-form -->
                        </div>
                        <!-- / .single-post-article -->
                    </div>
                </div>
            </div>
        </div>
        <!-- / container -->
    </section>
    <!-- ===========================================================================
        RECENT POST SLIDESET
        ================================================================================-->
    <section class="recent-posts section-padding-top section-padding-bottom light-sky-blue">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">be well with adventure-nx</h2>
                    <div class="title-line-lg"></div>
                    <!-- ABOUT SLIDESET  -->
                    <div class="about-slideset">
                        <div data-uk-slideset="{small: 2, medium: 3, large: 3}">
                            <div class="uk-slidenav-position">
                                <!-- slideset item list -->
                                <ul class="uk-grid uk-slideset">
                                    @if(isset($eventCategory) && $eventCategory->isNotEmpty())
                                        @foreach($eventCategory as $evk => $evv)
                                    <li class="uk-text-center">
                                        <figure class="uk-overlay uk-animation-hover">
                                            <div class="overflow-hidden thumb-img-wrapper">
                                                <img class="uk-animation-scale uk-animation-reverse" src="{{asset('Events/EventsCategories/'.$evv->image)}}" alt="Post Images" width="364px" height="605px">
                                            </div>
                                            <figcaption class="uk-overlay-panel uk-overlay-bottom uk-text-left">
                                                <div>
                                                    <h3 class="h-3">{{$evv->name}}</h3>
                                                    <div class="title-line-lg"></div>
                                                    {!! substr($evv->about, 0, 100) !!}
                                                    <a class="text-overview">Read more<img class="icon-right" src="{{asset('images/images_wellness/icon-arrow-right.png')}}" alt="Read More Icon"></a>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </li>
                                    @endforeach
                                @endif
                                    <!-- / slideset item list end -->
                                </ul>
                                <!-- dotnav -->
                                <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / ABOUT SLIDESET END -->
        </div>
        <!-- / uk-container end -->
    </section>
@endsection

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        $('#commentform').submit(function(event){
            event.preventDefault();
            var user =$('#authuser').val();
            if(user == 0){
                alert('Please Login first to leave the comment');
                return false;
            }
            else{
                var formData =$(this).serialize();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/blogcomments",
                    type: 'POST',
                    data: formData,

                    beforeSend: function () {

                    },
                    success: function (response) {
                            $('#formsubmitmessage').html('Your Comment submitted successfully');
                            $('#formsubmitmessage').css('color','green');
                            $("#commentform").trigger("reset");
                            $('.appendrecentblock').append(response);

                    }
                });
            }
        });
    </script>

    @endsection