<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Event extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */

       protected $table = 'events';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_name', 'event_display_name', 'event_visibility', 'event_start_date', 'event_start_time', 'event_end_date', 'event_end_time', 'event_venue', 'event_descriptions','image','status','created_by','creator_id'];


    public function eventAddress(){
        return $this->hasOne('App\EventAddress');
    }

    public function eventTicket(){
        return $this->hasOne('App\EventTicket');
    }

    
}
