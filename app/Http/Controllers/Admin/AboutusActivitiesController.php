<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AboutusActivity;
use Illuminate\Http\Request;
class AboutusActivitiesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $aboutusactivities = AboutusActivity::where('name', 'LIKE', "%$keyword%")
                ->orWhere('count', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $aboutusactivities = AboutusActivity::latest()->paginate($perPage);
        }

        return view('admin.aboutus-activities.index', compact('aboutusactivities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.aboutus-activities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        AboutusActivity::create($requestData);

        return redirect('admin/aboutus-activities')->with('flash_message', 'AboutusActivity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $aboutusactivity = AboutusActivity::findOrFail($id);

        return view('admin.aboutus-activities.show', compact('aboutusactivity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $aboutusactivity = AboutusActivity::findOrFail($id);

        return view('admin.aboutus-activities.edit', compact('aboutusactivity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        $aboutusactivity = AboutusActivity::findOrFail($id);
        $aboutusactivity->update($requestData);

        return redirect('admin/aboutus-activities')->with('flash_message', 'AboutusActivity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AboutusActivity::destroy($id);

        return redirect('admin/aboutus-activities')->with('flash_message', 'AboutusActivity deleted!');
    }
}
