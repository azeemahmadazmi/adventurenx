<?php

namespace App\Http\Controllers\FrontUser;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Banner;
use App\Event;
use App\EventTicket;
use App\EventAddress;
use Illuminate\Support\Facades\Session;
use App\Cart;
use App\EventDynamicForms;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Input;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function cartpayment(Request $request)
    {
        $data = $request->all();
        $data['total_quantity']=array_sum($data['quantity']);
        $quantity = $data['total_quantity'];
        $requestData['user_id'] = Auth::user()->id;
        $req['cart']['quantity'] = $quantity;
        $req['cart']['event_id'] = $data['event_id'];
        $req['cart']['total_quantity']=serialize(array_filter($data['quantity']));
         $eventTicketDetails=array_filter($data['quantity']);
         $totalcountevents=count($eventTicketDetails);
        $requestData['cart'] = serialize($req['cart']);
        $cartDetails = Cart::where('user_id', Auth::user()->id)->first();
        $cartDetails['user_details'] = null;
        if ($cartDetails) {
            $cartDetails->update($requestData);
        } else {
            Cart::create($requestData);
        }
        $html = '';
        $html .= '<span style="display: none">
        <div class="">
            <ul class="list">
                <li class="active">
                </li>
            </ul>
        </div>
</span>
<link href="' . asset('plugins/bootstrap/css/bootstrap.css') . '" rel="stylesheet">
<link href="' . asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') . '"
    rel="stylesheet"/>
<link href="' . asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') . '" rel="stylesheet"/>
<link rel="stylesheet" href="' . asset('wp-content/plugins/elementor/assets/css/frontend.min91ac.css') . '"/>
<link href="' . asset("css/style.css") . '" rel="stylesheet">
<style>
#cart_banner{
display:none;
}
footer{
display:none;
}
</style>
<section>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-offset-3  col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                <h4 style="background: green;color: white">
                    <center>ATTENDEE ';
        if ($quantity > 1) {
            $html .= '#1';
        }
        $html .= '</center>
                </h4>
                <div class="card">
                    <div class="body">';
        if ($quantity > 1) {
            $html .= '<form id="form_validation" class="next_users_detaisl" accept-charset="UTF-8" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="' . csrf_token() . '">
                                  <input type="hidden" name="quantity" value="' . $quantity . '">                             
                                  <input type="hidden" name="event_id" value="' . $data['event_id'] . '">';
        } else {
            $html .= '<form id="form_validation" method="POST" action="' . url("/userdetails_attendee") . ' " accept-charset="UTF-8" enctype="multipart/form-data">
                               <input type="hidden" name="_token" value="' . csrf_token() . '">
                               <input type="hidden" name="event_id" value="' . $data['event_id'] . '">
                               <input type="hidden" name="quantity" value="' . $quantity . '">
                               ';
        }
        $html .= '<div class="form-group form-float">
                                <div class="form-line">';
                                 if($totalcountevents > 1){
                                     $html.='<select type="text" class="form-control" name="event_ticket_id" required>
                                             <option value=""></option> ';
                                     foreach ($eventTicketDetails as $evk => $evv){
                                             $ticketDetails = EventTicket::find($evk);
                                             $html .= '<option value="' . $ticketDetails->id . '">' . $ticketDetails->event_ticket_name . '</option>';
                                     }
                                     $html.=' </select>';

                                 }
                                 else{
                                     foreach ($eventTicketDetails as $evk => $evv){
                                             $ticketDetails=EventTicket::find($evk);
                                             break;
                                     }
                                   $html.='<input type="text" class="form-control" name="event_ticket" value="'.$ticketDetails->event_ticket_name.'" required>
                                    <input type="hidden" name="event_ticket_id" value="'.$ticketDetails->id.'">';
                                 }

                                    $html.='<label class="form-label">Event Ticket Name</label>
                                </div>
                            </div>
<div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="form_fullname" required>
                                    <label class="form-label">Full Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" name="form_email" required>
                                    <label class="form-label">Email</label>
                                </div>
                            </div>';
                             $formfield=unserialize($ticketDetails->form_fields);

                             if(isset($formfield) && !empty($formfield)){
                                 foreach ($formfield as $formfil){

                                     if($formfil=='form_mobile'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
                               <input class="form-control"  name="'.$form_field->name.'" required type="number" >  
      <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';

                                     }
                                     elseif ($formfil=='form_title'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html .='<div class="form-group form-float">
                                <div class="form-line">
      <select class="form-control" name="'.$form_field->name.'" required>';
                                         foreach (json_decode('{"mr": "MR", "mrs": "MRS","miss":"MISS","dr":"DR","engg":"ER","prof":"PROF"}', true) as $optionKey => $optionValue){
                                             $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                                         }
                                         $html .='</select>
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif ($formfil=='form_gender'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html .='<div class="form-group form-float">
                                <div class="form-line"><select class="form-control" name="'.$form_field->name.'" required>';
                                         foreach (json_decode('{"male": "Male", "female": "Female","other":"Other"}', true) as $optionKey => $optionValue){
                                             $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                                         }
                                         $html .='</select>
<label class="form-label">'. $form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_designations'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
  <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_organisations'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
   <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_address'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
  <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_city'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
  <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_state'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_country'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->dispaly_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_pincode'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_emergency_contact_number'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();

                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  name="'.$form_field->name.'" required type="number">  
        <label class="form-label">'.$form_field->display_name.'</label>
      </div>
 </div>';
                                     }
                                     elseif($formfil=='form_emergency_contact_name'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();

                                         $html.=' <div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_emergency_contact_relations'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_dob'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control datepicker"  id="datepicker" type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }

                                     elseif ($formfil=='form_blood_group'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html .='<div class="form-group form-float">
                                <div class="form-line"><select class="form-control" name="'.$form_field->name.'" required>';
                                         foreach (json_decode('{"A+": "A+", "O+": "O+","B+":"B+","B+":"B+","AB+":"AB+","A-":"A-","O-":"O-","B-":"B-","AB-":"AB-"}', true) as $optionKey => $optionValue){
                                             $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                                         }
                                         $html .='</select><label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif ($formfil=='form_tshirt_size'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html .='<div class="form-group form-float">
                                <div class="form-line"><select class="form-control" name="'.$form_field->name.'" required>';
                                         foreach (json_decode('{"XS": "XS", "S": "S","M":"M","L":"L","XL":"XL","XLL":"XLL","XLLL":"XLLL"}', true) as $optionKey => $optionValue){
                                             $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                                         }
                                         $html .='</select><label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_attendee_photo'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="file" name="'.$form_field->name.'" required>  
<label class="form-label" style="top: -16px; !important;">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                     elseif($formfil=='form_attende_id_proof'){
                                         $form_field=EventDynamicForms::where('name',$formfil)->first();
                                         $html.='<div class="form-group form-float">
                                <div class="form-line" name="'.$form_field->name.'" required>
        <input class="form-control"  type="file">  
 <label class="form-label" style="top: -16px; !important;">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                                     }
                                 }
                             }



                           $html.='<button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
   <script src="' . asset('plugins/jquery/jquery.min.js') . '"></script>
    <script src="' . asset('plugins/bootstrap/js/bootstrap.js') . '"></script>
    <script src="' . asset('plugins/jquery-validation/jquery.validate.js') . '"></script>
    <script src="' . asset("plugins/node-waves/waves.js") . '"></script>
    <script src="' . asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') . '"></script>
    <script src="' . asset('js/admin.js') . '"></script>
    <script src="' . asset('js/pages/forms/form-validation.js') . '"></script>
    <script src="' . asset('js/uikit.min.js') . '"></script>
   ';
        return $html;
        //return view('frontuser.cart.makepayemt',compact('banner','tax','servicetax','totalPayment','event','lastInserId'));

    }

    public function users_tickets_description(Request $request)
    {
        return view('frontuser.users_tickets_description');
    }

    public function userdetails_attendee(Request $request)
    {

        $data = $request->all();


        $cartDetails = Cart::where('user_id', Auth::user()->id)->first();
        if ($cartDetails->user_details != null) {
            $userData = unserialize($cartDetails->user_details);
            $userdetailscount = count($userData) + 1;
        } else {
            $userdetailscount = 1;
        }

        if ($request->hasFile('form_attendee_photo')) {
            $filename = $cartDetails->id.'_'.Input::file('form_attendee_photo')->getClientOriginalName();
            $request->form_attendee_photo->move(base_path('public/AttendeeImage'), $filename);
            $data['form_attendee_photo']=$filename;
        }
        if ($request->hasFile('form_attende_id_proof')) {
            $filename = $cartDetails->id.'_'.Input::file('form_attende_id_proof')->getClientOriginalName();
            $request->form_attende_id_proof->move(base_path('public/AttendeeIdProof'), $filename);
            $data['form_attende_id_proof']=$filename;
        }
        $userdetails = ['user-' . $userdetailscount => Arr::except($data, ['_token','quantity','event_id','event_ticket']) ];

        if ($cartDetails->user_details != null) {

            $finaluserdetail = array_merge($userdetails, $userData);
        } else {
            $finaluserdetail = $userdetails;
        }
        $cartDetails->user_details = serialize($finaluserdetail);
        $cartDetails->save();
        return redirect('/final_cart_payment');

    }

    public function final_cart_payment()
    {
        $cartDetails = Cart::where('user_id', Auth::user()->id)->first();
        $cart = unserialize($cartDetails['cart']);
        $qun=unserialize($cartDetails->user_details);
        $tax=0;
        $servicetax=0;
        $price=0;
       foreach ($qun as $qk => $qv){
           $eventTicket = EventTicket::findOrFail($qv['event_ticket_id']);

           if ($eventTicket->adventurenx_fees == 1) {
               $tax += 0;
           } else {
               $tax += 2 * ($eventTicket->price * 1) / 100;
           }

           if ($eventTicket->gateway_fees == 1) {
               $servicetax += 0;
           } else {
               $servicetax += 2 * ($eventTicket->price * 1) / 100;
           }
           $price +=$eventTicket->price;

       }
        $eventID = $cart['event_id'];
        $event = Event::find($eventID);
        $lastInserId = $cartDetails->id;
        $banner = Banner::where('name', '=', 'cartpayment')->latest()->first();
        $totalPayment = $price  + $tax + $servicetax;
        return view('frontuser.cart.makepayemt', compact('banner', 'tax', 'servicetax', 'totalPayment', 'event', 'lastInserId','qun'));

    }

    public function next_users_detaisl(Request $request)
    {
        $data = $request->all();
        $cartDetails = Cart::where('user_id', Auth::user()->id)->first();
        $qun=unserialize($cartDetails->cart);
        $eventTicketDetails=unserialize($qun['total_quantity']);
        $totalcountevents=count($eventTicketDetails);
        $quantity = $data['quantity'];
        if ($cartDetails->user_details != null) {
            $userData = unserialize($cartDetails->user_details);
            $userdetailscount = count($userData) + 1;
        } else {
            $userdetailscount = 1;
        }
        if ($request->hasFile('form_attendee_photo')) {
            $filename = $cartDetails->id.'_'.Input::file('form_attendee_photo')->getClientOriginalName();
            $request->form_attendee_photo->move(base_path('public/AttendeeImage'), $filename);
            $data['form_attendee_photo']=$filename;
        }
        if ($request->hasFile('form_attende_id_proof')) {
            $filename = $cartDetails->id.'_'.Input::file('form_attende_id_proof')->getClientOriginalName();
            $request->form_attende_id_proof->move(base_path('public/AttendeeIdProof'), $filename);
            $data['form_attende_id_proof']=$filename;
        }
        $userdetails = ['user-' . $userdetailscount => Arr::except($data, ['_token','quantity','event_id','event_ticket']) ];


        if ($cartDetails->user_details != null) {
            $totalCount = unserialize($cartDetails->user_details);
            $finaluserdetail = array_merge($userdetails, $totalCount);
        } else {
            $finaluserdetail = $userdetails;
        }

        $cartDetails->user_details = serialize($finaluserdetail);
        $cartDetails->save();


        if ($cartDetails->user_details != null) {
            $totalCount = unserialize($cartDetails->user_details);
            $totalCountusers = count($totalCount);
            $remaining = (int)$quantity - count($totalCount);
        } else {
            $totalCountusers = 0;
            $remaining = (int)$quantity - 0;
        }
        $html = '';
        $html .= '<span style="display: none">
        <div class="">
            <ul class="list">
                <li class="active">
                </li>
            </ul>
        </div>
</span>
<link href="' . asset('plugins/bootstrap/css/bootstrap.css') . '" rel="stylesheet">
<link href="' . asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') . '"
    rel="stylesheet"/>
<link href="' . asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') . '" rel="stylesheet"/>
<link rel="stylesheet" href="' . asset('wp-content/plugins/elementor/assets/css/frontend.min91ac.css') . '"/>
<link href="' . asset("css/style.css") . '" rel="stylesheet">
<style>
#cart_banner{
display:none;
}
footer{
display:none;
}
</style>
<section>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-offset-3  col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                <h4 style="background: green;color: white">
                    <center>ATTENDEE #' . ($totalCountusers + 1) . '</center>
                </h4>
                <div class="card">
                    <div class="body">';
        if ($remaining > 1) {
            $html .= '<form id="form_validation" class="next_users_detaisl" accept-charset="UTF-8" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="' . csrf_token() . '">
                                  <input type="hidden" name="quantity" value="' . $quantity . '">              
                                  <input type="hidden" name="event_id" value="' . $data['event_id'] . '">';
        } else {
            $html .= '<form id="form_validation" method="POST" action="' . url("/userdetails_attendee") . '" accept-charset="UTF-8" enctype="multipart/form-data">
                               <input type="hidden" name="_token" value="' . csrf_token() . '">
                               <input type="hidden" name="event_id" value="' . $data['event_id'] . '">
                               <input type="hidden" name="quantity" value="' . $quantity . '">
                               ';
        }
        $html.='<div class="form-group form-float">
                                <div class="form-line">';
                                 if($totalcountevents > 1){
                                     $html.='<select type="text" class="form-control" name="event_ticket_id" required>
                                             <option value=""></option> ';
                                     foreach ($eventTicketDetails as $evk => $evv){
                                             $ticketDetails = EventTicket::find($evk);
                                             $html .= '<option value="' . $ticketDetails->id . '">' . $ticketDetails->event_ticket_name . '</option>';
                                     }
                                     $html.=' </select>';
                                 }
                                 else{
                                     foreach ($eventTicketDetails as $evk => $evv){
                                             $ticketDetails=EventTicket::find($evk);
                                             break;
                                     }
                                   $html.='<input type="text" class="form-control" name="event_ticket" value="'.$ticketDetails->event_ticket_name.'" required>
                                    <input type="hidden" name="event_ticket_id" value="'.$ticketDetails->id.'">';
                                 }

        $html.='<label class="form-label">Event Ticket Name</label>
                                </div>
                            </div>
<div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="form_fullname" required>
                                    <label class="form-label">Full Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" name="form_email" required>
                                    <label class="form-label">Email</label>
                                </div>
                            </div>';
        $formfield=unserialize($ticketDetails->form_fields);

        if(isset($formfield) && !empty($formfield)){
            foreach ($formfield as $formfil){

                if($formfil=='form_mobile'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
                               <input class="form-control"  name="'.$form_field->name.'" required type="number" >  
      <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';

                }
                elseif ($formfil=='form_title'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html .='<div class="form-group form-float">
                                <div class="form-line">
      <select class="form-control" name="'.$form_field->name.'" required>';
                    foreach (json_decode('{"mr": "MR", "mrs": "MRS","miss":"MISS","dr":"DR","engg":"ER","prof":"PROF"}', true) as $optionKey => $optionValue){
                        $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                    }
                    $html .='</select>
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif ($formfil=='form_gender'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html .='<div class="form-group form-float">
                                <div class="form-line"><select class="form-control" name="'.$form_field->name.'" required>';
                    foreach (json_decode('{"male": "Male", "female": "Female","other":"Other"}', true) as $optionKey => $optionValue){
                        $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                    }
                    $html .='</select>
<label class="form-label">'. $form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_designations'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
  <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_organisations'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
   <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_address'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
  <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_city'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
  <label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_state'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_country'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->dispaly_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_pincode'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_emergency_contact_number'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();

                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  name="'.$form_field->name.'" required type="number">  
        <label class="form-label">'.$form_field->display_name.'</label>
      </div>
 </div>';
                }
                elseif($formfil=='form_emergency_contact_name'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();

                    $html.=' <div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_emergency_contact_relations'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_dob'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control datepicker"  id="datepicker" type="text" name="'.$form_field->name.'" required>  
<label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }

                elseif ($formfil=='form_blood_group'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html .='<div class="form-group form-float">
                                <div class="form-line"><select class="form-control" name="'.$form_field->name.'" required>';
                    foreach (json_decode('{"A+": "A+", "O+": "O+","B+":"B+","B+":"B+","AB+":"AB+","A-":"A-","O-":"O-","B-":"B-","AB-":"AB-"}', true) as $optionKey => $optionValue){
                        $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                    }
                    $html .='</select><label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif ($formfil=='form_tshirt_size'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html .='<div class="form-group form-float">
                                <div class="form-line"><select class="form-control" name="'.$form_field->name.'" required>';
                    foreach (json_decode('{"XS": "XS", "S": "S","M":"M","L":"L","XL":"XL","XLL":"XLL","XLLL":"XLLL"}', true) as $optionKey => $optionValue){
                        $html .='<option value="'.$optionKey.'">'.$optionValue.'</option>';
                    }
                    $html .='</select><label class="form-label">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_attendee_photo'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line">
        <input class="form-control"  type="file" name="'.$form_field->name.'" required>  
<label class="form-label" style="top: -16px; !important;">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
                elseif($formfil=='form_attende_id_proof'){
                    $form_field=EventDynamicForms::where('name',$formfil)->first();
                    $html.='<div class="form-group form-float">
                                <div class="form-line" name="'.$form_field->name.'" required>
        <input class="form-control"  type="file">  
 <label class="form-label" style="top: -16px; !important;">'.$form_field->display_name.'</label>
                                </div>
                            </div>';
                }
            }
        }
        $html.='<button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
   <script src="' . asset('plugins/jquery/jquery.min.js') . '"></script>
    <script src="' . asset('plugins/bootstrap/js/bootstrap.js') . '"></script>
    <script src="' . asset('plugins/jquery-validation/jquery.validate.js') . '"></script>
    <script src="' . asset("plugins/node-waves/waves.js") . '"></script>
    <script src="' . asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') . '"></script>
    <script src="' . asset('js/admin.js') . '"></script>
    <script src="' . asset('js/pages/forms/form-validation.js') . '"></script>
    <script src="' . asset('js/uikit.min.js') . '"></script>
   ';
        return $html;


    }


}

