@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            User Profile's <strong>{{ $clientprofile->name }}</strong>
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/user-profile') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>Unique ID</th>
                                        <td>{{ 1000000000+$clientprofile->id}}</td>

                                </tr>
                                <tr>
                                    <th> Profile Image</th>
                                    @if(isset($clientprofile->image))
                                        <td><img src="{{$clientprofile->image}}" height="200px" width="150px"></td>
                                    @else
                                        <td><img src="{{asset('dummy-profile-pic.jpg')}}" height="250px" width="180px"></td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $clientprofile->name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $clientprofile->email }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    @if(isset($clientprofile->mobile))
                                        <td> {{ $clientprofile->mobile }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Is Social Login ?</th>
                                   <td>{!!  $clientprofile->social_type==1?'<span class="label label-success">YES</span>':'<span class="label label-danger">NO</span>' !!}</td>
                                </tr>

                                <tr>
                                    <th> Social Login Type</th>
                                    @if($clientprofile->social_login_type==0)
                                        <td><span class="label label-success">Normal</span></td>
                                        @else
                                    <td>{!!  $clientprofile->social_login_type==1?'<span class="label label-info">FACEBOOK</span>':'<span class="label label-danger">GOOGLE</span>' !!}</td>
                               @endif
                                </tr>

                                <tr>
                                    <th> Joining Date</th>
                                    <td> {{ date('d F Y',strtotime($clientprofile->created_at)) }} </td>
                                </tr>

                                <tr>
                                    <th> Joining Time</th>
                                    <td> {{ date('H:i:A',strtotime($clientprofile->created_at)) }} </td>
                                </tr>

                                <tr>
                                    <th> Address</th>
                                    @if(isset($clientprofile->address))
                                        <td> {{ $clientprofile->address }} </td>
                                    @else
                                        <td>----------</td>
                                    @endif

                                </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
