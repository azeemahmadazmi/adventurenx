<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function socialLogin($social)
    {

        return Socialite::driver($social)->redirect();
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password'=>'required',
        ]);

        $remember_me = $request->has('remember') ? true : false;
        $user = User::where('email',$request->email)->first();
        if(!$user){
            session()->flash('failed_message','Email Id is not registered with us');
            return redirect('/login');
        }else if (!Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $remember_me)){
            session()->flash('failed_message','Incorrect Password');
            return redirect('/login');
        }else{
           // dd($user,$request);
            $previousUrl=Session::get('previous_url');
            $this->guard()->login($user);
            return redirect($previousUrl);

        }
    }

    public function handleProviderCallback($social)
    {
        $previousUrl=Session::get('previous_url');
        $userSocial = Socialite::driver($social)->user();
        if ($userSocial->getEmail() == null || $userSocial->getEmail() == '') {
            return redirect()->back()->with('error_message', 'Enter your Social login Email!');
        }
        $user=User::where('email',$userSocial->getEmail())->first();
        if( $user){
            $this->guard()->login($user);
            return redirect($previousUrl);
        }
        else{
            if($social=='facebook'){
                $scocailTypeLogin=1;
            }
            elseif($social=='google'){
                $scocailTypeLogin=2;
            }
            else{
                $scocailTypeLogin=0;
            }
            $user = User::create([
                'name' => $userSocial->getName(),
                'email' => $userSocial->getEmail(),
                'image' =>$userSocial->avatar,
                'password' => bcrypt(12345678),
                'social_type' => 1,
                'social_login_type'=>$scocailTypeLogin,
                'email_verified_at' => date('Y-m-d H:i:s')
            ]);
            $this->guard()->login($user);
            return redirect($previousUrl);

        }

    }
}
