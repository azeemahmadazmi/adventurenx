<?php

namespace App\Http\Controllers\FrontUser;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Transaction;
use App\Order;
use App\Event;
use App\Cart;
use App\EventTicket;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Banner;

class RazorpayController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('payments.razorpay');
    }

    public function paysuccess(Request $request)
    {
        $cart = Cart::where('user_id', Auth::user()->id)->first();
        $data=$request->all();
        $data['user_details']=$cart->user_details;
        $order= new Order();
        $orderId=$order->saveOrder($data);
        $orderDetails = Order::find($orderId);
        $transaction['razorpay_payment_id']=$data['razorpay_payment_id'];
        $transaction['order_id']=$orderId;
        $transaction['user_id']=Auth::user()->id;
        $transaction['status']=1;
        $trns=Transaction::create($transaction);
        DB::beginTransaction();
            try {
                    $orderDetails->payment_status = 1;
                    $orderDetails->order_status = 1;
                    $orderDetails->transaction_id = $trns->id;
                    $orderDetails->save();
                    $cart->cart = null;
                    $cart->user_details = null;
                    $cart->save();
                    DB::commit();
                    Session::put('totalAmount',$orderDetails->grand_total);
                    Session::put('final_orderID',$orderDetails->id);
                    $arr = array('msg' => 'Payment successfully credited', 'status' => true);
                    return Response()->json($arr);


            } catch (\Exception $e) {
                DB::rollback();
                $arr = array('msg' => 'Payment unsuccessfully credited', 'status' => false);
                return Response()->json($arr);
            }


    }

    public function thankYou()
    {
        $banner = Banner::where('name', '=', 'thankyou')->latest()->first();
        return view('frontuser.cart.thankyou', compact('banner'));
    }

    public function reciept(Request $request)
    {

        $orderId=base64_decode($request['order_id']);
        $order=Order::find($orderId);
        $cartDetails=unserialize($order->cart);
        $event = Event::find($cartDetails['event_id']);
        $totalQuantity=$cartDetails['quantity'];
        $eventTicket = EventTicket::where('event_id', $event->id)->first();
        $users=Auth::user();
        return view('frontuser.cart.reciept', compact('order','event','totalQuantity','eventTicket','users'));
    }

    public function createorder(){
        $cart = Cart::where('user_id', Auth::user()->id)->first();
        if ($cart) {
            $cartData = unserialize($cart->cart);
            $quantity = $cartData['quantity'];
            $event_id = $cartData['event_id'];
            $event = Event::find($event_id);


            $qun=unserialize($cart->user_details);
            $tax=0;
            $servicetax=0;
            $price=0;
            foreach ($qun as $qk => $qv){
                $eventTicket = EventTicket::findOrFail($qv['event_ticket_id']);

                if ($eventTicket->adventurenx_fees == 1) {
                    $tax += 0;
                } else {
                    $tax += 2 * ($eventTicket->price * 1) / 100;
                }

                if ($eventTicket->gateway_fees == 1) {
                    $servicetax += 0;
                } else {
                    $servicetax += 2 * ($eventTicket->price * 1) / 100;
                }
                $price +=$eventTicket->price;

            }

            $discount = 0;
            $toalAmount=$price;
            $grandtotalAmount=($price + $tax + $servicetax) - $discount;
                $arr = array('msg' => 'Order store successfully', 'status' => true,'total_amount' =>$toalAmount,'quantity' =>$quantity,'tax' => $tax, 'grand_total_amount' =>$grandtotalAmount,'service_charge'=>$servicetax,'discount' =>$discount,'cart' => $cart->cart,'event_id' =>$event_id,'user_id' => Auth::user()->id);
                return Response()->json($arr);
        } else {
            $arr = array('msg' => 'Order store Unsuccessful', 'status' => false);
            return Response()->json($arr);
        }
    }
}
