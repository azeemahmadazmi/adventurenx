@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4 auth-left">
            <div class="row auth-form mb-4">
                <div class="col-12 col-sm-12">
                    <div class="auth-text-top mb-4 text-center">
                        <a href="{{url('/')}}"><img src="{{asset('images/loginregister.png')}}" width="250 px"
                                                    height="140px"></a><br>
                        <small>Please Reset your password</small>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <div class="input-icon">
                                <i class="mdi mdi-email"></i>
                                <input type="email" class="form-control" id="email" name="email"
                                       value="{{ $email ?? old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label for="password">Password</label>
                            <div class="input-icon">
                                <i class="mdi mdi-lock"></i>
                                <input type="password" class="form-control" id="password" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label for="password_confirmation">Confirm Password</label>
                            <div class="input-icon">
                                <i class="mdi mdi-lock"></i>
                                <input type="password" class="form-control" id="password_confirmation"
                                       name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-c mt-4 mb-4"> {{ __('Reset Password') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8 auth-right d-lg-flex d-none bg-gradient" id="particles">
            <div class="logo">
                <img src="{{asset('assets/img/logo.png')}}" width="100" alt="logo">
            </div>
            <div class="heading">
                <h3>Welcome to AdventureNX</h3>
            </div>
            <div class="shape"></div>
        </div>
    </div>
    </div>
@endsection
