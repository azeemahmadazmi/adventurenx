<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventCreatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_creates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('event_name')->nullable();
            $table->string('event_display_name')->nullable();
            $table->string('event_visibility');
            $table->string('event_start_date')->nullable();
            $table->string('event_start_time')->nullable();
            $table->string('event_end_date')->nullable();
            $table->string('event_end_time')->nullable();
            $table->string('event_venue')->nullable();
            $table->text('event_descriptions')->nullable();
            $table->string('image')->nullable();
            $table->string('status');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_creates');
    }
}
