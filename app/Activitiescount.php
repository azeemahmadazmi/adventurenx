<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Activitiescount extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */

       protected $table = 'activitiescounts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'total', 'status'];

    
}
