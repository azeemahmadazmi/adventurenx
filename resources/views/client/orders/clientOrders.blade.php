@extends('client.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                    </div>
                    <br>
                    <a href="#" class="btn btn-success btn-sm waves-effect"
                       title="Add New Coupon" style="margin-left: 22px;">
                        <i class="material-icons">shopping_cart</i>  Order's History
                    </a>
                    {!! Form::open(['method' => 'GET', 'url' => '/client/client-events/orders', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               style="border: ridge">
                        <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                    </div>
                    {!! Form::close() !!}
                    <div class="body">
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Order-ID</th>
                                    <th>Event Name</th>
                                    <th>User's Name</th>
                                    <th>User's email</th>
                                    <th>Order Status</th>
                                    <th>Payment Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->event->event_name }}</td>
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->user->email }}</td>
                                        <td>{!! $item->order_status==1?'<span class="label label-success">Completed</span>':'<span class="label label-danger">In-Complete</span>'  !!} </td>
                                        <td>{!! $item->payment_status==1?'<span class="label label-success">Completed</span>':'<span class="label label-danger">Failed</span>'  !!} </td>
                                        <td class="text-center">
                                            <a href="{{ url('/client/client-events/orders/' . base64_encode($item->id)) }}" title="View Coupon">
                                                <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                    View
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $orders->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
