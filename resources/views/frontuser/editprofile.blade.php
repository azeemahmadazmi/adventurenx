@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative account-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Account Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">account</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">edit profile</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
<style>
    * {
        box-sizing: border-box;
    }

    input[type=text], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }

    label {
        padding: 12px 12px 12px 0;
        display: inline-block;
    }

    input[type=submit] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        float: right;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }

    .container {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
    }

    .col-25 {
        float: left;
        width: 25%;
        margin-top: 6px;
    }

    .col-75 {
        float: left;
        width: 75%;
        margin-top: 6px;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
        .col-25, .col-75, input[type=submit] {
            width: 100%;
            margin-top: 0;
        }
    }
</style>
@section('content')
    <div class="account section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <aside class="uk-width-medium-3-10">
                    <h4 class="h-4">my account</h4>
                    <div class="title-line-lg"></div>
                    <ul class="aside-navigation">
                        <li class="active"><a href="{{url('/myaccount')}}">Account Information<i class="uk-icon-angle-right icon-right"></i></a></li>
                        <li><a href="{{url('/orderhistories')}}">My Orders<i class="uk-icon-angle-right icon-right"></i></a></li>
                        {{--<li><a href="{{url('/cart')}}">My Cart<i class="uk-icon-angle-right icon-right"></i></a></li>--}}
                    </ul>
                    <div class="uk-grid uk-grid-small uk-grid-width-large-1-2" data-uk-grid-margin>
                        <div>
                            <a href="{{url('/myaccount')}}"><button class="uk-button btn-custom">My Account</button></a>
                        </div>
                        <div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="uk-button btn-custom-black" type="submit" style="background: red !important;">sign out</button>
                            </form>

                        </div>
                    </div>
                </aside>
                <div class="uk-width-medium-7-10">
                    <p class="text-sixteen border-b2 account-welcome">
                        Welcome <span class="post-author">{{$user->name}}</span>. This is your account. Click on the sidebar sections to manage your personal information, check the status of an order or download event's order receipt.
                    </p>
                    <ul class="account-info">
                        <form action="{{url('/saveuserprofile')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-25">
                                    <label for="name">User Name:</label>
                                </div>
                                <div class="col-75">
                                    <input type="text" id="name" name="name" value="{{$user->name?$user->name:old('name')}}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="email">Email:</label>
                                </div>
                                <div class="col-75">
                                    <input type="text" id="email" name="email" value="{{$user->email}}" required disabled="disabled">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="mobile">Mobile:</label>
                                </div>
                                <div class="col-75">
                                    <div class="col-75">
                                        <input type="text" id="mobile" name="mobile" value="{{$user->mobile?$user->mobile:old('mobile')}}" required>
                                        @if ($errors->has('mobile'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="address">Addresss:</label>
                                </div>
                                <div class="col-75">
                                    <textarea id="address" name="address" style="height:100px" required>{{$user->address?$user->address:old('address')}} </textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div><br>
                            <div class="row">
                                <input type="submit" value="Save">
                            </div>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
