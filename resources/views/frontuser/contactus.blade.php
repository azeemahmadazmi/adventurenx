@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative contact-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Single Post Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">contact</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">contact</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="contact-wellness section-padding-bottom" style="padding-top:5px !important;">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    {{--<div class="uk-cover-background uk-position-relative gmap-wrapper">--}}
                        {{--<img class="" src="https://www.lonelyplanet.com/maps/asia/india/mumbai/map_of_mumbai.jpg" alt="Location Map" width="600px">--}}
                        {{--<div class="uk-position-cover" id="gmap_canvas_contact"></div>--}}
                    {{--</div>--}}
                    <div class="uk-grid contact-scopes-wellness" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <h5 class="h-5">our contacts</h5>
                            <div class="title-line-lg"></div>
                            <ul class="contact-info">
                                <li><img class="icon-left" src="images/images_wellness/location-icon.png" alt="Location Icon">Mumbai,Maharashtra-India</li>
                                <li><img class="icon-left" src="images/images_wellness/phone-icon.png" alt="Phone Icon">+91-9969695882</li>
                                <li><img class="icon-left" src="images/images_wellness/msg-icon.png" alt="Email Icon"><a href="http://www.3jon.com/cdn-cgi/l/email-protection" class="" data-cfemail="6d0c19000c3208000c04012d08000c0401430e0200">adventurenx@gmail.com</a></li>
                            </ul>
                            <ul class="uk-grid">
                                <li><a href="#"><img src="images/images_wellness/facebook-icon.png" alt="Facebook Icon"></a></li>
                                <li><a href="#"><img src="images/images_wellness/twitter-icon.png" alt="Twitter Icon"></a></li>
                                <li><a href="#"><img src="images/images_wellness/instagram-icon.png" alt="Instagram Icon"></a></li>
                                <li><a href="#"><img src="images/images_wellness/vimo-icon.png" alt="Vimo Icon"></a></li>
                            </ul>
                        </div>
                        <!-- contact info end -->
                        <div class="uk-width-medium-1-2">
                            <section class="write-comment-form">
                                <h5 class="uk-text-uppercase h-5">send us message</h5>
                                <div class="title-line-lg"></div>
                                <h4 id="formsubmitmessage"></h4>
                                <form class="uk-form form-custom" method="post" id="contact_us_form">
                                    <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                                        <div class="uk-width-1-2">
                                            <input type="text" placeholder="Name" name="name" class="uk-width-1-1 input-custom" id="contactname" required>
                                        </div>
                                        <div class="uk-width-1-2">
                                            <input type="email" placeholder="Email" name="email" class="uk-width-1-1 input-custom" id="contact_email" required>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <textarea placeholder="Message" name="message" class="uk-width-1-1 input-custom" id="contact_message" required></textarea>
                                        </div>
                                        <div class="uk-width-1-1 form-btn-grid">
                                            <input class="uk-button btn-custom-black" type="submit" value="send">
                                        </div>
                                    </div>
                                </form>
                            </section>
                            <!-- / write comment form end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ===========================================================================
        SUBSCRIPTION
        ================================================================================-->
    <section class="subscription-wellness light-sky-blue">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-5-10 subscription-header-wellness">
                    <h3 class="h-3">subscribe to discounts</h3>
                    <div class="title-line-lg"></div>
                </div>
                <form class="uk-width-large-4-10 uk-form form-custom" method="post" id="subscriber_form">
                    <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                        <div class="uk-width-3-5">
                            <input type="email" placeholder="E-mail" name="email" class="input-custom uk-width-1-1" required>
                        </div>
                        <div class="uk-width-small-2-5">
                            <input class="uk-button btn-custom-black uk-width-1-1" type="submit" value="Subscribe">
                        </div>
                    </div>
                    <h5 id="formsubmitmessagesubscriber"></h5>
                </form>

            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $('#contact_us_form').submit(function (event) {
            $('#formsubmitmessage').html('');
            event.preventDefault();
            var formData=$(this).serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/contactusform",
                type: 'POST',
                data: formData,

                beforeSend: function () {

                },
                success: function (response) {
                    if (response==1) {
                        $('#formsubmitmessage').html('Contact us form submitted successfully');
                        $('#formsubmitmessage').css('color','green');
                        $("#contact_us_form").trigger("reset");
                    }
                    else if(response==2){
                        $('#formsubmitmessage').html('Today you have already submitted ! please try tomorrow.');
                        $('#formsubmitmessage').css('color','red');
                        $("#contact_us_form").trigger("reset");
                    }
                    else{
                        $('#formsubmitmessage').html('Contact us form submitted un-successful');
                        $('#formsubmitmessage').css('color','red');
                    }
                }
            });
        });



        $('#subscriber_form').submit(function (event) {
            $('#formsubmitmessagesubscriber').html('');
            event.preventDefault();
            var formData=$(this).serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/subscriberform",
                type: 'POST',
                data: formData,

                beforeSend: function () {

                },
                success: function (response) {
                    if (response==1) {
                        $('#formsubmitmessagesubscriber').html('Your Subscription submitted successfully');
                        $('#formsubmitmessagesubscriber').css('color','green');
                        $("#subscriber_form").trigger("reset");
                    }
                    else if(response==2){
                        $('#formsubmitmessagesubscriber').html('You have already subscribed');
                        $('#formsubmitmessagesubscriber').css('color','red');
                        $("#subscriber_form").trigger("reset");
                    }
                    else{
                        $('#formsubmitmessagesubscriber').html('Your Subscription submitted un-successful');
                        $('#formsubmitmessagesubscriber').css('color','red');
                    }
                }
            });
        });
    </script>
    @endsection