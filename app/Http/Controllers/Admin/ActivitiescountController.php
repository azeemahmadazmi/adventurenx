<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Activitiescount;
use Illuminate\Http\Request;
class ActivitiescountController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $activitiescount = Activitiescount::where('name', 'LIKE', "%$keyword%")
                ->orWhere('total', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $activitiescount = Activitiescount::latest()->paginate($perPage);
        }

        return view('admin.activitiescount.index', compact('activitiescount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.activitiescount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'total' => 'required'
		]);
        $requestData = $request->all();
        
        Activitiescount::create($requestData);

        return redirect('admin/activitiescount')->with('flash_message', 'Activitiescount added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $activitiescount = Activitiescount::findOrFail($id);

        return view('admin.activitiescount.show', compact('activitiescount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $activitiescount = Activitiescount::findOrFail($id);

        return view('admin.activitiescount.edit', compact('activitiescount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'total' => 'required'
		]);
        $requestData = $request->all();
        
        $activitiescount = Activitiescount::findOrFail($id);
        $activitiescount->update($requestData);

        return redirect('admin/activitiescount')->with('flash_message', 'Activitiescount updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Activitiescount::destroy($id);

        return redirect('admin/activitiescount')->with('flash_message', 'Activitiescount deleted!');
    }
}
