@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative classes-breadcrumbs-wellness">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Classes Header Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">events</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">events</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="popular-events-wellness section-padding-top">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="h-2">popular events</h2>
                    <div class="title-line-lg"></div>
                    <div class="events-thumbnail border-b2">
                        <div class="uk-grid uk-grid-width-medium-1-3" data-uk-grid-margin>
                            @if(isset($events) && $events->isNotEmpty())
                                 @php $count=0;@endphp
                                @foreach($events as $ek => $ev)
                                    @php $count++; @endphp
                                    <div class="thumbnails-wrapper">
                                        <figure>
                                            <div class="overflow-hidden uk-animation-hover thumb-img-wrapper">
                                                <a href="{{url('/eventdescriptions?eventid='.base64_encode($ev->id))}}">
                                                    <img class="uk-animation-scale uk-animation-reverse" src="{{url('Events/'.$ev->image)}}" alt="Event Image" width="364px" height="250px">
                                                </a>
                                            </div>
                                            <figcaption>
                                                <div class="figcaption-wrapper uk-grid uk-grid-small" data-uk-grid-margin>
                                                    <div class="uk-width-large-1-3 uk-text-left">
                                                        <button class="uk-button btn-custom" type="button">{{date('d/m/Y',strtotime($ev->event_start_date))}}</button>
                                                    </div>
                                                    <div class="uk-width-large-2-3 events-detail uk-text-left">
                                                        <h5 class="h-5">{{$ev->event_name}}</h5>
                                                        <div class="title-line-lg"></div>
                                                        <p>{{isset($ev->eventAddress->event_venue)?ucwords($ev->eventAddress->event_venue):''}} | at {{ date('h:i:A',strtotime($ev->event_start_time))}}</p>
                                                        <p>Event Price : ₹{{isset($ev->eventTicket->price)?$ev->eventTicket->price:0}}</p>
                                                    </div>
                                                </div>
                                                @if($count%3==0)
                                                <br><br><br>
                                                    @endif
                                            </figcaption>
                                        </figure>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <style>
                            .active * {
                                color: white !important;
                                font-size: 15px !important;
                            }
                            .pagination > li > span {
                                padding: 6px 22px !important;
                            }
                        </style>
                        <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
                        <div class="pagination-wrapper pull-right"> {!! $events->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
            <!-- / custom-thumbnail -->
        </div>
    </section>
    <!-- / EVENTS end -->
    <!-- ===========================================================================
        EVENTS DESCRIPTION
        ================================================================================-->
    {{--<section class="events-description-wellness section-padding-top section-padding-bottom">--}}
        {{--<div class="uk-container uk-container-center">--}}
            {{--<div class="uk-grid">--}}
                {{--<div class="uk-width-1-1">--}}
                    {{--<h3 class="h-3">new rafting white water "route bextreme_3" / class 2-3</h3>--}}
                    {{--<div class="title-line-lg"></div>--}}
                    {{--<div class="uk-grid">--}}
                        {{--<div class="uk-width-medium-7-10 events-description-body-wellness text-sixteen">--}}
                            {{--<img class="events-cover-image-wellness" src="images/images_wellness/events_description.jpg" alt="Events Description">--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam reprehenderit, tenetur libero quae maiores recusandae. Deserunt excepturi nemo aliquam repudiandae nulla explicabo assumenda minus earum animi. Impedit id, maiores fuga. Deserunt excepturi nemo aliquam repudiandae nulla explicabo assumenda minus earum animi. Impedit id, maiores fuga.</p>--}}
                            {{--<div class="events-description-slideset" data-uk-slideset="{small: 2, medium: 2, large: 2}">--}}
                                {{--<div class="uk-slidenav-position">--}}
                                    {{--<ul class="uk-grid uk-slideset">--}}
                                        {{--<li><img src="images/images_wellness/events_slide_01.jpg" alt="Events"></li>--}}
                                        {{--<li><img src="images/images_wellness/events_slide_02.jpg" alt="Events"></li>--}}
                                        {{--<li><img src="images/images_wellness/events_slide_01.jpg" alt="Events"></li>--}}
                                        {{--<li><img src="images/images_wellness/events_slide_02.jpg" alt="Events"></li>--}}
                                    {{--</ul>--}}
                                    {{--<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>--}}
                                    {{--<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut unde consequatur nisi! Enim iste, animi impedit exercitationem asperiores libero sapiente laboriosam, quos suscipit, omnis laudantium, deserunt nobis eum nulla consectetur.</p>--}}
                            {{--<div class="events-description-schedule">--}}
                                {{--<div class="uk-grid">--}}
                                    {{--<div class="uk-width-medium-1-2">--}}
                                        {{--<ul class="contact-info">--}}
                                            {{--<li>--}}
                                                {{--<img class="icon-left" src="images/images_sport/location-icon.png" alt="Location Icon">--}}
                                                {{--7508 Pine Street Reisterstown, MD 21136--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<img class="icon-left" src="images/images_sport/phone-icon.png" alt="Phone Icon">--}}
                                                {{--(593) 119-7864--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                    {{--<div class="uk-width-medium-1-2">--}}
                                        {{--<ul class="contact-info">--}}
                                            {{--<li>--}}
                                                {{--<img class="icon-left" src="images/images_sport/Schedule-icon.png" alt="Schedule Icon">--}}
                                                {{--04/ 11 /16,  Mon - Fri, 6pm - 9pm--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<span class="post-subtitle-red">Instructor: </span>--}}
                                                {{--<a class="post-author">Richard Daugherty</a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<aside class="uk-width-medium-3-10 events-sidebar-wellness">--}}
                            {{--<h4 class="h-4">recent events</h4>--}}
                            {{--<div class="title-line-lg"></div>--}}
                            {{--<ul class="aside-navigation">--}}
                                {{--<li>--}}
                                    {{--<div class="uk-grid uk-grid-small" data-uk-grid-margin>--}}
                                        {{--<div class="uk-width-large-1-3">--}}
                                            {{--<button class="uk-button btn-custom" type="button">04/11/16</button>--}}
                                        {{--</div>--}}
                                        {{--<div class="uk-width-large-2-3 events-detail">--}}
                                            {{--<h5 class="h-5">CrossFit Camp Challenge / part 1</h5>--}}
                                            {{--<div class="title-line-sm"></div>--}}
                                            {{--<p>Jeff Keatinge trainer | at 10 pm</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="uk-grid uk-grid-small" data-uk-grid-margin>--}}
                                        {{--<div class="uk-width-large-1-3">--}}
                                            {{--<button class="uk-button btn-custom" type="button">04/23/16</button>--}}
                                        {{--</div>--}}
                                        {{--<div class="uk-width-large-2-3 events-detail">--}}
                                            {{--<h5 class="h-5">CrossFit Camp Challenge / part 2</h5>--}}
                                            {{--<div class="title-line-sm"></div>--}}
                                            {{--<p>Jeff Keatinge trainer | at 10 pm</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="uk-grid uk-grid-small" data-uk-grid-margin>--}}
                                        {{--<div class="uk-width-large-1-3">--}}
                                            {{--<button class="uk-button btn-custom" type="button">05/02/16</button>--}}
                                        {{--</div>--}}
                                        {{--<div class="uk-width-large-2-3 events-detail">--}}
                                            {{--<h5 class="h-5">running marathon</h5>--}}
                                            {{--<div class="title-line-sm"></div>--}}
                                            {{--<p>For all comers | start at 8 pm</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                            {{--<div class="events-calendar">--}}
                                {{--<h4 class="h-4">events calendar</h4>--}}
                                {{--<div class="title-line-lg"></div>--}}
                                {{--<div class="date-picker-custom"></div>--}}
                            {{--</div>--}}
                        {{--</aside>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- / container -->--}}
    {{--</section>--}}
@endsection