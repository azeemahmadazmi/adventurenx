<?php

namespace App\Http\Controllers\FrontUser;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\HomePageBanner;
use App\Event;
use App\Banner;
use App\EventAddress;
use App\EventTicket;
class EventController extends Controller
{

  public function event(Request $request){
      $allevents=Event::where('status',1)->get();
      $eventId=base64_decode($request['eventid']);
      $event=Event::find($eventId);
      $eventAddress=EventAddress::where('event_id',$eventId)->first();
      $eventSchedule=date('d/m/Y',strtotime($event->event_start_date)).' to '.date('d/m/Y',strtotime($event->event_end_date));
      $eventTimeSchedule=date('h:i:A',strtotime($event->event_start_time)).' to '.date('h:i:A',strtotime($event->event_end_time));
      if($eventAddress){
          $eveAddresss=$eventAddress->event_venue.' '.$eventAddress->event_street_line_1.' '.$eventAddress->event_street_line_2.' '.$eventAddress->city;
      }
      else{
          $eveAddresss='';
      }

      $eventTicket=EventTicket::where('event_id',$eventId)->first();
      if($eventTicket){
          $eventFess=$eventTicket->price;
      }
      else{
          $eventFess='';
      }
      $banner=Banner::where('name','=','eventpage')->latest()->first();
     return view('frontuser.events.event',compact('event','allevents','banner','eventSchedule','eventTimeSchedule','eveAddresss','eventFess'));

  }



}
