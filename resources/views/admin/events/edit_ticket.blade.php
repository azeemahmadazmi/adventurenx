<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | AdventureNX</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet"/>

    <!-- Animation Css -->
    <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet"/>

    <!-- Sweet Alert Css -->
    <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet"/>

    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <!-- Morris Chart Css-->
    <link href="{{asset('plugins/morrisjs/morris.css')}}" rel="stylesheet"/>
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"
          rel="stylesheet"/>

    <!-- Bootstrap DatePicker Css -->
    <link href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('css/themes/all-themes.css')}}" rel="stylesheet"/>

</head>

<body class="theme-red">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">

    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
        position: initial !important;
        opacity:1 !important;
        transform : scale(1.4);
    }
</style>
<!-- #END# Search Bar -->
<!-- Top Bar -->
@include('admin.layouts.header')
<section>
    @include('admin.layouts.sidebar')
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Ticket Name: <span style="color: red;"> {{$eventTicket->event_ticket_name}} </span>  <span class="pull-right">Sale start Date : <span style="color:red">{{$eventTicket->ticket_sale_start_date}}</span></span></h2>
                    </div>
                    <div class="body">
                        <form id="wizard_with_validation" method="POST" action="{{url('/admin/event/saveeditTicket/'.base64_encode($eventTicket->id))}}" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf
                            <h3>Edit Ticket for: <span>{{$eventTicket->event_ticket_name}}</span></h3>
                            <fieldset>
                                <label for="event_ticket_name" style="font-size:14px;">{{ 'Event Ticket Name' }}</label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="event_ticket_name" value="{{$eventTicket->event_ticket_name}}" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="price"
                                               style="font-size:14px;">{{ 'Price (in Rs)' }}</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" name="price" class="form-control" value="{{$eventTicket->price}}"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="quantity" style="font-size:14px;">{{ 'Quantity' }}</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" name="quantity" class="form-control" value="{{$eventTicket->quantity}}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="minimum_per_booking"
                                               style="font-size:14px;">{{ 'Minimum per Booking' }}</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" name="minimum_per_booking" class="form-control" value="{{$eventTicket->minimum_per_booking}}"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="maximum_per_booking"
                                               style="font-size:14px;">{{ 'Maximum Per Booking' }}</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" name="maximum_per_booking" class="form-control" value="{{$eventTicket->maximum_per_booking}}"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--<label for="type" style="font-size:14px;">{{ 'Event Type' }}</label>--}}

                                {{--<div class="form-group form-float">--}}
                                    {{--<input name="type" type="radio" id="radio_33"--}}
                                           {{--class="with-gap radio-col-teal" value="1" @if($eventTicket->type==1) checked @endif>--}}
                                    {{--<label for="radio_33">Normal</label>--}}
                                    {{--<input name="type" type="radio" id="radio_34"--}}
                                           {{--class="with-gap radio-col-teal" value="0" @if($eventTicket->type==0) checked @endif>--}}
                                    {{--<label for="radio_34">Donation</label>--}}
                                {{--</div>--}}

                                <label for="adventurenx_fees"
                                       style="font-size:14px;">{{ "Adventurenx Fees paid by" }}</label>

                                <div class="form-group form-float">
                                    <input name="adventurenx_fees" type="radio" id="radio_37"
                                           class="with-gap radio-col-teal" value="1" @if($eventTicket->adventurenx_fees==1) checked @endif>
                                    <label for="radio_37">Me</label>
                                    <input name="adventurenx_fees" type="radio" id="radio_38"
                                           class="with-gap radio-col-teal" value="0" @if($eventTicket->adventurenx_fees==0) checked @endif>
                                    <label for="radio_38">Attendee</label>
                                </div>

                                <label for="gateway_fees" style="font-size:14px;">{{ "Gateway Fees paid by" }}</label>

                                <div class="form-group form-float">
                                    <input name="gateway_fees" type="radio" id="radio_35"
                                           class="with-gap radio-col-teal" value="1" @if($eventTicket->gateway_fees==1) checked @endif>
                                    <label for="radio_35">Me</label>
                                    <input name="gateway_fees" type="radio" id="radio_36"
                                           class="with-gap radio-col-teal" value="0" @if($eventTicket->gateway_fees==0) checked @endif>
                                    <label for="radio_36">Attendee</label>
                                </div>


                                <div class="row clearfix"></div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ticket_sale_start_date"
                                               style="font-size:14px;">{{ 'Ticket Sale Start Date' }}</label>

                                        <div class="form-line">
                                            <input type="text" class="form-control datepicker"
                                                   name="ticket_sale_start_date"
                                                   id="ticket_sale_start_date" required value="{{$eventTicket->ticket_sale_start_date}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ticket_sale_start_time"
                                               style="font-size:14px;">{{ 'Ticket Sale  Start Time' }}</label>

                                        <div class="form-line">
                                            <input type="text" class="form-control timepicker"
                                                   name="ticket_sale_start_time"
                                                   id="ticket_sale_start_time" required value="{{$eventTicket->ticket_sale_start_time}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ticket_sale_end_date"
                                               style="font-size:14px;">{{ 'Ticket Sale End Date' }}</label>

                                        <div class="form-line">
                                            <input type="text" class="form-control datepicker"
                                                   name="ticket_sale_end_date"
                                                   id="ticket_sale_end_date" required value="{{$eventTicket->ticket_sale_end_date}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ticket_sale_end_time"
                                               style="font-size:14px;">{{ 'Ticket Sale End Time' }}</label>

                                        <div class="form-line">
                                            <input type="text" class="form-control timepicker"
                                                   name="ticket_sale_end_time"
                                                   id="ticket_sale_end_time" required value="{{$eventTicket->ticket_sale_end_time}}">
                                        </div>
                                    </div>
                                </div>
                                <label for="ticket_description"
                                       style="font-size:14px;">{{ 'Ticket Description' }}</label>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea class="form-control ckeditor" name="ticket_description"
                                                  id="ticket_description">{{$eventTicket->ticket_description}}</textarea>
                                    </div>
                                </div>

                                <label for="message_to_attendee"
                                       style="font-size:14px;">{{ 'Message To Attendee' }}</label>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea class="form-control ckeditor" name="message_to_attendee"
                                                  id="message_to_attendee">{{$eventTicket->message_to_attendee}}</textarea>
                                    </div>
                                </div>
                                <label for="message_to_attendee"
                                       style="font-size:14px;">{{ 'Term and Conditions' }}</label>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea class="form-control ckeditor" name="term_and_conditions"
                                                  id="term_and_conditions" >{{$eventTicket->term_and_conditions}}</textarea>
                                    </div>
                                </div>

                            </fieldset>

                            <h3>Event Form</h3>

                            <fieldset>
                                <div class="col-sm-8 dynamic_forms_html">
                                    <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input class="form-control"  type="text" id="name">
                                        </div>
                                    </div>
                                    <label for="email" style="font-size:18px;">{{ 'Email' }}</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input class="form-control" type="email" id="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>Field Name</th>
                                            </tr>
                                            </thead>
                                            @foreach(App\EventDynamicForms::get() as $key => $value)
                                                <tr>
                                                    <td class=""><input type="checkbox" @if(in_array($value->name,$formfield)) checked @endif data-dispaly_name="{{$value->display_name}}" name="{{$value->name}}" value="{{$value->name}}" class="filled-in dynamicform_fields"></td>
                                                    <td>{{$value->display_name}}</td>
                                                </tr>
                                            @endforeach
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Form Example With Validation -->
    </div>
</section>

<!-- Jquery Core Js -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Select Plugin Js -->
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Jquery Validation Plugin Css -->
<script src="{{asset('plugins/jquery-validation/jquery.validate.js')}}"></script>

<!-- JQuery Steps Plugin Js -->
<script src="{{asset('plugins/jquery-steps/jquery.steps.js')}}"></script>

<!-- Sweet Alert Plugin Js -->
<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('plugins/node-waves/waves.js')}}"></script>
<script src="{{asset('plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('plugins/morrisjs/morris.js')}}"></script>

<!-- ChartJs -->
<script src="{{asset('plugins/chartjs/Chart.bundle.js')}}"></script>
<script src="{{asset('plugins/autosize/autosize.js')}}"></script>

<script src="{{asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
<script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<!-- Custom Js -->
<script src="{{asset('js/admin.js')}}"></script>
<script src="{{asset('js/pages/forms/form-wizard.js')}}"></script>
<script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>
<script src="{{asset('js/demo.js')}}"></script>
<script src="{{asset('plugins/tinymce/tinymce.js')}}"></script>
<script src="{{asset('js/pages/forms/editors.js')}}"></script>
<!-- Demo Js -->
<script src="{{asset('js/demo.js')}}"></script>


<script>
    $('#event_name').keypress(function () {
        //   alert(123);

    });
    $( document ).ready(function() {
        $("input[type=checkbox]:checked").each(function(){
            var name = $(this).attr( "name" );
            var dispaly_name =$(this).data( "dispaly_name" );
            if($(this).is(':checked')) {
                $.ajax({
                    url: '/admin/add_dynamic_form',
                    type: 'GET',
                    data: {name: name, dispaly_name: dispaly_name},
                    success: function (data) {
                        $('.dynamic_forms_html').append(data);
                        $('.datepicker').bootstrapMaterialDatePicker({
                            format: 'dddd DD MMMM YYYY',
                            clearButton: true,
                            weekStart: 1,
                            time: false
                        });
                    }
                });
            }
        });

        $('.dynamicform_fields').click(function () {
            var name = $(this).attr( "name" );
            var dispaly_name =$(this).data( "dispaly_name" );
            if($(this).is(':checked')){
                $.ajax({
                    url: '/admin/add_dynamic_form',
                    type: 'GET',
                    data:{name:name,dispaly_name:dispaly_name},
                    success: function (data) {
                        $('.dynamic_forms_html').append(data);
                        $('.datepicker').bootstrapMaterialDatePicker({
                            format: 'dddd DD MMMM YYYY',
                            clearButton: true,
                            weekStart: 1,
                            time: false
                        });
                    }
                });
            }else{
                $.ajax({
                    url: '/admin/delete_dynamic_form',
                    type: 'GET',
                    data:{name:name},
                    success: function (data) {
                        $('.'+data).remove();
                    }
                });

            }
        });
    });
</script>


</body>
</html>
