@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            BlogComment {{ $blogcomment->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/blog-comments') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/blog-comments/' . $blogcomment->id . '/edit') }}"
                           title="Edit BlogComment">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>
                        <form method="POST" action="{{ url('admin/blogcomments' . '/' . $blogcomment->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete BlogComment"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $blogcomment->id }}</td>
                                </tr>
                                <tr>
                                    <th> User Name</th>
                                    <td> {{ $blogcomment->user_name }} </td>
                                </tr>
                                <tr>
                                    <th> User Email</th>
                                    <td> {{ $blogcomment->user_email }} </td>
                                </tr>
                                <tr>
                                    <th> Blog Name</th>
                                    <td>{{ isset($blogcomment->blog->name)?$blogcomment->blog->name:'' }}</td>
                                </tr>
                                <tr>
                                    <th> Message</th>
                                    <td  width="700px"> {!! $blogcomment->message !!} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $blogcomment->status==1?'Active':'Disabled' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
