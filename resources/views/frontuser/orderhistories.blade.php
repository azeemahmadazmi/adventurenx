@extends('frontuser.layouts.master')
@section('banner')
    <section class="header-breadcrumbs">
        <div class="uk-cover-background uk-position-relative account-breadcrumbs">
            <img class="uk-invisible" src="{{asset('bannerImage/'.$banner->image)}}" alt="Account Breadcrumbs">
            <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle breadcrumbs-wrapper">
                <div>
                    <ul class="uk-breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="uk-active"><a href="#">account</a></li>
                    </ul>
                    <div class="title-line-lg"></div>
                    <h1 class="h-1">my orders</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href=" https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        @media screen and (max-width: 768px){
            .card {
                width: 580px;
                margin-left: -60px;
            }
        }
        @media screen and (max-width: 414px){
            .card {
                width: 400px;
            }
        }
        @media screen and (max-width: 375px){
            .card {
                width: 350px;
            }
        }
        @media screen and (max-width: 360px){
            .card {
                width: 340px;
            }
        }
        @media screen and (max-width: 320px){
            .card {
                width: 260px;
            }
        }

    </style>
@endsection
@section('content')
    <div class="account section-padding-top section-padding-bottom">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin>
                <aside class="uk-width-medium-2-10">
                    <h4 class="h-4">my account</h4>
                    <div class="title-line-lg"></div>
                    <ul class="aside-navigation">
                        <li ><a href="{{url('/myaccount')}}">Account Information<i class="uk-icon-angle-right icon-right"></i></a></li>
                        <li class="active"><a href="{{url('/orderhistories')}}">My Orders<i class="uk-icon-angle-right icon-right"></i></a></li>
                        {{--<li><a href="{{url('/cart')}}">My Cart<i class="uk-icon-angle-right icon-right"></i></a></li>--}}
                    </ul>
                    <div class="uk-grid uk-grid-small uk-grid-width-large-1-2" data-uk-grid-margin>
                        <div>
                            <a href="{{url('/myaccount')}}"><button class="uk-button btn-custom">My Account</button></a>
                        </div>
                        <div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="uk-button btn-custom-black" type="submit" style="background: red !important;">sign out</button>
                            </form>

                        </div>
                    </div>
                </aside>
                <div class="uk-width-medium-8-10">
                    <p class="text-sixteen border-b2 account-welcome">
                        Welcome <span class="post-author">{{$user->name}}</span>. This is your account. Click on the sidebar sections to manage your personal information, check the status of an order or download event's order receipt.
                    </p>
                    <ul class="account-info">
                        @if(isset($orders) && $orders->isNotEmpty())

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header">
                                                        <h2>
                                                            Order History
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" style="width:100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Sr.No</th>
                                                                <th>Order-ID</th>
                                                                <th>Total Amount</th>
                                                                <th>Order Status</th>
                                                                <th>Payment Status</th>
                                                                <th>Booking Date</th>
                                                                <th>Order Details</th>
                                                                <th>Receipt</th>
                                                            </tr>
                                                            </thead>
                                                            {{--<tfoot>--}}
                                                            {{--<tr>--}}
                                                                {{--<th>Order-ID</th>--}}
                                                                {{--<th>Total Amount</th>--}}
                                                                {{--<th>Order Status</th>--}}
                                                                {{--<th>Payment Status</th>--}}
                                                                {{--<th>Booking Date</th>--}}
                                                                {{--<th>Order Details</th>--}}
                                                                {{--<th>Receipt</th>--}}
                                                            {{--</tr>--}}
                                                            {{--</tfoot>--}}
                                                            <tbody>
                                                            @foreach($orders as $ok => $ov)
                                                            <tr>
                                                                <td>{{++$ok}}</td>
                                                                <td>{{$ov->id}}</td>
                                                                <td><strong>₹ {{$ov->grand_total}}</strong></td>
                                                                <td>{!! $ov->order_status==0?'<span style="background:red;color:white">In-Complete</span>':'<span style="background:green;color:white">Completed</span>' !!}</td>
                                                                <td>{!! $ov->payment_status==0?'<span style="background:red;color:white">Failed</span>':'<span style="background:green;color:white">Completed</span>' !!}</td>
                                                                <td>{{date('d/m/Y',strtotime($ov->created_at))}}</td>
                                                                <td><a href="{{url('/show_orderhisories?order_id='.base64_encode($ov->id))}}" target="_blank"><button class="btn btn-xs btn-info">View</button></a></td>
                                                                <td><a href="{{url('/reciept?order_id='.base64_encode($ov->id))}}" target="_blank"><button class="btn btn-xs btn-success">Receipt</button></a></td>
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                            @else
                            <h3 style="color:red;text-transform: capitalize;">There is no previous order found.</h3>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @if(isset($orders) && $orders->isNotEmpty())
    @foreach($orders as $ok => $ov)
        <?php
                $cart=unserialize($ov->cart);
                $event=App\Event::find($cart['event_id']);
        ?>
        <div class="modal" id="myModal-{{$ov->id}}">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body">
                        <h3>Order Details</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th> Order Id</th>
                                    <td>{{ $ov->id }}</td>
                                </tr>
                                <tr>
                                    <th> Event Id</th>
                                    <td>{{ $event->id }}</td>
                                </tr>
                                <tr>
                                    <th> Event Name</th>
                                    <td> {{ $event->event_name }} </td>
                                </tr>
                                <tr>
                                    <th> Event Date</th>
                                    <td> {{ $event->event_start_date }} </td>
                                </tr>
                                <tr>
                                    <th> Event Time</th>
                                    <td> {{ date('h:i:A',strtotime($event->event_start_time)) }} </td>
                                </tr>
                                <tr>
                                    <th> Event Venue</th>
                                    <td> {{ $event->event_venue }} </td>
                                </tr>
                                <tr>
                                    <th> Total Order Quantity</th>
                                    <td> {{ $cart['quantity'] }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    @endforeach
    @endif


@endsection
@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 <script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
 <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
 <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
 <script src=" https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
 <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>




@endsection
